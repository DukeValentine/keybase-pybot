from datetime import datetime
import asyncio
from collections import defaultdict
import json
import os
try:
    from database import ops
    from database.ops.user import mark_user_removed
    from kb_commands import team,post
except:
    from ..database import ops
    from ..database.ops.user import mark_user_removed
    from ..kb_commands import team


INFO = 0
WARNING_LOW = 1
WARNING_MEDIUM = 2
WARNING_HIGH = 3
BAN = 4
PERMABAN = 5


severity_to_name : {
    INFO : "INFO",
    WARNING_LOW : "WARNING_LOW",
    WARNING_MEDIUM : "WARNING_MEDIUM",
    WARNING_HIGH : "WARNING_HIGH",
    BAN : "BAN",
    PERMABAN : "PERMABAN"
}

name_to_severity : {
    "INFO" : INFO,
    "WARNING_LOW" : WARNING_LOW,
    "WARNING_MEDIUM" : WARNING_MEDIUM,
    "WARNING_HIGH" : WARNING_HIGH,
    "BAN" : BAN,
    "PERMABAN" : PERMABAN
}

async def remove_user(db,username,team_name,context,date):
    await ops.log.moderation.insert_moderation_log(db,username,team_name,severity = {"level": BAN, "level_name": "BAN"},context = context, date = date)
    await mark_user_removed(db,username)
    return

async def remove_users(db,users,moderator,team_name,context,message):
    date = datetime.utcnow()
    
    removed_users = await team.remove_users(db,users,moderator,team_name)
    
    tasks = asyncio.gather(*[remove_user(db,user['username'],team_name,context,date) for user in removed_users if user['status'] == 0])
    await tasks
    
    
    



