from antlr4 import *
from keybaseLexer import keybaseLexer
from keybaseListener import keybaseListener
from keybaseParser import keybaseParser
import sys

class keybasePrintListener(keybaseListener):
    def enterKbcommand(self, ctx):
        print(f"hey {ctx.EMPTY}")

def main():
    print("start")
    lexer = keybaseLexer(InputStream("!kbbb"))
    stream = CommonTokenStream(lexer)
    parser = keybaseParser(stream)
    print("tree")
    tree = parser.keybase()
    printer = keybasePrintListener()
    walker = ParseTreeWalker()
    walker.walk(printer, tree)

if __name__ == '__main__':
    main()
