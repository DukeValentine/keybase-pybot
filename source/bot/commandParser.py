"""
reverse image search: !search !iqdb !saucenao or reactions in reply to image
set weekly loli





"""

from lark import Lark,UnexpectedInput,UnexpectedCharacters, Token
from lark import Transformer as LarkTransformer
from enum import Enum

import asyncio,ast
from collections import defaultdict
from datetime import datetime
import json
import sys
nested_dict = lambda: defaultdict(nested_dict)

from . import botCommands
from .commands import grammar



        
        
class CommandTokens(Enum):
    SEARCH = "COMMAND"
    ADD= "COMMAND"
    REMOVE= "COMMAND"
    POOL= "COMMAND"
    MOVE= "COMMAND"
    QUERY= "COMMAND"
    REPORT= "COMMAND"
    WARN= "COMMAND"
    BAN= "COMMAND"
    INVITE= "COMMAND"

def get_token_alias(token):
    try:
        alias = CommandTokens[token].value
        return alias
    except KeyError as error:
        return token
    
    
    
def evaluate(string_value):
    try:
        value = ast.literal_eval(string_value.capitalize())
        
    except ValueError:
        return string_value
    else:
        return value




class Transformer(LarkTransformer):
    def parameters(self, matches):
        #print(f"paramters {matches}")
        return ("parameters",matches)
    
    def parameter(self,matches):
        return matches[0]
    
    def argument(self,matches):
        return {"ARGUMENT": {"value" : matches[0].value}}
    
    def date(self,matches):
        print("ddddate")
        return Token("DATE", datetime.strptime(matches[0].value,'%Y-%m-%d') )
    
    def from_date(self,matches):
        date_string = matches[0].value.replace("/","-")
        
        return Token("from_date", datetime.strptime(matches[0].value,'%Y-%m-%d') )
    
    def to_date(self,matches):
        date_string = matches[0].value.replace("/","-")
        
        return Token("to_date", datetime.strptime(matches[0].value,'%Y-%m-%d') )
        
    def assignment(self,matches):
        if(len(matches) == 2):
            values = evaluate(matches[1].value)
        else:
            values = [evaluate(match.value) for match in matches[1:]]
        
        return Token(matches[0].value,  values)
    def user(self, matches):
        
        matches[0].value = matches[0].value.replace("@","")
        return matches[0]

    def users(self, matches):
        return Token("USERS", [match.value for match in matches])
    
    
    def not_implemented_command(self,matches):
        return {"command_name" : None,'returncode': 3}
    
    def preview_command(self,matches):
        links = [match.value for match in matches[1:]]
        links = [f"https://{link}" for link in links if "http" not in link]
        
        return {"command_name": matches[0].type, "links": [match.value for match in matches[1:]],"returncode": 0,"command": botCommands.PreviewCommand()}
    
    
    def query_command(self,matches):
        command_info =  {"command_name" : matches[0].type, 'query' : matches[1].value.strip('\"')}
        try:
            command_info['users'] =  matches[2].value
        except IndexError as e:
            pass
        
        command_info['command'] = botCommands.QueryCommand()
        return command_info
    
    def search_command(self,matches):
        command_info = {"command_name" : matches[0].type,'returncode': 0,"command" : botCommands.SearchCommand()}
        try:
            command_info['source'] = matches[1].value
        except IndexError as e:
            pass
        
        return command_info
        
    def move_command(self,matches):
        match = {"command_name": matches[0].type,'channel': matches[1].value.strip("#"),'returncode': 0,"command": botCommands.MoveCommand()}
        try:
            match['team'] = matches[2]
        except IndexError as e:
            pass
        
        return match
    
    def add_remove_command(self,matches):
        command_info = {"command_name": matches[0].type, 'target': matches[1].value.strip(".").strip("_").strip("-").strip(">"),'users': None}
        command_info['returncode'] = 0
        
        for match in matches[2:]:
            
            if(match.type == "USERS"):
                command_info['users'] = match.value
                
            elif(match.type == "DATE"):
                command_info['date'] =  match.value
               
            
            else:
                if "values" in command_info:
                    command_info['values'].append( match.value.strip('"').strip("'"))
                    
                else:
                    command_info['values'] = [ match.value.strip('"').strip("'") ]
        
        if("date" not in command_info):
            command_info['date'] = datetime.utcnow()
            
        command_info['command'] = botCommands.AddRemoveCommand()
        return command_info
    
    def show_command(self,matches):
        command_info = {"command_name": matches[0].type, 'target': matches[1].value.strip(".").strip("_").strip("-").strip(">"),'returncode': 0, 'command': botCommands.ShowCommand()}
        return command_info
    
    def stat_command(self,matches):
        command_info = nested_dict()
        command_info = {"command_name": matches[0].type, 'target': matches[1].value.strip(".").strip("_").strip("-").strip(">"),'returncode': 0, 'command': botCommands.StatsCommand()}
        command_info['parameters'] = dict()
        try:
            for match in matches[2:]:
                command_info['parameters'][match.type] = match.value
        except IndexError:
            pass
        
        return command_info
    
    def auth_command(self,matches):
        target = matches[1].value.strip(".").split(".")
        return {"command_name": matches[0].type, "target": target.pop(), "collection": ".".join(target),'returncode': 0}
    
    def join_command(self,matches):
        target = matches[1].value.strip(".").split(".")
        
        return {"command_name": matches[0].type, "team": matches[len(matches)-1].value.strip("."),"target": ".".join(target[0:]),'returncode': 0}
        
        
    def pool_command(self,matches):
        return {"command_name": matches[0].type,'title': matches[1].value.strip('\"'), 'options': [match.value for match in matches[2:]],'returncode': 0,"command": botCommands.PoolCommand()}
    
    def warn_command(self,matches):
        command_info = {"command_name": matches[0].type, "users": matches[1].value}
        try:
            command_info['reason'] = matches[2].value
        except IndexError:
            pass
        command_info['returncode'] = 0
        command_info['command'] = botCommands.WarnCommand()
        return command_info
    
    def ban_command(self,matches):
        command_info = {"command_name": matches[0].type, "users": matches[1].value}
        try:
            command_info['reason'] = matches[2].value
        except IndexError:
            pass
        command_info['returncode'] = 0
        command_info['command'] = botCommands.BanCommand()
        return command_info
    
    def start(self, matches):
        try:
            if(matches[0].type == "USERNAME"):
                matches = matches[1:]
        except AttributeError as e:
            pass
        
        return matches
        
        


bot_grammar = grammar.initialize(Transformer())
    
 
def get_input_from_response(response):
    content_type = response['msg']['content']['type']
    
    if(content_type == "text" and response['msg']['content']['text']['body'].startswith("!")):
        return response['msg']['content']['text']['body']
    
    elif(content_type == "reaction"):
        return response['msg']['content']['reaction']['b']
    
    return None
    
 
def parse_commands(raw_input,is_json = True):
    if(is_json):
        command_input = get_input_from_response(raw_input)
    else:
        command_input = raw_input
    
    print(f"input == {command_input}")

    if(command_input is None):
        return [{'returncode': -1, "input": raw_input,"command": botCommands.Command()}]

    try:
        tree = bot_grammar.parse(command_input)
        print("parse done")
        return tree
        
    except UnexpectedCharacters as error:
        error_message = f"Unexpected {error.unexpected_character} at line: {error.line},column: {error.column} . Expected : "
        allowed = [type for type in error.allowed]
        error_message+= " , ".join(allowed)
        return [{'returncode': 1, 'error': error_message,"command": botCommands.Command()}]
        
    except UnexpectedInput as error:
        error_message = f"Unexpected {error.token.type}:{error.token.value} at line {error.line}, column {error.column}. Expected : "
        
        expected_tokens = [expected_token for expected_token in error.expected]
        error_message += " , ".join(expected_tokens)
        
        print(error.token.type)
        
        
        if(get_token_alias(error.token.type) == "COMMAND"):
            error_message += "\nMultiple commands must always have a semicolon between each other [ !command parameters;!command parameters ]"
            
        elif(get_token_alias(error.token.type) == "COMMAND_IDENTIFIER"):
            error_message += "\nUnkown command"
            
        if("DATE" in error.expected):
            error_message += "\nDates must follow the yyyy-mm-dd format, with the month and day being allowed to be single digit"
            
        
        return [{'returncode': 2, 'error': error_message,"command": botCommands.Command() }]

        
    
        
async def listen_commands(team, listen_all,users = []):
    
    print("listening")
    channel_filter = {"name": team, "members_type": "team"}
    if(listen_all is False):
        channel_filter['topic_name'] = "0_testing"
        
    channel_filter = [channel_filter]
    
    for user in users:
        channel_filter.append({'name': user})
    
    error_count = 0
    result = await asyncio.create_subprocess_exec("keybase", "chat","api-listen",'--filter-channels',f"{json.dumps(channel_filter)}",stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    while True:
         stdout = await result.stdout.readline()
         
         decoded_line = stdout.decode('utf-8').strip()
         
         try:
            yield json.loads( decoded_line),"keybase"
        
         except json.decoder.JSONDecodeError:
             
             print(f"error parsing {decoded_line}")
             error_count+=1
             if(error_count >= 5):
                 error_line = await result.stderr.read()
                 print(f"{error_line.decode('utf-8')}")
                 exit(1)
             
         else:
             print("decoded")
             error_count = 0
             

            
        
        
if __name__ == "__main__":
    print(parse_commands(":eye: https://link1.com,link2.com,link3.org,www.teste.org;!search;!pool title: option1,option2",is_json=False))
    print(parse_commands("!join.target lo_nly.miscellaneous",is_json=False))
    print(parse_commands("!move #channel team",is_json=False))
    print(parse_commands("!stats.team team = lo_nly START 2020-01-01",is_json=False))
    print(parse_commands("!warn @user1 @user2 @2use3 spam",is_json=False))
    print(parse_commands("!auth.botcommand.MoveCommand role = 4",is_json=False))
    
