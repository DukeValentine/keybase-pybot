import asyncio,aiofiles
from aiostream import stream
from enum import Enum
import csv
from collections import Counter
from datetime import datetime
import traceback
import os
from . import ripping,lookup
from . import commandParser
from . import moderation

try:
    from kb_commands import download,keybase,post
    import database.pybot_db as database
    import database.operations as db_operations
    from database import ops
    import utils.formatting as formatting
    import utils.logger as logger
    from ripper import danbooru,pixiv
    
except:
    from ..kb_commands import keybase,download,post
    from ..database import ops
    from ..database.ops import log
    from ..database import pybot_db as database
    from ..database import operations as db_operations
    from ..utils import formatting,logger
    from ..ripper import danbooru,pixiv
    from ..database.ops.update import user,team,post
    from ..database.ops import user,team,config
    




#ACTIONS = ["warn","ban","list","download","config","rebuild","update"]
ACTION_LEVEL = Enum("action_type","warn ban list download config rebuild update ripping")

def getFromDict(dataDict, mapList):    
    for k in mapList: dataDict = dataDict[k]
    return dataDict



def get_action(db,action,team_name,interval = 28,parser = None):
    
    all_actions = {
        'list' : ListMembers(db,action,team_name,interval,parser),
        'warn' : WarnMembers(db,action,team_name,interval,parser),
        'ban'  : BanMembers(db,action,team_name,interval,parser),
        'download' : DownloadAttachments(db,action,team_name,interval,parser),
        "config": ConfigDatabase(db,action,team_name,interval,parser),
        "rebuild":RebuildDatabase(db,action,team_name,interval,parser),
        "update": {
            "posts": UpdatePosts(db,action,team_name,interval,parser),
            "user" : UpdateUser(db,action,team_name,interval,parser),
            "team" : UpdateTeam(db,action,team_name,interval,parser),
            "lookup": UpdateLookup(db,action,team_name,interval,parser),
            },
        "ripping": RippingAction(db,action,team_name,interval,parser),
        "run" : RunBot(db,action,team_name,interval,parser)
        }
        
    try:
        return getFromDict(all_actions,action.split("."))
    except KeyError as error:
        return BlankAction(db,action,team_name,interval,action,error)



class Action:
    def __init__(self,db,level,team_name,interval):
        self.level = level
        self.team_name = team_name
        self.team_members = None
        self.db = db
        self.db_config = None
        self.ripping_config = None
        self.days_of_inactivity = interval
        
        if(self.team_name is None):
            self._get_team_name()
        
        if(self.team_name is None):
            exit(3)
            
    async def initialize_credentials(self):
        self.danbooru_credentials = await ops.auth.danbooru.get_credentials(self.db)
        self.atf_credentials = await ops.auth.atf.get_credentials(self.db)
        self.pixiv_credentials = await ops.auth.pixiv.get_credentials(self.db)
    
    async def initialize_ripconfig(self):
        self.ripping_config = await ops.config.get_ripping_config(self.db)
        

    async def log_debug(self,message):
        await ops.log.events.debug(self.db, type = "ACTION", name = self.class_name(),date = datetime.utcnow(),event_message = message, context = self.__dict__)
        
    async def log_verbose(self,message):
        await ops.log.events.verbose(self.db, type = "ACTION", name = self.class_name(),date = datetime.utcnow(),event_message = message, context = self.__dict__)
    
    async def log_info(self,message,context = None):
        await ops.log.events.info(self.db, type = "ACTION", name = self.class_name(),date = datetime.utcnow(),event_message = message, context = context)
    
    async def log_warning(self,message):
        await ops.log.events.warning(self.db, type = "ACTION", name = self.class_name(),date = datetime.utcnow(),event_message = message, context = self.__dict__)
    
    async def log_error(self,message):
        await ops.log.events.error(self.db, type = "ACTION", name = self.class_name(),date = datetime.utcnow(),event_message = message, context = self.__dict__)
    
    async def log_critical(self,message):
        await ops.log.events.critical(self.db, type = "ACTION", name = self.class_name(),date = datetime.utcnow(),event_message = message, context = self.__dict__)
        
    def _get_team_name(self):
        loop = asyncio.get_event_loop()
        self.team_name = loop.run_until_complete(ops.team.get_main_team(self.db))
        
        
    def class_name(self):
        return (self.__class__.__name__)
        
    def execute(self):
        raise NotImplementedError
    
class BlankAction(Action):
    def __init__(self,db,level,team_name,interval,action,error_key):
        super().__init__(db,level,team_name,interval)
        self.action = action
        self.error_key = error_key
        
    async def execute(self):
        print(f"Failed to parse action {self.action}. Error {self.error_key}")

class DownloadAttachments(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval)
        self.directory = parser.download_base_directory
        self.parser = parser
        
    async def execute(self):
        await download.download_pool_manager(self.db,self.team_name,self.directory)
       
        #await download.download_all_attachments_subteams(self.db,self.team_name,self.directory)
        
class RunBot(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval)
        self.parser = parser
        
    async def check_reminders(self):
        while True:
            await asyncio.sleep(14400)
            yield await ops.log.periodical.check_reminders(self.db),"reminder"
    def format_response(self,response):
        sender = response['msg']['sender']['username']
        msg_id = response['msg']['id']
        conversation_id = response['msg']['conversation_id']
        return f"{msg_id=},{sender=}, {conversation_id=}"
        
    async def execute(self):
        await db_operations.log_event_general(self.db, type = "ACTION", name = self.class_name(),date = datetime.utcnow(),level = logger.LEVELS['info'],event_message = "Running Keybase pybot", context = {"parser": self.parser.__dict__, "team": self.team_name})
        
        await logger.initialize_logger(self.parser.log_level)
        await logger.info("running bot")
        combined_stream = stream.merge(self.check_reminders(),commandParser.listen_commands(self.team_name, self.parser.listen_all,self.parser.listen_users))
        
        
        async with combined_stream.stream() as streamer:
            async for response,type in streamer:
                if(type == "reminder"):
                    if(response is not None and response['message'] is not None):
                        await post.send_message(response['message'],response['team'],response['channel'])
                    continue
                
                
                await db_operations.insert_keybase_post(self.db,await post.parse_post(response.get('msg',None)))
                parsed_commands = commandParser.parse_commands(response)
                await logger.debug(self.format_response(response))
                
                run_context = {"message": "", "current_index": 0 , "max_index": len(parsed_commands)}
                
                for index,parsed_command in enumerate(parsed_commands): 
                    if parsed_command.get("returncode",-2) == 0:
                        command = parsed_command['command']
                        message = response['msg']
                        await command.initialize_context(message,parsed_command,self.db, run_context)
                        
                        try:
                            await command.execute(self.db,parsed_command,self.parser)
                            
                        except keybase.KeybaseAPIError as error:
                            await command.send_reply(error.get_context())
                            
                        except Exception as error:
                            await command.send_reply(f"Failed to execute command!\n{traceback.format_exc()}")
                    
                    
class RippingAction(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval)
        self.parser = parser
        self.dryrun = parser.dryrun
        
        
    async def log_ripping_event(self,level,message):
        return 
    
    async def log_rip_result(self,source):
        return
    
    
    
    def counter_to_db(self, ripcounter):
        results = dict(ripcounter)
        results['total'] = sum(ripcounter.values())
        
        return results
    
    def sum_all_counters(self,*counters):
        total_counter = Counter()
        
        for counter in counters:
            total_counter += counter
            
        return sum(total_counter.values())
    
    async def log_ripping_results(self,destination,**results):
        total_results = sum(results.values(),Counter())
        results['total'] = total_results
        
        await db_operations.log_event_general(self.db, type = "ACTION", name = self.class_name(),date = datetime.utcnow(),level = logger.LEVELS['verbose'],event_message = f"Finished ripping for {destination}", context = results)
        

        
    async def execute(self):
        await self.log_info("Start ripping action", context = None)
        
        await logger.initialize_logger(self.parser.log_level)
        await self.initialize_credentials()
        await self.initialize_ripconfig()
        
        
        base_dir = self.ripping_config['base_dir']
        
        
        
        async for dest in await ops.config.get_ripping_destination(self.db):
            await self.log_info(f"starting download for {dest['name']}", context = {'tag': dest['name']})
            
            download_path = os.path.join(base_dir,"keybase-rip", dest['name'])
            
            download_tasks = asyncio.gather(danbooru.fetch_favgroup(self.db,self.danbooru_credentials,download_path,dest.get('danbooru',None), target = "danbooru",dryrun= self.dryrun),
                                            danbooru.fetch_favgroup(self.db,self.atf_credentials,download_path,dest.get('atf',None),target ="atf",dryrun = self.dryrun),
                                            pixiv.fetch_favtag(self.db,self.pixiv_credentials,dest.get('pixiv',None),download_path,self.dryrun))
            
            results = await download_tasks
            
            await self.log_ripping_results(dest['name'],danbooru = results[0], atf = results[1], pixiv = results[2])
            
            for entry in dest['keybase']:
                await self.log_info(f"starting uploading for {dest['name']}", context = {'tag': dest['name']})
                
                await danbooru.upload_to_keybase(self.db, dest['name'], entry['team'],entry['channel'],target = "danbooru",dryrun= self.dryrun)
                await danbooru.upload_to_keybase(self.db, dest['name'], entry['team'],entry['channel'],target = "atf",dryrun= self.dryrun)
                await pixiv.upload_to_keybase(self.db,dest['name'], entry['team'],entry['channel'],dryrun= self.dryrun)
                
                await self.log_info(f"finished uploading for {dest['name']}", context = {'tag': dest['name']})
            
        await self.log_info("Finished ripping action")

class ConfigDatabase(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval)
        self.config_path = parser.configpath
        
    async def execute(self):
        await ops.config.load_config_file(self.db,self.config_path)
    
        
class RebuildDatabase(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval)    
        
    async def execute(self):
        await ops.config.reset_database(self.db)
        await ops.config.create_indexes(self.db)
        await ops.update.team.initialize(self.db,self.team_name)
        await ops.update.post.update_posts(self.db, unread_only = False)
        await ops.update.user.initialize(self.db,self.team_name)
        
class UpdatePosts(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval)    
        self.unread = parser.unread
        self.page_size = parser.page_size
        
    
    async def execute(self):
        await ops.update.post.update_posts(self.db, max_days_of_inactivity = 28,unread_only = self.unread, page_size = self.page_size)
        
class UpdateUser(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval) 
        self.usernames = parser.usernames
        self.useraction = parser.update_action
        self.userfield = parser.field
        self.uservalue = parser.value
        
    async def execute(self):
        print(self.team_name)
        await ops.update.user.update_team_members(self.db,self.team_name)
        await ops.update.user.update_activity(self.db,self.team_name)
        
class UpdateTeam(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval) 
        
    async def execute(self):
        await ops.update.team.update(self.db)
        
class UpdateLookup(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval) 
        self.csvpath = parser.csvfile
        self.csvpath = self.csvpath.replace("~", os.getenv("HOME")) if self.csvpath else None
        
    async def _looksource(self,semaphore,path,fingerprint,saucenao_api_key):
        if(await ops.log.fingerprint_already_inserted(self.db,fingerprint) == True):
            return
        
        
        async with semaphore:
            directory,filename = os.path.split(path)
            source = await lookup.lookup_source(self.db,filename,directory,saucenao_api_key, format_source=False)
            await ops.log.insert_lookup_source(self.db, fingerprint,source)
            
        
    async def execute(self):
        tasks = []
        semaphore = asyncio.BoundedSemaphore(30)
        saucenao_credentials = await ops.auth.saucenao.get_credentials(self.db)
        self.saucenao_api_key = saucenao_credentials['apikey'] if saucenao_credentials else None
        
        with open(self.csvpath,"r") as csvfile :
            reader = csv.DictReader(csvfile)
            tasks = asyncio.gather(*[self._looksource(semaphore,entry['path'],entry['fingerprint'],self.saucenao_api_key) for entry in reader])
        
        await tasks
        return

#initialize_database(db,team, max_days_of_inactivity = 28):    
class ListMembers(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval)
        self.parser = parser
        self.excluded = parser.excluded
        self.max_role = parser.max_inactive_role
        
        
    async def execute(self):
        db_config = await db_operations.get_database_config(self.db)
        
        if(db_config):
            self.max_role = db_config['db']["max_inactive_role"]
            self.days_of_inactivity = db_config['db'].get("max_days_of_inactivity",self.days_of_inactivity)
        
        
        inactive_users,reference_date = await ops.user.get_inactive_users(self.db,self.max_role,self.team_name,self.days_of_inactivity)
        print(f"{len(inactive_users)} inactive users found")
        for user in inactive_users:
            print(formatting.format_inactive_user_info(self.team_name,user,reference_date,self.excluded))
        
class BanMembers(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval)
        self.excluded = parser.excluded
        self.moderator = parser.moderator
        self.max_role = parser.max_inactive_role
        
    
    async def execute(self):
        db_config = await db_operations.get_database_config(self.db)
        
        if(db_config):
            self.max_role = db_config['db']["max_inactive_role"]
            self.days_of_inactivity = db_config['db'].get("max_days_of_inactivity",self.days_of_inactivity)
            if(self.moderator is None):
                self.moderator = db_config['db']['moderators']
        
        inactive_users,reference_date = await ops.user.get_inactive_users(self.db,self.max_role,self.team_name,self.days_of_inactivity)
        for user in inactive_users:
            print(formatting.format_inactive_user_info(self.team_name,user,reference_date,self.excluded))
            
        
        
        await keybase.remove_users(self.db,[user['username'] for user in inactive_users if user['username'] not in self.excluded],self.moderator, self.team_name)
        
        for user in inactive_users:
            if(user['username'] in self.excluded):
                print(f"{user['username']} is excluded,skipping")
                continue
                
            
            usernames = self.moderator + [user['username']]
            message = formatting.format_inactive_user_removal(self.team_name,user,reference_date,self.days_of_inactivity )
            await post.send_chat_message(message,user['username'],*self.moderator)
        
            
            


        
        
        
class WarnMembers(Action):
    def __init__(self,db,level,team_name,interval,parser):
        super().__init__(db,level,team_name,interval)
        self.excluded = parser.excluded
        self.moderator = parser.moderator
        self.db = db
        
    
    async def execute(self):
        
        if(self.moderator is None):
            db_config = await db_operations.get_database_config(self.db)
            self.moderator = db_config['db']['moderators']
            
        
        inactive_users,reference_date = await database.get_inactive_users(self.db,'reader',self.team_name,self.days_of_inactivity)
        for user in inactive_users:
            if(user['username'] in self.excluded):
                print(f"{user['username']} is excluded,skipping")
                continue
                            
            
            usernames = self.moderator + [user['username']]
            message = formatting.format_inactive_user_warning(self.team_name,user,reference_date,self.days_of_inactivity )
            
            await ops.log.moderation.insert_moderation_log(self.db,user['username'],self.team_name,severity,context,date)
            await post.send_chat_message(message,user['username'],*self.moderator)
