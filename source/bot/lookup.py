import asyncio
import aiohttp
import aiofiles
import bs4
import json
import re
import os
from datetime import datetime

try:
    from utils import logger
    from database import ops
except:
    from ..database import ops 
    from ..utils import logger

#from collections import defaultdict

percentage_regex = re.compile("([0-9]*(.[0-9]*?))%")
pixiv_filename_regex = re.compile("([0-9]+)_p[0-9]+")
pixiv_ugoira_regex = re.compile("([0-9]+)_ugoira")
PIXIV_ARTWORKS_URL = "https://www.pixiv.net/en/artworks/"
home_dir = os.getenv("HOME")

def make_url_clickable(href):
    if(href.startswith("https")):
        return href
    return f"https:{href}"

async def iqdb_file(path, threshold = 85,best_match_only = True):
    url = "https://iqdb.org/"
    path = path.replace("~",home_dir)
    
    async with aiofiles.open(path,mode = "rb") as pic:
        async with aiohttp.ClientSession() as session:
            async with session.post(url, data = {'file' :  await pic.read()}) as response:
                text_result = bs4.BeautifulSoup(await response.text(),'html.parser')
                match_links = []
    
   
    
                for match in text_result.find_all('table'):
                    for similarity_match in match.find_all('td'):
                        percentage = percentage_regex.match(similarity_match.text)
                        if(percentage is not None and float(percentage.group(1)) >= threshold):
                            links = [make_url_clickable(link.get('href'))  for link in match.find_all('a')]
                            match_links.extend(links)
                

                
                if(len(match_links) > 0):
                    if(best_match_only):
                        print(match_links)
                        return match_links
                    else:
                        return  match_links
                else:
                    return None
    



async def saucenao_file(api_key, path, threshold = 85):
    url = f"https://saucenao.com/search.php?db=999&output_type=2&api_key={api_key}&testmode=1&numres=16"
    path = path.replace("~",home_dir)
    
    async with aiofiles.open(path,mode = "rb") as pic:
        async with aiohttp.ClientSession() as session:
            async with session.post(url, data = {'file' :  await pic.read()}) as response:
                try:
                    json_response = json.loads(await response.text())
                except json.decoder.JSONDecodeError:
                     return {'header' : {"status": -1 , "returncode": response.status},'links': None}
                 
                if(json_response['header']['status'] == 0):
                    long_remaining = json_response['header']['long_remaining']
                    links = []
                    
                    #print(json_response['header'])
                    for item in json_response['results']:
                        similarity = float(item['header']['similarity'])
                        if(similarity >= threshold):
                            links.append(item['data']['ext_urls'])
                        
                    links = None if len(links) == 0 else links
                        
                    return {'header' : json_response['header'],'links': links}
                else:
                    return {'header' : {"status" : json_response['header']['status']}}
    
    return

async def lookup_source(db,filename,directory,saucenao_api_key,format_source = True):
    if(pixiv_filename_regex.match(filename) is not None):
        return f"{PIXIV_ARTWORKS_URL}{pixiv_filename_regex.match(filename).group(1)}"
        
    elif(pixiv_ugoira_regex.match(filename) is not None):
        return f"{PIXIV_ARTWORKS_URL}{pixiv_ugoira_regex.match(filename).group(1)}"
    
    
    lookup_path = os.path.join(directory,filename) 
    
    try:
        saucenao_result = await saucenao_file(api_key = saucenao_api_key, path = lookup_path)
    except aiohttp.client_exceptions.ClientConnectorError as error:
        print(error)
        await ops.log.events.general(db, type = "CONNECTION", name = "lookup",date = datetime.utcnow(),level = logger.LEVELS['error'],event_message = f"Received {error} when accessing {fav_url}",context = {"error": error.__dict__})
        saucenao_result = {'header' : {"status": -1}}
    
    
    if(saucenao_result['header']['status'] == 0 and saucenao_result['links'] is not None):
        results = " , ".join(saucenao_result['links']) if format_source else saucenao_result['links']
        return results
        
    else:
        max_attempts = 3
        for attempt in range(0,max_attempts):
            try:
                iqdb_result = await iqdb_file(lookup_path)
                break
            
            except aiohttp.client_exceptions.ClientError:
                asyncio.sleep(5)
                
        
        return iqdb_result
    
    

    
    



async def main():
    pass
    
if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
