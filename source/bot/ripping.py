import os
import json
from collections import defaultdict
from concurrent.futures.process import ProcessPoolExecutor
import database.operations as db_operations
import utils.logger as logger
import subprocess
import shlex
import asyncio



home_dir = os.getenv("HOME")

async def run_gallerydl_command(base_dir,db_file,dest_dir,url,count=1):
    if(url is None): 
        return
    
    download_dir = os.path.join(base_dir,dest_dir)
    await logger.info(f"{url} ,,,, {count}")
    
    result = await asyncio.create_subprocess_exec("gallery-dl","--ugoira-conv","-o","extractor.danbooru.directory=\"\"","-o","extractor.pixiv.directory=\"\"","-d",download_dir,"--download-archive",db_file,url,stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    #await result.wait()
    stdout,stderr = await result.communicate()
    decoded = stdout.decode('utf-8')
    print(decoded)
    print(stderr)
    if(not decoded):
        return 0,url
    
    ripped_count = len(decoded.split("\n"))
    return ripped_count,url
    
    
def sub_loop(base_dir,db_file,dest_dir,url,count):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    return loop.run_until_complete(run_gallerydl_command(base_dir,db_file,dest_dir,url,count))

def format_pagecount(url,ripcount):
    if("pixiv" in url):
        return f"{url}&p={int(ripcount/20)+1}"
    else:
        parts = url.split("?")
        
        return f"{parts[0]}?page={int(ripcount/20)+1}&{parts[1]}"
    
async def ripping_pool_manager(db,base_dir,gallerydl_dbfile,dest_dir,urls):
    await logger.info(f"Starting ripping for {dest_dir}")
    
    rip_counts = []

    tasks = asyncio.gather(*[run_gallerydl_command(base_dir,gallerydl_dbfile,dest_dir,url,0) for url in urls])
    #for ripcount,url in await tasks:
        #print(url)
        ##await db_operations.update_url_page(db,url,ripcount)
    await tasks
    await logger.info(f"Finished ripping for {dest_dir}")
    return

async def rip_danbooru_favgroup(db,favgroup_name,directory):
    return



