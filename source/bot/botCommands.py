import asyncio,re,os
import logging
from collections import defaultdict
from urllib.parse import urlparse
from datetime import datetime



try:
    import html_parser
    from kb_commands import post,keybase,download,member
    from database import ops as db_ops
    from utils import formatting,logger
    from utils import fingerprint as fingerprint_tools
except:
    from .. import html_parser
    from ..kb_commands import post,keybase,download,member
    from ..database import operations as db_operations
    from ..database import ops as db_ops
    from ..database.ops import log,team
    from ..database.ops.log.misc import *
    from ..database.ops.stats import team
    from ..database.ops import post
    from ..utils import fingerprint as fingerprint_tools
    
    

from database import operations as db_operations


from . import lookup,action




basic_url_regex = re.compile("[http https]:/")

class CommandInitializationException(Exception):
    def __init__(self,message,status):
        self.status = status
        self.message = message
        
    def __str__(self):
        return repr(self.message)



class Command():
    def __init__(self):
        self.command_info = None
        self.response = None
        self.message_id = None
        self.target_id = None
        self.channel = None
        self.sender = None
        self.team = None
        self.status = None
        self.initialized = False
        self.target_team = None
    
    async def execute(self,db,command_info,parser = None):
        raise NotImplementedError("Implement execute method")
    
    
    async def is_authorized(self,db):
        if(self.initialized is False):
            return False
        
        else:
            user_role = await db_ops.user.get_user_role(db, self.sender)
            return await db_ops.auth.botCommands.is_user_authorized(db,self.__class__.__name__, user_role)
        
    async def verify_initialization(self,db):
        if(self.initialized == False):
            raise CommandInitializationException(f"Post context not initialized, can't execute {self.__class__.__name__}",-1)
        
        if(await self.is_authorized(db) is False):
            raise CommandInitializationException("User doesn't have enough privileges to execute command",-2)
        
        
        
    
    
    async def get_post_info(self,db):
        post_info = await db_ops.post.get_post_info(db,self.target_id,self.team,self.channel)
        
            
        if(post_info is None):
            chat = self.sender if self.channel is None else None
            
            await db_ops.update.post.update_posts(db, max_days_of_inactivity = 28,unread_only = False,page_size = 50,chat_source=chat)
            post_info = await db_ops.post.get_post_info(db,self.target_id,self.team,self.channel)
            
        return post_info
    
    
    async def initialize_context(self, response,command_info, db, run_context):
        content_type = response['content']['type']
        if(content_type == "reaction"):
            self.target_id = response['content']['reaction']['m']
        elif(content_type == "text"):
            self.target_id = response['content']['text'].get('replyTo',response['id'])
            
        self.db = db
        self.message_id = response['id']    
        self.convtype = response['channel']['members_type']
        self.channel = response['channel'].get('topic_name',None)
        self.team = response['channel']['name']
        self.sender = response['sender']['username']
        self.response = response
        self.command_info = command_info
        self.run_context = None
        
        
        try:
            await self.initialize_parameters(command_info)
            self.initialize_command_target(command_info)
        except AttributeError:
            pass
        
        
        self.initialized = True
        
    def increment_command_index(self):
        if(self.run_context):
            self.run_context['current_index'] += 1 
    
    def is_last_command(self):
        return (self.run_context is None or self.run_context['current_index'] >= self.run_context['max_index'])
    
    def prepare_message(self,message):
        if(self.run_context is None):
            return message
        
        else:
            self.run_context['message'] += f"\n{message}"
            return self.run_context['message']
        
        
    def encode_command_info(self):
        encoded = dict()
        for key,value in self.command_info.items():
            if(isinstance(value,Command)):
                continue
            encoded[key] = value
        
        
    async def log_startfinish(self, is_start):
        type = "STARTED" if is_start else "FINISHED"
        name = self.__class__.__name__
        print(f"name ===== {name}")
        
        
        
        await db_ops.log.events.general(self.db,type = "COMMAND",name = name, date = datetime.utcnow(),level = logging.INFO,event_message = f"{type} {name}", context = {"response": self.response, "command": self.encode_command_info()})
        
    async def send_reply(self, message):
        if(self.initialized is False):
            return
        
        await post.send_reply(message,self.team,self.channel,self.message_id)
   
class StopCommand(Command):
    def __init__(self):
        super(StopCommand,self).__init__()
        
    async def execute(self,db,command_info,parser = None):    
        if(self.is_authorized(db) is False):
            print("Sender doesn't have enough privileges to execute command")
            self.status = -3
            return
    
        message = "Bot shutting down..."
        await post.send_reply(message,self.team,self.channel,self.message_id)
        exit(0)
        

class SearchCommand(Command):
    def __init__(self):
        super(SearchCommand,self).__init__()
        self.saucenao_api_key = None
        self.lookup_directory = None
        self.lookup_filename = None
        
    async def initialize_parameters(self,command_info):
        saucenao_credentials = await db_ops.auth.saucenao.get_credentials(self.db)
        self.saucenao_api_key = saucenao_credentials['apikey'] if saucenao_credentials else None
        
        config = await db_ops.config.get_database_config(self.db)
        self.lookup_directory = config['temp_dir'] if config else None
        
        post_info = await self.get_post_info(self.db)
        self.lookup_filename = post_info['content']['filename'] 
    
    async def execute(self,db,command_info,parser = None):
        if(self.initialized == False):
            print("Post context not initialized, can't execute search")
            self.status = -1
            return 
        
        if(self.target_id is None):
            print("Search command requires a target post to find a source")
            self.status = -2
            return
        
        if(self.target_id == self.message_id):
            print("Search command can only be used as a reaction or as a reply")
            self.status = -2
            return
        
        
        await self.log_startfinish(is_start = True)
        
        print(f"executing search  {self.message_id}//{self.target_id}")
        if(self.lookup_directory is None):
            print("Lookup directory not found.Aborting")
            return
        
        await download.download_attachment(self.team,self.channel,self.lookup_directory,self.target_id,self.lookup_filename,max_attempts = 6, team_rip = False)
        
        lookup_path = os.path.join(self.lookup_directory,self.lookup_filename)
        fingerprint = await fingerprint_tools.generate_fingerprint_file(lookup_path)
        
        similar = await db_ops.log.find_similar_fingerprint(self.db,fingerprint)
        
        if(similar is not None):
            print("shortcut!")
            results =  similar['source']
            
        else:
            results = await lookup.lookup_source(self.db,self.lookup_filename,self.lookup_directory,self.saucenao_api_key,format_source = False)
        
        
        await db_ops.log.insert_lookup_source(self.db, fingerprint,results)
        await post.send_reply(" , ".join(results),self.team,self.channel,self.target_id)
        await self.log_startfinish(is_start = False)
        
        
class QueryCommand(Command):
    def __init__(self):
        super(QueryCommand,self).__init__()
    
    async def execute(self,db,command_info,parser = None):
        if(self.initialized == False):
            print("Post context not initialized, can't execute query")
            self.status = -1
            return 
        
class MoveCommand(Command):
    def __init__(self):
        super(MoveCommand,self).__init__()
        self.target_channel = None
        self.target_team = None
        
    
    async def execute(self,db,command_info,parser = None):
        if(self.initialized == False):
            print("Post context not initialized, can't execute move")
            self.status = -1
            return
        
        if(self.target_id is None):
            print("Move command requires a target post to move")
            self.status = -2
            return
        
        if(self.is_authorized(db) is False):
            print("Sender doesn't have enough privileges to execute command")
            self.status = -3
            return
        
        
        await self.log_startfinish(is_start = True)
        self.target_channel = self.command_info['channel']
        self.target_team = self.command_info.get('team',self.team)
        
        if(self.target_team is None):
            self.target_team = self.team
            
        print("moving")
        directory = await db_operations.get_temp_directory(db)
        
        try:
            post_info = self.get_post_info(db)
                
            filename = post_info['content']['filename']
            result = await download.download_attachment(self.team,self.channel,directory,self.target_id,filename,max_attempts = 6, team_rip = False)
            print("download ok")
            
            result = await post.send_chat_attachment(directory,filename,self.target_team,self.target_channel, title = f"Post moved from {self.team}#{self.channel}\n\n\r| Original poster {post_info['sender']}")
            print("upload ok")
            result = await post.delete_message(self.team,self.channel,self.target_id)
            await post.delete_message(self.team,self.channel,self.message_id)
            
             
        except keybase.KeybaseAPIError as error:
            print(error.get_context())
            
        else:
            await self.log_startfinish(is_start = False)
            
class PreviewCommand(Command):
    def __init__(self):
        super(PreviewCommand,self).__init__()
        
    async def execute(self,db,command_info,parser = None):
        if(self.initialized == False):
            print("Post context not initialized, can't execute move")
            self.status = -1
            return
        
        if(self.target_id is None):
            print("Preview command requires a target to preview")
            self.status = -2
            return    
        
        await self.log_startfinish(is_start = True)
        
        links = self.command_info.get("links",[])
        if(links is None or len(links) == 0):
            post_info = await self.get_post_info(db)
            if(post_info['content']['type'] != "text"):
                print("Previews can only be generated for text posts")
                return 
            text = post_info['content']['text']['body']
            links = [word for word in text.split(" ") if len(urlparse(word).netloc) > 0]
            
            
        
        config = await db_ops.config.get_database_config(self.db)
        path = config['temp_dir'] if config else None
        
        if(links is not None):
            for link in links:
                link_parser = html_parser.get_parser(link)
                link_parser.path = path
                link_parser.db = db
                print(await link_parser.generate_preview())
                await link_parser.send_preview(self.team, self.channel)
        
        await self.log_startfinish(is_start = False)
                

class StatsCommand(Command):
    def __init__(self):
        super(StatsCommand,self).__init__()
        self.target = self.subtarget = self.stat_type = None
        self.start_date = None
        self.end_date = datetime.utcnow()
        self.by_subteam = False
        self.by_user = True
        self.max_entries = 10
        self.types = []
        
        
        
    async def initialize_parameters(self,command_info):
        self.start_date = command_info['parameters'].get("from_date",self.start_date)
        self.end_date = command_info['parameters'].get("to_date", self.end_date)
        self.by_subteam = command_info['parameters'].get("by_subteam", self.by_subteam)
        self.types = command_info['parameters'].get("type", self.types)
        
        return
        
    def initialize_command_target(self,command_info):
        self.stat_type = command_info['target'].split(".")[0]
        targets = {'team': command_info['parameters'].get('team',self.team), 'user': self.command_info['parameters'].get('users',None)}
        
        self.target = targets.get(self.stat_type,None)
                
        try:
            self.subtarget = self.command_info['target'].split(".")[1]
        except IndexError:
            pass    
        
        
        
    async def execute(self,db,command_info,parser = None):
        if(self.initialized == False):
            print("Post context not initialized, can't execute search")
            self.status = -1
            return 
        
        await self.log_startfinish(is_start = True)
        
        stats_results = await db_ops.stats.aggregate(self,db)
        if "error" in stats_results:
            return
        
        message = ""
        for team,stats_category in stats_results.items():
            message += f"Team {team} stats:\n\n"
            for stat_type,entries in stats_category.items():
                message += f"\n>Category {stat_type}\n\n"
                for count,entry in enumerate(entries):
                    if(count == self.max_entries):
                        break
                    message += f">> {entry.toString(stat_type)}\n"
        
        await self.log_startfinish(is_start = False)
        await post.send_reply(message,self.team,self.channel,self.target_id)
                
class ShowCommand(Command):
    
    
    
    def __init__(self):
        super(ShowCommand,self).__init__()
        self.command_target = None
        self.command_subtarget = None
        self.targets = defaultdict(self.default_show_option)
        self.targets['contest'] = self.parse_contests
        
        
    def default_show_option(self):
        return self.parse_periodical
        
    async def parse_contests(self,db,subtarget = None):
        items = await db_ops.log.contest.get_history(db)
        
        message = "Contest history:\n\n"
        
        for item in items:
            winners = " , ".join(item.get('winners',["No winners chosen yet"]))
            if 'end_date' in item: item['end_date'] = item['end_date'].strftime('%B %d, %Y')
            
            message += f"Name: {item['contest_name']} \nFrom {item['start_date'].strftime('%B %d, %Y')} to {item.get('end_date', '?')}. Winners : {winners}\n"
        
        
        return message
    
    
    
    async def parse_periodical(self,db,target):
        items,duplicate_candidates = await db_ops.log.periodical.get_periodical_items(db,target)
        
        message = f"Previously chosen for {target}\n\n"
        
        if(items is not None):
            for item in items:
               message += f"{item['item']} :  {item['last_date'].strftime('%B %d, %Y')}\n"
           
            if(duplicate_candidates is not None):
                message += "\nCan be added again:\n\n"
           
                for duplicate in duplicate_candidates:
                    message += f"{duplicate['item']}\n"
        
        return message
    
    async def parse_teams(self,db):
        return 
    
    async def parse_users(self,db):
        return 
    
    async def parse_moderators(self,db):
        return
        
    async def parse_items(self,db):
        target = self.command_info.get("target",None)
        if(target is not None):
            self.command_target = target.split(".")[0]
            try:
                self.command_subtarget = target.split(".")[1:]
            except IndexError:
                pass
            else:
                self.command_subtarget =  ".".join(self.command_subtarget)
        if(self.command_target == "contest"):
            return await self.parse_contests(db)
        
        else:
            return await self.parse_periodical(db,target)
        
    async def execute(self,db,command_info,parser = None):
        if(self.initialized == False):
            print("Post context not initialized, can't execute ")
            self.status = -1
            return
        
        if(await self.is_authorized(db) is False):
            print("Sender doesn't have enough privileges to execute command")
            self.status = -3
            return
        
        
        await self.log_startfinish(is_start = True)
        self.increment_command_index()
        
        target = self.command_info.get("target",None)
        
        if(target is None):
            print("Error! no target found")
        
        message = await self.parse_items(db)
        message = self.prepare_message(message)
        
        await self.log_startfinish(is_start = False)
        if(self.is_last_command()): await post.send_reply(message,self.team,self.channel,self.target_id)
            
        
            
        
            
        
        
class AddRemoveCommand(Command):
    def __init__(self):
        super(AddRemoveCommand,self).__init__()
        self.target = None
        self.command_subtarget = None
        self.users = self.command_name = None
        self.values = [None]
        self.date = datetime.utcnow()
        
    async def initialize_parameters(self,command_info):
        self.values = self.command_info.get("values", self.values)
        self.users = self.command_info.get("users",self.users)
        self.date = self.command_info.get("date",self.date)
        self.command_name = self.command_info.get("command_name")
        
        return
        
        
    async def parse_target(self,db):
        target = self.command_info.get("target",None)
        if(target is not None):
            self.target = target.split(".")[0]
            try:
                self.command_subtarget = target.split(".")[1:]
            except IndexError:
                pass
            else:
                self.command_subtarget =  ".".join(self.command_subtarget)
                
        else:
            raise ValueError
                
        
        values = self.command_info.get("values",[None])
        users = self.command_info.get("users",None)
        date = self.command_info.get("date",datetime.utcnow())
        command_name = self.command_info.get("command_name")
            
        if(self.target == "contest"):
            message = await db_ops.log.contest.change(self.db,self.command_name,self.values[0],self.date,self.users, self.command_subtarget)
            
        else:
            message = await db_ops.log.periodical.change_periodical(self.db,self.target,self.values,self.command_name,self.date)
        
        return message
        
    
    async def execute(self,db,command_info,parser = None):
        if(self.initialized == False):
            print("Post context not initialized, can't execute ")
            self.status = -1
            return
        if(await self.is_authorized(db) is False):
            print("Sender doesn't have enough privileges to execute command")
            self.status = -3
            return
        
        await self.log_startfinish(is_start = True)
            
        print(f"{self.command_name} : {self.target} -> {self.values}")
        message = None
        try:
            message = await self.parse_target(db)
        except ValueError:
            message = "failed to parse target"
            
        else:
            await self.log_startfinish(is_start = False)
            
        finally:
            await self.send_reply(message)
            
            
class JoinCommand(Command):
    def __init__(self):
        super(JoinCommand,self).__init__()
        
    async def parse_target(self):
        target = self.command_info.get("target",None)
        self.target_team = self.command_info.get('team',None)
        
    async def execute(self,db,command_info,parser = None):
        await self.verify_initialization(db)
        await self.parse_target() 
        
        joinable_status = db_ops.team.is_team_open(db,self.team,self.target_team)
        if(joinable_status is None):
            message = "No subteam found"
        elif (joinable_status is False):
            message = "this subteam isn't open to members"
            
        else:
            await member.add_to_team(self.target_team,self.sender)
            message = "Added successfully to subteam"
        
        
        await self.send_reply(message)
            
            
        
class PoolCommand(Command):
    def __init__(self):
        super(PoolCommand,self).__init__()
    
    async def execute(self,db,command_info,parser = None):
        print()
        
class WarnCommand(Command):
    def __init__(self):
        super(WarnCommand,self).__init__()
    
    async def execute(self,db,command_info,parser = None):
        print()

class BanCommand(Command):
    def __init__(self):
        super(BanCommand,self).__init__()
    
    async def execute(self,db,command_info,parser = None):
        print()

class RunActionCommand(Command):
    def __init__(self):
        super(PoolCommand,self).__init__()
    
    async def execute(self,db,command_info,parser = None):
        print()
        
        
        
