from lark import Lark

def initialize(transformer):
    
    
    return Lark(
    '''
    start: user? command (";" command)* ";"?
    ?command: search_command | move_command | add_remove_command | pool_command | query_command | preview_command | warn_command | ban_command | auth_command | join_command | show_command | stat_command|not_implemented_command
    
    
    
    
    
    
    add_remove_command: (ADD|REMOVE) COMMAND_TARGET (ARGUMENT (","ARGUMENT)*)?  users? date?
    auth_command : AUTH COMMAND_TARGET ARGUMENT "=" (CNAME|DIGIT)+
    show_command: SHOW COMMAND_TARGET ("from" DATE "to" DATE)? 
    stat_command : STATS COMMAND_TARGET assignment* (from_date? to_date?)?
    create_command : CREATE
    
    
    
    join_command: JOIN COMMAND_TARGET? TEAM
    invite_command: INVITE users
    ban_command: BAN users ARGUMENT?
    warn_command: WARN users ARGUMENT?
    
    search_command: SEARCH ARGUMENT?
    move_command:  MOVE CHANNEL TEAM?
    pool_command: POOL ARGUMENT":"ARGUMENT (","ARGUMENT)+
    preview_command: PREVIEW  (LINK ("," LINK )*)?
    query_command: QUERY ARGUMENT users?
    
    report_command: REPORT users
    role_command: ROLE ROLENAME users (","ROLENAME users)*
    
    
    
    
    not_implemented_command : "!"ID | ":" ID ":"
    
    SEARCH: ("!search" | ":mag:" | ":mag_right:")
    MOVE: "!move" 
    ADD : "!add" 
    AUTH : "!auth"
    SHOW: "!show"
    STATS: "!stats"
    
    CREATE: "!create"

    REMOVE: "!remove"
    POOL: "!pool"
    PREVIEW: "!preview" | ":eye:" | ":eyes:"
    QUERY: "!query" | "!lookup"
    JOIN : "!join"
    REPORT: "!report" | ":warning:" | ":police_car:" | ":rotating_light:"
    INVITE : "!invite"
    BAN : "!ban" | "!remove_user"
    WARN : "!warn"
    ROLE : "!role" 
    
    
    
    
    
    users: user (","? user)*
    parameters : parameter(","parameter)*
    
    parameter : argument | assignment
    argument: ARGUMENT
    assignment.2: ARGUMENT "=" ARGUMENT ("," ARGUMENT)*
    date : DATE
    from_date : ("from"|"FROM"|"START"| "start") DATE
    to_date : ("to"| "TO"| "end"| "END") DATE
    
    ASSIGNMENT : ARGUMENT "=" ARGUMENT
    
    DATE.2: /[0-9]{4}/ ("-"|"/") /[0-9]{1,2}/ ("-"|"/") /[0-9]{1,2}/ 
    ROLENAME: CNAME
    COMMAND : "!"ID | ":" ID ":"
    user : USERNAME 
    TEAM: ID
    ID: /\w/(CNAME|/\\./|DIGIT)+
    COMMAND_TARGET: "."  ID
    USERNAME: "@"ID
    ARGUMENT: ID|ESCAPED_STRING| ESCAPED_STRING_ALT | DATE | "-"?(DIGIT)+
    LINK : ("http:"|"https:")? (CNAME|/\\./|DIGIT|/\\//)+ 
    CHANNEL: "#"ID
    
    NEXT_COMMAND : ";"
    COMMAND_IDENTIFIER: "!"
    
    
    %import common.WORD   // imports from terminal library
    %import common.DIGIT
    %import common.CNAME
    %import common.WS_INLINE
    %import common.LF
    %import common.ESCAPED_STRING
    %import common.ESCAPED_STRING_ALT
    %ignore " "   // Disregard spaces in text
    ''',parser='lalr',transformer = transformer)
