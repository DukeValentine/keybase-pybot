import asyncio,aiohttp,aiofiles,os
from urllib.parse import urlparse

class commonParser():
    def extract_paragraph_character_limit(self,paragraph_list,limit):
        paragraph = 0
        while(len("".join(paragraph_list[0:paragraph+1])) + len(paragraph_list[paragraph+1]) < limit):
            paragraph+=1
        return "\n\n".join(paragraph_list[0:paragraph])
    
    async def configure_session(self,session):
        raise NotImplementedError("Implement this method in the child class")
    
    async def get_credentials(self):
        raise NotImplementedError("Implement this method in the child class")
    
    
    async def download_thumbnail(self,url):
        if(url is None):
            return None
        
        print(url)
        filename = url.split("/")[-1] 
        async with aiohttp.ClientSession() as session:
            result = await self.configure_session(session)
            if(result is None):
                return None
            
            async with session.get(url) as response:
                if response.status == 200:
                    try:
                        image = await aiofiles.open(os.path.join(self.path,filename), 'wb')
                        await image.write(await response.read())
                        await image.close()
                        self.filename = filename
                        self.status = 0
                        return "downloaded"
                        
                    except OSError as error:
                        print(error)
                        self.status = 1
                        return "error"
                else:
                    print(response.status)
        
    
    async def get_url_content(self):
        async with aiohttp.ClientSession() as session:
            
            if(self.need_cookies is True):
                result = await self.configure_session_cookies(session)
                if(result is None):
                    return None
            
            async with session.get(self.url) as response:
                print(response.__dict__)
                return await response.text()
            
    async def generate_preview(self):
        print(f"no parser found for given {self.url}")
            
            
    def __init__(self,url,preview_size = 2000):
        self.url = url
        self.parsed_url = urlparse(self.url)
        self.preview_size = preview_size
        self.need_cookies = False
        self.need_auth = False
        self.path = None
        self.db = None
        self.reply_id = None
        self.filename = None
        self.status = None
        
        
#parsers = {
        #"exhentai.org": exhentai.exhentaiParser,
        #"asstr.org": asstr.asstrParser,
        #"www.asstr.org": asstr.asstrParser
        
    
#}

#def get_parser(url):
    #parsed_url = urlparse(url)
    #domain = parsed_url.netloc
    
    #parser = parsers.get(domain,CommonParser)
    #return parser(url)



