from .common import commonParser
from database import ops
import asyncio,aiohttp,bs4,re
background_url_regex = re.compile("url\((.*)\)")
from kb_commands import post

class exhentaiParser(commonParser):
    def __init__(self,url):
        super(exhentaiParser,self).__init__(url)
        self.credentials = None
        
        
    async def get_credentials(self):
        if(self.db is None):
            print("No defined database")
            return 
        
        
        result = await ops.auth.exhentai.get_credentials(self.db)
        if(result is None):
            print("failed to get credentials")
        else:
            self.credentials = result.get("credentials",None)
        
    
    async def get_thumbnail_url(self,content):
        if(content is None):
            return None
        
        html = bs4.BeautifulSoup(content, 'html.parser')
        thumb_div = html.find("div",{"id":"gd1"})
        url_match = background_url_regex.search(str(thumb_div))
        
        if(url_match is None):
            return None
        else:
            return url_match.group(1)
        
    

    
    async def generate_preview(self):
        if(self.path is None):
            return "no path specified"
        
        await asyncio.sleep(2)
        print("exhentai")
        result = await self.get_url_content()
        thumbnail_url = await self.get_thumbnail_url(result)
        print(await self.download_thumbnail(thumbnail_url))
        
    
    async def configure_session(self,session):
        await self.get_credentials()
        
        if(self.credentials is None):
            print("Failed to set cookies")
            return None
        
        cookie_jar = session._cookie_jar._cookies
        cookie_jar['.exhentai.org']['ipb_member_id'] = self.credentials['ipb_member_id']
        cookie_jar['.exhentai.org']['ipb_pass_hash'] = self.credentials['ipb_pass_hash']
        cookie_jar['.exhentai.org']['igneous'] = self.credentials['igneous']
        
        return cookie_jar
    
    
    async def send_preview(self,team,channel):
        if(self.status != 0):
            print("Failed to generate preview. Can't send it")
            return 
        
        await post.send_chat_attachment(self.path,self.filename,team,channel,title = self.url)
        
    
    
    




