import requests,bs4
import asyncio,aiohttp
from urllib.parse import urlparse
from .common import commonParser




def extract_paragraph_character_limit(paragraph_list,limit):
    paragraph = 0
    while(len("".join(paragraph_list[0:paragraph+1])) + len(paragraph_list[paragraph+1]) < limit):
        paragraph+=1
    return "\n\n".join(paragraph_list[0:paragraph])


def no_parser(page_content):
    print("No extractor for this asstr section")
    return 


def piper_parsing(page_content):
    print("piper");return
    
    html = bs4.BeautifulSoup(page_content, 'html.parser')
    storytext = (html.find("div",{"class": "contentareabox"})).getText()
    storytext = storytext.replace("\n\n","\n").strip().split("\n")
    
    
def ls_parsing(page_content):
    print("ls");return
    
    html = bs4.BeautifulSoup(page_content, 'html.parser')
    storytext = [paragraph.getText() for paragraph in (html.find("div",{"id": "target"}).find("div",{"id": "column"}).find_all("p"))]
    
    
class asstrParser(commonParser):
    all_sections = ["LS","Piper","Uther_Pendragon"]
    
    def dummy(self):
        return
    
    async def no_parser(self):
        storytext = None
        for section in self.sections:
            storytext = getattr(self,f"_{section}",self.dummy)()
            if(storytext is not None):
                return storytext
            
        
        return 
    

    async def _LS(self):
        page_content = await self.get_url_content()
        
        html = bs4.BeautifulSoup(page_content, 'html.parser')
        storytext = [paragraph.getText() for paragraph in (html.find("div",{"id": "target"}).find("div",{"id": "column"}).find_all("p"))]
        return extract_paragraph_character_limit(storytext,self.preview_size)
        
    async def _Piper(self):
        page_content = await self.get_url_content()
        
        html = bs4.BeautifulSoup(page_content, 'html.parser')
        storytext = (html.find("div",{"class": "contentareabox"})).getText()
        storytext = storytext.replace("\n\n","\n").strip().split("\n")    
        return extract_paragraph_character_limit(storytext,self.preview_size)
    
    async def _Uther_Pendragon(self):
        page_content = await self.get_url_content()
        html = bs4.BeautifulSoup(page_content, 'html.parser')
        try:
            storytext = [paragraph.getText() for paragraph in  (html.find("table").find_all("tr")[2].find_all("p"))]
            storytext = extract_paragraph_character_limit(storytext,self.preview_size)
            
        except (AttributeError,IndexError):
            return None
        
       
        return storytext
    
    def __init__(self,url,preview_size = 2000):
        super(asstrParser,self).__init__(url)
        self.section = self.parsed_url.path.split("/")[1].strip("~")
        self.preview_size = preview_size
        self.generate_preview = getattr(self,f"_{self.section}", self.no_parser)
    
    

    
    
async def main():
    #parser = asstrParser("https://www.asstr.org/~Piper/authors/willobeegoode/aprilshowers.html")
    #print(parser.section)
    #print(await parser.generate_preview())
    
    parser = asstrParser("https://www.asstr.org/~Uther_Pendragon/story/flights.htm")
    print(parser.section)
    print(await parser.generate_preview())
    
    parser = asstrParser("https://www.asstr.org/~Uther_Pendragon/story/april.htm")
    print(parser.section)
    print(await parser.generate_preview())
    return
    
if __name__ == "__main__":    
    asyncio.run(main())
    
#parser = asstrParser("https://www.asstr.org/~Piper/authors/willobeegoode/aprilshowers.html")
#parser.parse()
#exit(0)


#page = requests.get("https://www.asstr.org/~Piper/authors/willobeegoode/aprilshowers.html")
#html = bs4.BeautifulSoup(page.content, 'html.parser')
#storytext = (html.find("div",{"class": "contentareabox"})).getText()
#storytext = storytext.replace("\n\n","\n").strip().split("\n")
#print("link 1")
#preview = ""
#total = 0
#paragraph = 0
#while(len("".join(storytext[0:paragraph+1])) + len(storytext[paragraph+1]) < 2000):
    #paragraph+=1

#print("\n\n".join(storytext[0:paragraph]))


#page = requests.get("https://www.asstr.org/~Piper/authors/alvotorelli/attheweddingamcf1.html")
#html = bs4.BeautifulSoup(page.content, 'html.parser')
#storytext = (html.find("div",{"class": "contentareabox"})).getText()
#storytext = storytext.replace("\n\n","\n").strip()
#print("link 2")
#print("\n\n".join(storytext.split("\n")[0:4]))

#page = requests.get("https://www.asstr.org/~LS/stories/frankmccfoy5832.html")
#html = bs4.BeautifulSoup(page.content, 'html.parser')
#storytext = [paragraph.getText() for paragraph in (html.find("div",{"id": "target"}).find("div",{"id": "column"}).find_all("p"))]
#print("link 3")
#print("\n\n".join(storytext[0:4]))
