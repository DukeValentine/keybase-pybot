from .common import commonParser
from database import ops
from ripper.gelbooru import download_post,upload_post,fetch_post
import asyncio,aiohttp,bs4,re
background_url_regex = re.compile("url\((.*)\)")
postid_regex = re.compile("id=([0-9]+)")
from kb_commands import post

class gelbooruParser(commonParser):
    def __init__(self,url):
        super(gelbooruParser,self).__init__(url)
        self.post = None
        self.credentials = None
        self.post_id = None
        try:
            self.post_id = postid_regex.search( self.url).group(1)
        except AttributeError:
            pass
        
        
    async def get_credentials(self):
        if(self.db is None):
            print("No defined database")
            return 
        
        
        result = await ops.auth.gelbooru.get_credentials(self.db)
        if(result is None):
            print("failed to get credentials")
        else:
            self.credentials = result.get("credentials",None)
            
            

            
    async def configure_session(self,session):
        return "ok"
        
    async def generate_preview(self):
        if(self.path is None):
            return "no path specified"
        
        if(self.post_id is None):
            return "failed to extract postid from url"
        
        print("gelbooru")
        
        await self.get_credentials()
        
        if(self.credentials is None):
            return
        
        async with aiohttp.ClientSession() as session:
            self.post = await fetch_post(session, self.credentials['api_key'], self.credentials['userid'],self.post_id)
            
            if( (await download_post(self.db,session,self.post,self.path, context = "preview"))  in ["downloaded","skipped"] ):
                self.status = 0
        
    

    
    async def send_preview(self,team,channel):
        if(self.status != 0):
            print("Failed to generate preview. Can't send it")
            return 
        
        await upload_post(self.db,self.path,team,channel,self.post, context = "preview")
        
    
    
    




