from . import exhentai,asstr,common,gelbooru,danbooru,pixiv,twitter
from urllib.parse import urlparse
import asyncio


parsers = {
        "exhentai.org": exhentai.exhentaiParser,
        "asstr.org": asstr.asstrParser,
        "www.asstr.org": asstr.asstrParser,
        "gelbooru.com": gelbooru.gelbooruParser,
        "danbooru.donmai.us": danbooru.danbooruParser,
        "www.pixiv.net": pixiv.pixivParser,
        "twitter.com": twitter.twitterParser
        
    
}

def get_parser(url):
    parsed_url = urlparse(url)
    domain = parsed_url.netloc
    
    parser = parsers.get(domain,common.commonParser)
    return parser(url)
