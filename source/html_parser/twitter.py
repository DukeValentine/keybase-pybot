import requests,bs4,shlex,json,os
import asyncio,aiohttp
from urllib.parse import urlparse
from .common import commonParser
try:
    from kb_commands import post
except:
    from ..kb_commands import post

class twitterParser(commonParser):
    def __init__(self,url):
        super(twitterParser,self).__init__(url)
        self.preview_path = None
    
    
    async def download_tweet_attachment(self):
        process = await asyncio.create_subprocess_exec(*["youtube-dl","--print-json",self.url,"-o", os.path.join(self.path, "twitter_test", "%(display_id)s.%(ext)s")],stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
        stdout,stderr = await process.communicate()
        
        if(process.returncode != 0 ):
            params = shlex.split(f'gallery-dl --range 1 -o "extractor.twitter.filename"="{{tweet_id}}_{{num}}.{{extension}}" -o "extractor.base-directory"="{self.path}/twitter_test/" -o "extractor.directory"="" "{self.url}"')
            process = await asyncio.create_subprocess_exec(*params,stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
            stdout,stderr = await process.communicate()
            stdout = stdout.decode("utf-8").replace("#","").strip()
            paths = [path.strip() for path in stdout.split("\n")]
            return paths[0]
        
        stdout = stdout.decode('utf-8')
        result = json.loads(stdout)
        return os.path.join(self.path, "twitter_test",result['_filename'])

    async def generate_preview(self):
        if(self.path is None):
            return "no path specified"
        
        await asyncio.sleep(1)
        self.preview_path = await self.download_tweet_attachment()
        
    async def send_preview(self,team,channel):
        await post.send_chat_attachment(None,self.preview_path,team,channel,title= self.url)

    
async def main():
    return
    
if __name__ == "__main__":    
    asyncio.run(main())
