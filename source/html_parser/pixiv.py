from .common import commonParser
from database import ops
import asyncio,aiohttp,bs4,re
import pixivpy_async as pixiv_api
from ripper.pixiv import download_pixiv_post,upload_single_page
from kb_commands import post

try:
    from ripper.pixiv import download_pixiv_post,upload_single_page
    from kb_commands import post
except:
    from ..ripper.pixiv import download_pixiv_post,upload_single_page
    from ..kb_commands import post

postid_regex1 = re.compile("artworks/([0-9]+)")
postid_regex2 = re.compile("illust_id=([0-9])")

class pixivParser(commonParser):
    def __init__(self,url):
        super(pixivParser,self).__init__(url)
        self.post = None
        self.credentials = None
        self.post_id = None
        self.get_postid()
        
    def get_postid(self):
        if(postid_regex1.search(self.url) is not None):
            self.post_id = int(postid_regex1.search(self.url).group(1))
            return
        
        elif(postid_regex2.search(self.url) is not None) :
            self.post_id = int(postid_regex2.search(self.url).group(1))
            return
        
        
        
        
    async def get_credentials(self):
        if(self.db is None):
            print("No defined database")
            return 
        
        
        result = await ops.auth.pixiv.get_credentials(self.db)
        if(result is None):
            print("failed to get credentials")
            
        self.credentials = result
            
    async def generate_preview(self):
        await self.get_credentials()
        if(self.credentials is None):
            return 
        
        
        
        async with pixiv_api.PixivClient() as client:
            aapi = pixiv_api.AppPixivAPI(client=client)
            aapi.set_auth(self.credentials.get('access_token',None),self.credentials.get('refresh_token',None))
            try:
                username =  None if aapi.access_token else  self.credentials['username']
                password =  None if aapi.access_token else  self.credentials['password']
                user_info = await aapi.login(username,password)
            
            except pixiv_api.error.AuthCredentialsError as error:
                print("Failed to login with given credentials")
                
            self.post = (await aapi.illust_detail(self.post_id)).get("illust",None)
            result = await download_pixiv_post(self.db,aapi,"preview",self.post,self.path,context = "preview")
            print(result)
            if(result == "downloaded"):
                self.status = 0
                
    async def send_preview(self,team,channel):
        if(self.status != 0):
            print("Failed to generate preview. Can't send it")
            return 
        
        await upload_single_page(self.db,self.post_id,self.post['path'],self.post['filenames'][0],team,channel,context = "preview")
            
            
