from .common import commonParser
from database import ops
import asyncio,aiohttp,bs4,re
from ripper.danbooru import download_post,upload_post
post_regex = re.compile("(https://danbooru.donmai.us/posts/[0-9]+)")



class danbooruParser(commonParser):
    def __init__(self,url):
        super(danbooruParser,self).__init__(url)
        self.post = None
        
        
    async def get_credentials(self):
        if(self.db is None):
            print("No defined database")
            return 
        
        
        result = await ops.auth.danbooru.get_credentials(self.db)
        if(result is None):
            print("failed to get credentials")
        else:
            self.credentials = result.get("credentials",None)
        
        
    async def generate_preview(self):
        if(self.path is None):
            return "no path specified"
        
        await asyncio.sleep(2)
        
        post_url = post_regex.search(self.url)
        if(post_url is None):
            return
        
        post_url = f"{post_url.group(1)}.json"
        
        await self.get_credentials()
        
        if(self.credentials is None):
            return
        
        
        async with aiohttp.ClientSession(auth = aiohttp.BasicAuth (self.credentials['username'],self.credentials['api_key']) ) as  session:
            async with session.get(post_url) as response:
                self.post = await response.json()
                await download_post(self.db,session,"preview",self.post,self.path,target = "danbooru", context = "preview")
                self.status = 0
                
    async def send_preview(self,team,channel):
        await upload_post(self.db,self.path,team,channel,self.post,target = "danbooru", context = "preview")
