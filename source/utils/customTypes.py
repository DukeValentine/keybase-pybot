from collections import defaultdict
from .formatting import format_unit_si_base2

nested_dict = lambda: defaultdict(nested_dict)

class NoNoneDict(dict):
    def __setitem__(self, key, value):
        if key in self or value is not None:
            dict.__setitem__(self, key, value)
            
    def toString(self,type):
        str_repr = ""
        for key,value in self.items():
            value = format_unit_si_base2(value) if type == "size" and key == "total" else value
            
            str_repr += f"{key}  :  {value}         "
            
        return str_repr
