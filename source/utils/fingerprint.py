"""
Code modified from version created by Acrimon at https://git.nebulanet.cc/Acrimon/image-meta-tools


"""
import asyncio,aiofiles
from PIL import Image
import imagehash
import sys
import os
import hashlib
from collections import defaultdict
import csv
from bitarray import bitarray,bitdiff,util

FINGERPRINT_ENTROPY = 128

def similarity_fingerprints(fingerprint1, fingerprint2):
    ba1 = util.hex2ba(fingerprint1)
    ba2 = util.hex2ba(fingerprint2)
    
    difference_bits = bitdiff(ba1,ba2)
    difference_percentage = difference_bits/FINGERPRINT_ENTROPY
    
    return 100 * (1 - difference_percentage)

def extract_filename(path):
    return os.path.split(path)[1].split(".")[0]


async def generate_fingerprint_directory(path, output_filename = "output.csv",filter_filenames = False):
    if(os.path.isfile(path)): return
    
    process = await asyncio.create_subprocess_exec("walk-image-fingerprint", path, output_filename,stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    stdout,stderr = await process.communicate()
    if process.returncode == 0:
        with open(output_filename,"r") as file:
            reader = csv.DictReader(file)
            fingerprints = defaultdict(list)
            for item in reader:
                value = extract_filename(item['path']) if filter_filenames else item['path'] 
                fingerprints[item['fingerprint']].append(value)
                
            return fingerprints


async def generate_fingerprint_file(path, output_filename = "output.csv"):
    if(os.path.isdir(path)):
        return
    
    process = await asyncio.create_subprocess_exec("walk-image-fingerprint", path, output_filename,stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    stdout,stderr = await process.communicate()
    
    if process.returncode == 0:
        with open(output_filename,"r") as file:
            reader = csv.DictReader(file)
            content = [item for item in reader]
            fingerprint = content[0]['fingerprint']
            return str(fingerprint)
            
    
