from datetime import datetime
from engineering_notation import EngNumber




def format_unit_si_base2(value):
    try:
        return f"{EngNumber(value)}B"
    except ValueError:
        return value
        
    
    


def format_inactive_user_info(team,user,reference_date,excluded = []):
    inactive_period = reference_date - user['team_activity'][team]['latest']
    if(user['most_recent_post_date'] <= datetime.utcfromtimestamp(0)):
        last_post = "No post found"
        inactive_interval = "No inactivity interval info"
    else:
        last_post = user['most_recent_post_date'].ctime()
        inactive_interval = f"{inactive_period.days} days, {inactive_period.seconds/3600:.1f} hours inactive"
    
    return f"{user['username']} : Last post at {last_post} | {inactive_interval} [Is excluded? {user['username'] in excluded}]"


def format_inactive_user_warning(team,user,reference_date,days_of_inactivity):
    inactive_period = reference_date - user['most_recent_post_date']
    
    if(user['most_recent_post_date'] <= datetime.utcfromtimestamp(0)):
        last_post = "No post or comment found"
        inactive_interval = "No inactivity interval info"
    else:
        last_post = user['most_recent_post_date'].ctime()
        inactive_interval = f"{inactive_period.days} days, {inactive_period.seconds/3600:.1f} hours inactive"
    
    return f"Beep Boop, I am a bot.This is an automated message with a moderator added in the conversation. You're receiving a warning due to inactivity at {team}.\r\nYour last post was at : {last_post} . Inactive interval : {inactive_interval}\r\nThe maximum inactivity interval allowed is of {days_of_inactivity} days\r\nIf you stay inactive you'll be soon be removed from the team. If that happens, or if you want to explain you situation, it's suggested that you reply here or talk directly to the modteam"


def format_inactive_user_removal(team,user,reference_date,days_of_inactivity):
    inactive_period = reference_date - user['most_recent_post_date']
    
    if(user['most_recent_post_date'] <= datetime.utcfromtimestamp(0)):
        last_post = "No post or comment found"
        inactive_interval = "No inactivity interval info"
    else:
        last_post = user['most_recent_post_date'].ctime()
        inactive_interval = f"{inactive_period.days} days, {inactive_period.seconds/3600:.1f} hours inactive"
    
    return f"Beep Boop, I am a bot.This is an automated message with a moderator added in the conversation. You're being removed from {team} due to inactivity.\r\nYour last post was at : {last_post} . Inactive interval : {inactive_interval}\r\nThe maximum inactivity interval allowed is of {days_of_inactivity} days\r\nIf you wish to reapply or to explain you situation, it's suggested that you reply here or talk directly to the modteam"
