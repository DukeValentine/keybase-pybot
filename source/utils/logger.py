import asyncio
import logging
import os
import datetime
import sys


VERBOSE_LEVEL = 15
LOCAL_TIMEZONE = datetime.datetime.now(datetime.timezone(datetime.timedelta(0))).astimezone().tzinfo
pybot_logger = None


LEVELS = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL,
    'verbose' : VERBOSE_LEVEL
}

async def info(msg, logger = None):
    if(logger is None):
        logger = pybot_logger
        
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, logger.info,msg)

async def debug(msg, logger = None):
    if(logger is None):
        logger = pybot_logger
        
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, logger.debug,msg)

async def warning(msg, logger = None):
    if(logger is None):
        logger = pybot_logger
    
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, logger.warning,msg)

async def error(logger, msg):
    if(logger is None):
        logger = pybot_logger
        
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, logger.error,msg)

async def critical(msg, logger = None):
    if(logger is None):
        logger = pybot_logger
        
    loop = asyncio.get_event_loop()
    await loop.run_in_executor(None, logger.critical,msg)





def verbose(self, message, *args, **kws):
    if self.isEnabledFor(VERBOSE_LEVEL):
        # Yes, logger takes its '*args' as 'args'.
        self._log(VERBOSE_LEVEL, message, args, **kws)


async def initialize_logger(logging_level):
    global pybot_logger
    
    logger = logging.getLogger('KEYBASE')
    logging.basicConfig(level=logging_level,format = "[%(asctime)s" + f"][UTC{LOCAL_TIMEZONE}" +  "][%(levelname)-8s]  %(message)s" , datefmt = "%H:%M:%S")
    
    logging.addLevelName(VERBOSE_LEVEL,"VERBOSE")
    
    
    logging.Logger.verbose = verbose
   
    pybot_logger = logger
    
    

