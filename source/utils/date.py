from datetime import datetime 
from datetime import timedelta

def format_date(date):
    if isinstance(date,int):
        try:
            formatted_date = datetime.utcfromtimestamp(date)
            
        except ValueError as e:
            return datetime.utcfromtimestamp(date/1000.0)
        
        else:
            formatted_date
            
    if date is not None:
        return date.replace(microsecond=0)
    
    else:
        return datetime.utcfromtimestamp(0)
    

def date_ctime(date):
    if date is not None and isinstance(date,datetime):
        return date.ctime()
    
    else:
        return "None"




def get_datetime_from_delta(time_unit,time_value):
    time_in_hours = {
        'm' : 0,
        'h' : 1,
        'd' : 24
        }
    
    try:
    
        hour_delta = int(time_value) * time_in_hours[time_unit]
        date = format_date(datetime.now()) - timedelta(hours = hour_delta)
    
    except KeyError as e:
        return None
    
    else:
        return date
    
    
def get_delta_from_datetime(date):
    if(date is None):
        date = datetime.fromtimestamp(0)
    if isinstance(date,int):
        date = datetime.utcfromtimestamp(date)
    
    return(format_date(datetime.now())  - format_date(date)).days



def get_days_of_inactivity(weeks,days):
    DAYS_IN_WEEK = 7
    
    return(weeks*DAYS_IN_WEEK + days)
    
    
    
    
