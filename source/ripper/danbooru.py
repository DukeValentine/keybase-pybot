import os
import json
from collections import defaultdict,Counter
from time import sleep
from datetime import datetime
import utils.logger as logger
import subprocess
import shlex
import asyncio
import aiohttp
import aiofiles
try:
    from database import ops as db_operations
    from kb_commands import post,keybase

except:
    from ..database import ops as db_operations
    from ..kb_commands import post,keybase


BASE_URL = "https://danbooru.donmai.us"
FAVGROUP_ENDPOINT =  "favorite_groups.json"
POST_ENDPOINT = "posts.json"

DANBOORU_TARGETS = ["danbooru","atf"]


base_urls = {
    "danbooru": "https://danbooru.donmai.us",
    "atf": "https://booru.allthefallen.moe"
    }

current_dir = os.getcwd()

async def upload_post(db,path,team,channel,post_info,target = "danbooru", context = "ripper", dryrun = False):
    title = f"{target}/post/{post_info['id']}" if context == "preview" else None
    
    if(dryrun is False): await post.send_chat_attachment(path,post_info['filename'],team,channel,title = title)
    
    
    await db_operations.ripper.mark_danbooru_post_as_uploaded(db,post_info['id'],post_info['md5'],team,channel,target)
    await db_operations.events.upload(db,post_info['id'],post_info['filename'],team,channel,datetime.utcnow(),target,context)
    
    

async def download_post_timeout( db,session,favgroup,post_info,path,target = "danbooru", context = "ripper", dryrun = False):
    timeout = 5 if dryrun else 60
    
    return await asyncio.wait_for(download_post(db,session,favgroup,post_info,path,target , context , dryrun),timeout = timeout)
 
async def download_post( db,session,favgroup,post_info,path,target = "danbooru", context = "ripper", dryrun = False):    
    post_info['tag'] = favgroup
    

    try:
        if("ugoira" in post_info['tag_string'] or "ugoira" in post_info['source']):
            url = post_info['large_file_url']
            ext = url.split(".")[-1]
        else:
            url = post_info['file_url']
            ext = post_info['file_ext']
            

        post_info['path'] = path
        filename = f"{post_info['md5']}.{ext}"
        post_info['filename'] = filename
        

        
       
        if(context == "ripper" and (await db_operations.ripper.check_if_danbooru_post_downloaded(db,post_info['id'],post_info['md5'],target) is False)  ):
            try:
                os.makedirs(path)
            except OSError as error:
                pass
                
            async with session.get(url) as response:
                
                if response.status == 200:
                    try:
                        
                        if(dryrun is False):
                            image = await aiofiles.open(os.path.join(path,filename), 'wb')
                            await image.write(await response.read())
                            await image.close()
                            
                            
                        
                        await db_operations.ripper.insert_danbooru_post(db,post_info,downloaded = True,target = target, context= context)
                        return "downloaded"
                        
                    except OSError as error:
                        print(error)
                        await db_operations.ripper.insert_danbooru_post(db,post_info,downloaded = False,target = target, context= context)
                        return "error"
                else:
                    await db_operations.ripper.insert_danbooru_post(db,post_info,downloaded = False,target = target, context= context)
                    return "error"
                
        else:
            return "skipped"
            
    except KeyError as error:    
        print("failure to parse post")
        print(error)
        return "error"
    
   
    
    
async def update_posts_favgroup(db, favgroup,target):
    await db_operations.ripper.append_favgroup_to_posts(db,favgroup['post_ids'],favgroup['name'],target)
    return

async def update_posts_favgroups(db,session,login,target):
    update_url = "/".join([base_urls[target], FAVGROUP_ENDPOINT])
    params = {'search[creator_name]': login}
    
    async with session.get(update_url, params = params, headers = {"Content-Type": "application/json"}) as response:
        if(response.status == 200):
            favgroups = await response.json()
            tasks = asyncio.gather(*[update_posts_favgroup(db,favgroup) for favgroup in favgroups])
            await tasks
        
    
    return




        
async def fetch_favgroup_page(semaphore,db,session,favgroup,fav_url,fav_info,page,path,target = "danbooru", dryrun = False):
    fav_info['page'] = page
    max_attempts = 3
    batch_size = 25
    
    for attempt in range(0,max_attempts):
        try:
            
            async with session.get(fav_url,params = fav_info,headers = {"Content-Type": "application/json"}) as response:
                try:
                    if response.status == 200:
                        json_response = json.loads(await response.text())
                        result = Counter()
                        count = 0
                        
                        while( len(json_response[count:count+batch_size]) > 0):
                            tasks = asyncio.gather(*[download_post_timeout(db,session,favgroup,post,path,target,"ripper",dryrun) for post in json_response[count:count+batch_size]])
                            
                            try:
                                result_tasks = await tasks
                            
                            except asyncio.TimeoutError:
                                print("timeout")
                                await asyncio.sleep(3)
                                
                            else:
                                result += Counter(result_tasks)
                                count += batch_size
                                await asyncio.sleep(1)
                            
                        return result
                    
                    else:
                        print( f"Received {response.status} when accessing {fav_url}")
                        await db_operations.log.events.general(db, type = "CONNECTION", name = target,date = datetime.utcnow(),level = logger.LEVELS['error'],event_message = f"Received {response.status} when accessing {fav_url}",context = {"status": response.status, "header": response.headers, "response": await response.text()})
                        return Counter()
                
            
                    
                
                
                except KeyError as error:
                    print("failure to parse response")
                    return
                    
                except (aiohttp.client_exceptions.ServerDisconnectedError,aiohttp.client_exceptions.ClientPayloadError):
                    await asyncio.sleep(2)
                    
        except aiohttp.client_exceptions.ClientConnectorError as error:
            print(error)
            await db_operations.log.events.general(db, type = "CONNECTION", name = target,date = datetime.utcnow(),level = logger.LEVELS['error'],event_message = f"Received {error} when accessing {fav_url}",context = {"error": error.__dict__})
            return Counter()
    
    return Counter()
        
    

async def fetch_favgroup(db,credentials,path,favgroup,other_tags = None,target = "danbooru",dryrun = False):
    if(favgroup is None):
        return Counter()
    
    login = credentials['username']
    api_key = credentials['apikey']
    
    fav_url = "/".join([base_urls[target],POST_ENDPOINT])
    print(fav_url)
    favgroup_formatted =  f"favgroup:{favgroup}"
    POSTS_PER_PAGE = 200
    
    fav_info = {"tags": favgroup_formatted,"limit": POSTS_PER_PAGE}
    if(other_tags):
        fav_info['tags'] += f" {other_tags}"
    
    print(fav_info)
    total_counter = Counter()
    semaphore = asyncio.BoundedSemaphore(15)
    
    async with aiohttp.ClientSession(auth = aiohttp.BasicAuth (login,api_key)) as session:
        post_count = POSTS_PER_PAGE
        page = 1
        while post_count != 0:
            print(f"page {page}")
            
            try:
                page_counter = await fetch_favgroup_page(semaphore,db,session,favgroup,fav_url,fav_info,page,path,target,dryrun)
            except asyncio.TimeoutError:
                print("timeout page")
                await asyncio.sleep(3)    
            else:
                post_count = page_counter['downloaded'] if dryrun is False else (page_counter['downloaded'] + page_counter['skipped'])
                
                
                total_counter += page_counter
                page+=1
    
    return total_counter






async def upload_to_keybase(db,favgroup,team,channel,target = "danbooru",dryrun = False):
    
    async for danbooru_post in await db_operations.ripper.get_downloaded_danbooru_posts(db,favgroup,team,channel,uploaded_to_chat = False,target = target):
        try:
            if(dryrun == False): 
                await post.send_chat_attachment(danbooru_post['path'],danbooru_post['filename'],team,channel,title = None)
            await db_operations.ripper.mark_danbooru_post_as_uploaded(db,danbooru_post['id'],danbooru_post['md5'],team,channel,target)
            await db_operations.events.upload(db,danbooru_post['id'],danbooru_post['filename'],team,channel,datetime.utcnow(),source = target)
            
        except keybase.KeybaseAPIError as error:
            await db_operations.log.events.upload(db,danbooru_post['id'],danbooru_post['filename'],team,channel,datetime.utcnow(),source = target, success = False, context = {"error": error.message, "input": error.api_input,"args": error.api_args})
            print(f"Failed to upload {danbooru_post['filename']} to {team}#{channel}\n{error}")


    
    


def create_parser():
    import argparse
    parser = argparse.ArgumentParser(description = "danbooru test cli\n", prog = "danbooru_cli")
    parser.add_argument("api_key")
    return parser.parse_args()
    
    
if __name__ == "__main__":
    parser = create_parser()
    api_key = parser.api_key
    print(api_key)
    asyncio.run(fetch_favgroup("daily",api_key))
