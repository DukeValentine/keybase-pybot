import os
import json
from collections import defaultdict,Counter
from datetime import datetime



import shlex
import asyncio
import aiohttp
import aiofiles
import pixivpy_async as pixiv_api
try:
    from database import ops as db_operations
    from kb_commands import post,keybase

except:
    from ..database import ops as db_operations
    from ..kb_commands import post,keybase


POSTS_PER_PAGE = 30


async def download_pixiv_post(db,aapi,favtag,pixiv_post,path,context = "ripping",dryrun = False):
    pixiv_post['tag'] = favtag
    
    
    if(context == "ripping" and (await db_operations.ripper.check_if_pixiv_post_downloaded(db,pixiv_post['id']) is True)):
        return "skipped"
    
    
    try:
        os.makedirs(path)
    except OSError as error:
        pass
    
    
    
    if(pixiv_post['type'] == "ugoira"):
        print("ugoira!")
        
        ugoira_path,filename = await download_ugoira(aapi,pixiv_post['id'],path)
        pixiv_post['filenames'] = [filename]
        pixiv_post['path'] = ugoira_path
        if(ugoira_path is None):
            pixiv_post['filenames'] = None
            await db_operations.ripper.insert_pixiv_post(db,pixiv_post, downloaded = False, context=context)
            return "error"
        
    else:
        try:
            urls = [pixiv_post['meta_single_page']['original_image_url']]
            filename = pixiv_post['meta_single_page']['original_image_url'].split("/")[-1]
            pixiv_post['filenames'] = [filename]
            
        except KeyError as error:
            urls =   [image['image_urls']['original']  for image in  pixiv_post['meta_pages'] ]
            filenames =  [image['image_urls']['original'].split("/")[-1]  for image in  pixiv_post['meta_pages'] ]
            pixiv_post['filenames'] = filenames

        
        
        pixiv_post['path'] = path
        
        for url in urls:
            try:
                if(dryrun == False):
                        await aapi.download(url,path = path,fname = url.split("/")[-1])
                else:
                    asyncio.sleep(1)
            
            except pixiv_api.error.PixivError as error:
                print(f"Download failed for {pixiv_post['id']}")
                print(error)
                await db_operations.ripper.insert_pixiv_post(db,pixiv_post, downloaded = False, context=context)
                return "error"
    
    await db_operations.ripper.insert_pixiv_post(db,pixiv_post, downloaded = True, context=context)
    return "downloaded"




async def fetch_favtag(db,credentials,favtag,path,dryrun = False):
    if(favtag is None):
        return Counter()
    
    async with pixiv_api.PixivClient() as client:
        aapi = pixiv_api.AppPixivAPI(client=client)
        aapi.set_auth(credentials.get('access_token',None),credentials.get('refresh_token',None))
        
        total_counter = Counter()
        try:
            username =  None if aapi.access_token else  credentials['username']
            password =  None if aapi.access_token else  credentials['password']
            user_info = await aapi.login(username,password)
            
        except pixiv_api.error.AuthCredentialsError as error:
            print("Failed to login with given credentials")
            return total_counter
        
        await db_operations.auth.pixiv.set_tokens(db,aapi.access_token,aapi.refresh_token)
        user_id = user_info['response']['user']['id']
        print(f"pixiv {favtag}")
        
        
        
        
        results = await aapi.user_bookmarks_illust(user_id,tag = favtag)
        next_url =  results.next_url
        tasks = asyncio.gather(*[download_pixiv_post(db,aapi,favtag,pixiv_post,path,dryrun) for pixiv_post in results.get('illusts',[])])
        result = Counter(await tasks)
        total_counter += result
        
        if(result['downloaded'] == 0):
            return result
        
        while(next_url is not None):
            results = await aapi.user_bookmarks_illust(user_id,tag = favtag,max_bookmark_id = aapi.parse_qs(results.next_url).get('max_bookmark_id',None))
            tasks = asyncio.gather(*[download_pixiv_post(db,aapi,favtag,pixiv_post,path,dryrun) for pixiv_post in results.get('illusts',[])])
            next_url =  results.next_url 
            
            result = Counter(await tasks)
            total_counter += result
            
            if(result['downloaded'] == 0):
                return total_counter 
            
        return total_counter   
    
    
async def upload_single_page(db,post_id,path,filename,team,channel,context = "ripping"):
    await post.send_chat_attachment(path,filename,team,channel,title = None)
    await db_operations.events.upload(db,post_id,filename,team,channel,datetime.utcnow(),source = "pixiv", context = context)
    
            
async def upload_to_keybase(db,tag,team,channel,dryrun = False):
    success = 0
    failure = 0
    
    async for pixiv_post in await db_operations.ripper.get_downloaded_pixiv_posts(db,tag,team,channel,uploaded_to_chat = False):
        try:
            for filename in pixiv_post.get("filenames",None):
                if(dryrun == False): await upload_single_page(db,pixiv_post['id'],pixiv_post['path'],filename,team,channel)
                
            await db_operations.ripper.mark_pixiv_post_as_uploaded(db,pixiv_post['id'],team,channel)
            success += 1
            
        except keybase.KeybaseAPIError as error:
            await db_operations.events.upload(db,pixiv_post['id'],filename,team,channel,datetime.utcnow(),source = "pixiv", success = False, context = {"error": error.message, "input": error.api_input,"args": error.api_args})
            print(f"Failed to upload {pixiv_post['filenames']} to {team}#{channel}\n{error}")
            failure += 1
            
        except KeyError as error:
            print(f"Failed to upload {pixiv_post['filenames']} to {team}#{channel}\n{error}")
            failure += 1
            
    return Counter(success=success,failure=failure)


async def download_ugoira(aapi,pixiv_id,directory):
    ugoira = await aapi.ugoira_metadata(pixiv_id)
    
    ugoira_metadata = ugoira['ugoira_metadata']['frames']
    ugoira_path = os.path.join(directory,f"{pixiv_id}")
    
    try:
        os.makedirs(ugoira_path)
    except OSError:
        pass
    
    async with aiofiles.open(os.path.join(ugoira_path, "input.txt"),"w") as file:
        await file.write(format_ffmpeg_demuxer_info(ugoira_metadata))
        
    zip_url = ugoira['ugoira_metadata']['zip_urls']['medium']
    await aapi.download(zip_url, path = ugoira_path, name = f"{pixiv_id}.zip")
    
    if (await unzip_file(ugoira_path,f"{pixiv_id}.zip") == 0):
        video_path = os.path.join(ugoira_path,f'{pixiv_id}.mp4')
        if( (await create_video_from_ugoira(ugoira_path,video_path) == 0 )):
            return ugoira_path,f'{pixiv_id}.mp4'
        
    return None,None
        
    
    
async def unzip_file(path,filename):
    process = await asyncio.create_subprocess_exec(*["unzip", "-o", os.path.join(path,filename),"-d", path],stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    result = await process.communicate()
    return process.returncode

    
def format_ffmpeg_demuxer_info(ugoira_metadata):
    ffmpeg_info = ""
    for file in ugoira_metadata:
        ffmpeg_info += f"file '{file['file']}'\nduration {file['delay']/1000}\n"
        
    ffmpeg_info += f"file '{ugoira_metadata[-1]['file']}'"
    return ffmpeg_info 

async def create_video_from_ugoira(ugoira_path,video_path):
    params = f"ffmpeg -f concat -i {os.path.join(ugoira_path,'input.txt')} -y -vsync vfr vfr -c:v libx264  {video_path}"
    
    process = await asyncio.create_subprocess_exec(*shlex.split(params),stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    result = await process.communicate()
    return process.returncode
