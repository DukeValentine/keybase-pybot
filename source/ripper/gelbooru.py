import os
import json
from collections import defaultdict,Counter
from datetime import datetime
from database import ops as db_operations
from kb_commands import post,keybase
import utils.logger as logger
import subprocess
import shlex
import asyncio
import aiohttp
import aiofiles


base_url = "https://gelbooru.com"
post_endpoint = "index.php?page=dapi&s=post&q=index"


def format_api_auth(api_key, userid):
    return f"&api_key={api_key}&user_id={userid}"

def format_post_api_request(api_key,userid, post_id):
    return f"{base_url}/{post_endpoint}{format_api_auth(api_key,userid)}&id={post_id}&json=1"

async def upload_post(db,path,team,channel,post_info, context = "preview"):
    title = f"gelbooru/post/{post_info['id']}" if context == "preview" else None
    
    await post.send_chat_attachment(path,post_info['image'],team,channel,title = title)
    await db_operations.ripper.mark_danbooru_post_as_uploaded(db,post_info['id'],post_info['hash'],team,channel,"gelbooru")
    await db_operations.events.upload(db,post_info['id'],post_info['image'],team,channel,datetime.utcnow(),"gelbooru",context)

async def download_post(db,session,post_info,path, context = "preview"):
    if(post_info is None):
        return "error"
    
    
    if(await db_operations.ripper.check_if_danbooru_post_downloaded(db,post_info['id'],post_info['hash'],"gelbooru") is False):
        post_info['path'] = path
        
        try:
            os.makedirs(path)
        except OSError as error:
            pass
            
        async with session.get(post_info['file_url']) as response:
            if response.status == 200:
                try:
                    image = await aiofiles.open(os.path.join(path,post_info['image']), 'wb')
                    await image.write(await response.read())
                    await image.close()
                    await db_operations.ripper.insert_danbooru_post(db,post_info,downloaded = True,target = "gelbooru", context= context)
                    return "downloaded"
                    
                except OSError as error:
                    print(error)
                    await db_operations.ripper.insert_danbooru_post(db,post_info,downloaded = False,target = "gelbooru", context= context)
                    return "error"
            else:
                await db_operations.ripper.insert_danbooru_post(db,post_info,downloaded = False,target = "gelbooru", context= context)
                return "error"
            

        
    else:
        return "skipped"
    
    
    

async def fetch_post(session,api_key,userid,post_id):
    async with session.get(format_post_api_request(api_key,userid,post_id)) as response:
        if(response.status == 200):
            result = await response.json()
            post = result[0]
            return post
            
            
        else:
            print(f"failed to get post error {response.status}")
            return
    
    
    

