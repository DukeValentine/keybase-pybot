try:
    from database import ops,pybot_db
    import utils
except:
    from ..database import ops,pybot_db
    from ..database.ops import log
    from .. import utils

import os,re
import json
from collections import defaultdict,Counter
from time import sleep
from datetime import datetime
import subprocess
import shlex
import asyncio
import aiohttp,bs4
import aiofiles 
import asyncio
from pyppeteer import launch

LOGIN_COOKIE_LIST = ["__cfduid","csrftoken","sessionid"]
LOGIN_URL = 'https://nhentai.net/login'
IMAGE_URL = 'https://i.nhentai.net/galleries'
API_URL = "https://nhentai.net/api/gallery"

page_regex = re.compile("\?page=([0-9]+)")
media_id_favregex = re.compile("nhentai.net/galleries/([0-9]+)")
RATELIMIT_DELAY = 0.5
PAGE_INFO_CONCURRENCY_LIMIT = 3

page_ext = {'j': 'jpg', 'g':"gif", 'p': 'png'}

async def get_nhentai_login_cookies(db,credentials):
    print("No session id found, logging in to nhentai...")
    browser = await launch({"args": ['--no-sandbox', '--disable-setuid-sandbox']})
    page = await browser.newPage()
    
    await page.goto(LOGIN_URL)
    
    await page.type("input#id_username_or_email",credentials['username'])
    await page.type("input#id_password", credentials['password'])
    await page.click("button.button.button-wide")
    
    cookies = await page.cookies()
    
    login_cookies = dict()
    for item in cookies:
        if item['name'] in LOGIN_COOKIE_LIST:
            login_cookies[item['name']] = item['value']
    
    await ops.auth.nhentai.update_cookies(db,login_cookies)
    return login_cookies
    print("...done")
    await browser.close()




async def fetch_doujin_info(session,gallery_id):
    url = f"{API_URL}/{gallery_id}"
    async with session.get(url) as response:
        if(response.status == 200):
            return await response.json()
    
    print(f"got {response.status} from {url}")
    return


async def _semaphore_fetch_doujin_info(semaphore,session,gallery_id):
    max_attempts = 3
    delay_increase_factor = 2
    
    async with semaphore:
        for attempt in range(0,max_attempts):
            result = await fetch_doujin_info(session,gallery_id)
            await asyncio.sleep(RATELIMIT_DELAY + (delay_increase_factor*attempt))
            
            if(result): break
            
            
        return result
    
    

    

async def get_favpage_count(session):
    async with session.get(f"https://nhentai.net/favorites/") as response:
        if(response.status == 200):
            html = bs4.BeautifulSoup(await response.text(),'html.parser')
            last_page = html.find("a", {"class": "last"})
            if(last_page):
                return int(page_regex.match(last_page.get("href", "?page=0")).group(1))
            

def extract_media_id(item):
    return  media_id_favregex.search(item.find("img").get("data-src","nhentai.net/galleries/0")).group(1)
            

async def fetch_galleryids_favpage(session,page):
    async with session.get(f"https://nhentai.net/favorites/?{page=}") as response:
        if(response.status == 200):
            html = bs4.BeautifulSoup(await response.text(),'html.parser')
            gallery_favorites =  html.find_all("div", {"class": "gallery-favorite"})
            gallery_ids = [item.get("data-id",None) for item in gallery_favorites]
            gallery_ids = [str(item) for item in gallery_ids if item]
            
            return gallery_ids
            
    
    return 


async def download_doujin_cover(session,doujin_info,cover_path):
    galleryid = doujin_info['id']
    media_id = doujin_info['media_id']
    cover_ext = doujin_info['images']['pages'][0]['t']
    cover_ext = page_ext[cover_ext]
    url = f"{IMAGE_URL}/{media_id}/1.{cover_ext}"
    async with session.get(url) as response:
        async with aiofiles.open(os.path.join(cover_path,f"{galleryid}.{cover_ext}"), "wb") as image:
            await image.write(await response.read())
           
            
async def _semaphore_dowload_doujin_cover(semaphore,session,doujin_info,cover_path):
    async with semaphore:
        result = await download_doujin_cover(session,doujin_info,cover_path)
        await asyncio.sleep(RATELIMIT_DELAY)
        
    
    

async def fetch_page_doujin_info(db,session,page,cover_path):
    gallery_ids = await fetch_galleryids_favpage(session,page)
    gallery_ids = await ops.log.ripper.get_doujins_not_in_db(db,gallery_ids)
    
    semaphore = asyncio.BoundedSemaphore(PAGE_INFO_CONCURRENCY_LIMIT)
    tasks = asyncio.gather(*[_semaphore_fetch_doujin_info(semaphore,session,gallery_id) for gallery_id in gallery_ids])
    doujins = await tasks
    
    
    coroutines = [_semaphore_dowload_doujin_cover(semaphore,session,doujin,cover_path) for doujin in doujins]
    coroutines += [ops.log.ripper.insert_nhentai_doujin(db,doujin) for doujin in  doujins]
    tasks = asyncio.gather(*coroutines)
    await tasks
    return len(gallery_ids)



async def configure_cookies(session,db):
    
    
    cookie_jar = session._cookie_jar._cookies
    credentials = await ops.auth.nhentai.get_credentials(db)
    
        

    if(credentials):
        if(credentials.get('sessionid',None) is None):
            await get_nhentai_login_cookies(db,credentials)
            credentials = await ops.auth.nhentai.get_credentials(db)
        
        cookie_jar['.nhentai.net']["__cfduid"] = credentials["__cfduid"]
        cookie_jar['nhentai.net']['csrftoken'] = credentials['csrftoken']
        cookie_jar['nhentai.net']['sessionid'] = credentials['sessionid']
        return cookie_jar
    


async def generate_cover_fingerprints(db,cover_path = "/tmp/nhentai_covers"):
    doujin_fingerprints = await utils.fingerprint.generate_fingerprint_directory(cover_path,filter_filenames=True)
    for key,values in doujin_fingerprints.items():
        doujin_fingerprints[key] = sorted(values,reverse=True)
        
    await ops.log.update_doujin_fingerprints(db,doujin_fingerprints)
    #rm_process = await asyncio.create_subprocess_exec("rm","-r", cover_path)
        
    




async def update_favorites(db, cover_path = "/tmp/nhentai_covers"):
    async with aiohttp.ClientSession() as session:
        if(await configure_cookies(session,db) is None):
            print("failed to get config")
            return
       
        page_count = await get_favpage_count(session)
        for page in range(1,page_count):
            print(page)
            fetch_count = await fetch_page_doujin_info(db,session,page,cover_path)
            if(fetch_count == 0):
                break
            
            await asyncio.sleep(RATELIMIT_DELAY)
            
    await generate_cover_fingerprints(db,cover_path)
    await ops.log.update_similar_doujins(db)
    return

async def fetch_favorites(db,cover_path = "/tmp/nhentai_covers", limit = 10):
    await update_favorites(db,cover_path)
    

if __name__ == "__main__":
    
    db = pybot_db.connect_database()
    loop = asyncio.get_event_loop()
    loop.run_until_complete( fetch_favorites(db))
    
    
    
    
    
