from collections import defaultdict
from collections import Counter
from datetime import datetime
import motor.motor_asyncio
import pymongo
import asyncio
import re

import logging

async def set_last_page_channel(db,team,channel,page):
    if(db is not None):
        await db.posts.page_info.insert_one({'team' : team,'channel' : channel,'page': page});
        

async def update_user_field(db,username):
    return

async def get_users(db):
    return db.users.find({}, projection = {'username' : True})

async def get_user_memberships(db,user,team_name):
    result = await db.users.find_one({'username':user},projection = {'teams':True})
    teams = result['teams']
    teams = [team for team in teams if team_name in team]
    
    return teams
        
        
async def get_team_attachment_count_by_mimetype(db,team):
    return 

async def get_allteams_attachment_count_by_mimetype(db,team):
    return

async def get_user_received_reaction_count(db,username):
    return

async def get_user_given_reaction_count(db,username):
    return      


async def insert_keybase_post(db,post):
    if(post is None):
        return None
    
    return await db.posts.insert_one(post)
        
async def get_posts_channel_type(db,team,channel,type):
    result = db.posts.find({'team' : team,'channel' : channel,'content.type' : type})
    return await result.to_list(length = None)


async def get_posts_subteam_type(db,subteam,type):
    result = db.posts.find({'team' : subteam,'content.type' : type})
    return await result.to_list(length = None)

async def get_posts_subteams(db,team,type):
    return

async def get_teams(db):
    result = db.teams.find({})
    return result

async def get_all_configs(db):
    return db.config.find({})

async def get_database_config(db):
    return await db.config.find_one({"type" : "db"})

async def get_ripping_config(db):
    return await db.config.find_one({"type" : "ripping"})

async def update_runtime_stats(db):
    stats = await db.pybot_stats.find_one({"runtime" : {"$exists": True}})
    if(stats['runtime'].get("all",None) is None):
        all_runs = [stats['runtime']['first_run'],stats['runtime']['last_run']]
        await db.pybot_stats.update_one({"runtime" : {"$exists": True}}, {"$set": {"runtime.all_runs" : all_runs} })
        
    last_run = datetime.utcnow()
    await db.pybot_stats.update_one({"runtime" : {"$exists": True}}, {"$set": {"runtime.last_run" : last_run}})
    await db.pybot_stats.update_one({"runtime" : {"$exists": True}}, {"$push": {"runtime.all_runs" : last_run}})
    

async def log_event_general(db, type, name,date,level,event_message,context = None):
    await db.log.events.insert_one({"type": type.upper(),"level":level,"name":name.upper(),"date":date, "message": event_message,"context": context})
    return

async def log_event_upload(db,post_id,filename, team, channel,date,source = "unknown", success = True, context = None):
    await db.log.events.insert_one({"type": "UPLOAD","level": logging.INFO,"filename": filename,"post_id": post_id , "team":team, "channel": channel,"date": date, "source": source, "success": success, "context": context})
    return 

async def log_event_download(db,post_id,filename,directory,date,source = "unknown", success = True, context = None):
    await db.log.events.insert_one({"type": "DOWNLOAD","level": logging.INFO,"post_id": post_id ,"filename": filename, "directory":directory,"date": date, "source": source, "success": success, "context": context})
    return
    
async def log_event_ripping_results(db, source, rip_counter):
    return

async def get_saucenao_api_key(db):
    result = await get_database_config(db)
    return result['db'].get("saucenao_api_key", None)

async def get_temp_directory(db):
    result = await get_database_config(db)
    return result['db'].get("temp_directory", None)

async def update_channel_name(db,old_name,new_name,team):
    result = await db.teams.find_one({"name": team})
    current_channels = result['channels']
    try:
        index = current_channels.index(old_name)
    except ValueError as e:
        print("channel not found")
        return None
    else:
        current_channels[index] = new_name
        result = db.posts.update_many({"channel": old_name},{"$set" : {"channel" : new_name}})
    
    
    return

async def update_url_page(db,url,ripcount):
    
    
    rip_stats = await db.pybot_stats.ripcount.find_one({"url" : url})
    current_count = 0
    
    if(rip_stats is not None):
        if(ripcount == 0):
            return
        await db.pybot_stats.ripcount.update_one({'url' : url},{"$inc" : {"count" : ripcount}})
        
    else:
        await db.pybot_stats.ripcount.insert_one({'url' : url, 'count' : ripcount})
         
    #async for document in db.pybot_stats.ripcount.find({'url' : url}):
        #print(document)
        
async def get_ripcount_for_page(db,url):
    result = await db.pybot_stats.ripcount.find_one({'url' : url})
    if(result is None):
        return 0
    
    return result.get('count', 0)

async def get_post_info(db,msg_id,team,channel):
    return await db.posts.find_one({'id': msg_id,'channel': channel,'team': team})



    


async def get_most_recent_postdate_user(db,username):
    most_recent_post_date = await db.posts.find_one({ "sender":username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'}] },   sort=[('sent_at',pymongo.DESCENDING)], projection={'sent_at':True,'_id': False})
    if(most_recent_post_date is None):
        return datetime.utcfromtimestamp(-1)

    return most_recent_post_date.get('sent_at',datetime.utcfromtimestamp(-1))

async def get_oldest_postdate_user(db,username):
    oldest_post_date = await db.posts.find_one({ "sender":username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'}] },   sort=[('sent_at',pymongo.ASCENDING)], projection={'sent_at':True,'_id': False})
    if(oldest_post_date is None):
        return datetime.utcfromtimestamp(-1)
    
    return oldest_post_date.get('sent_at',datetime.utcfromtimestamp(-1))

async def get_most_recent_post_user(db,username):
    most_recent_post = await db.posts.find_one({ "sender":username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'}] },  sort=[('sent_at',pymongo.DESCENDING)], projection={'content':True,'_id': False,'channel' : True,'team': True})
    print(f"{username} : {most_recent_post}")
    return most_recent_post

async def get_oldest_post_user(db,username):
    oldest_post = await db.posts.find_one({ "sender":username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'}] },   sort=[('sent_at',pymongo.ASCENDING)], projection={'content':True,'_id': False})
    return oldest_post


async def get_user_total_postcount(db,username):
    return await db.posts.count_documents({'sender' : username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'}] })


async def get_user_postcount_by_subteam(db,username):
    result = db.posts.find({'sender' : username,"$and" : [{'content.type' : {'$ne' : 'reaction'}},{'content.type' : {'$ne' : 'join'}},{'content.type' : {'$ne' : 'leave'}}]},projection = {'_id' : False,'team': True})
    result = await result.to_list(length = None)
    
    post_list = [f"{post['team']}" for post in result]
    return Counter(post_list)

async def get_user_postcount_by_channel(db,username):
    result = db.posts.find({'sender' : username,'content.type' : {'$ne' : 'reaction'}},projection = {'channel' : True, '_id' : False,'team': True})
    result = await result.to_list(length = None)
    
    post_list = [f"{post['team']}#{post['channel']}" for post in result]
    return Counter(post_list)

async def get_user_postcount_by_interval(db,username,start_date,stop_date):
    return
    
async def get_team_postcount_by_channel(db,team):
    result = db.posts.find({'team' : team,'content.type' : {'$ne' : 'reaction'}},projection = {'channel' : True, '_id' : False})
    result = await result.to_list(length = None)
    
    post_list = [f"{post['channel']}" for post in result]
    return Counter(post_list)

async def get_allteams_postcount_by_channel(db,team):
    subteams = db.teams.find({},projection = {'team' : True, '_id' : False})
    
    post_counters = []
    
    async for subteam in subteams:
        result = db.posts.find({'team' : team,'content.type' : {'$ne' : 'reaction'}},projection = {'channel' : True, '_id' : False,'team': True})
        result = await result.to_list(length = None)
        
        post_list = [f"{post['team']}#{post['channel']}" for post in result]
        post_counters.append(Counter(post_list))

    return post_counters




    
    
        
async def get_attachments_cursor(db,team,get_subteams = True,channel = None):
    nested_dict = lambda: defaultdict(nested_dict)
    filter = nested_dict()
    filter['content.type'] = "attachment"
    if(get_subteams):
        filter["team"]["$regex"] = team
    else:
        filter["team"] = team
        
    if(channel is not None):
        filter["channel"] = channel
        
    
    result = db.posts.find(filter,projection = {'_id':False,'id':True,'team':True,'channel':True,'content.filename':True})
    post_count = await db.posts.count_documents(filter)
    return result,post_count
