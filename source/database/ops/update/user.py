import asyncio
from collections import defaultdict
from datetime import datetime,timedelta
from time import sleep
import motor.motor_asyncio


import json
import pymongo
import os
from typing import TypeVar,Dict
TeamMember = TypeVar('TeamMember')


try:
    from kb_commands import member,keybase,post
    from database.ops import user as ops_user
    from database import operations
    from utils import date,formatting
except:
    from ....kb_commands import member,keybase,post
    from ....database import operations
    from ....database.ops import user as ops_user
    from ....utils import date,formatting



async def update_team_members(db,team):
    team_members = await member.get_team_membership(team)
    if(team_members is None):
        return 

    for user in team_members.values():
        try:
            await db.users.insert_one({"username": user['username'],"role": user['role'],"devices": user['devices'], "joindate": user['joindate']})
        except pymongo.errors.DuplicateKeyError:
            await db.users.update_one({"username": user['username']}, {"$set": {"role": user['role'],"devices": user['devices'],"joindate": user['joindate']}} )
            
            
    
    return





async def get_member_activity_data(db,username,max_days_of_inactivity, team = None):
    if(team is None):
        team = (await db.teams.find_one({'top_level' : True}))['name']
    
    most_recent_post_date = await operations.get_most_recent_postdate_user(db,username)
    oldest_post_date = await operations.get_oldest_postdate_user(db,username)
    teams = await member.get_user_membership(username)
    post_count = await ops_user.get_total_postcount(db,username)
    
    is_member = (teams is not None) and team in teams
    
    return {
        "username" : username,
        "post_count" : post_count, 
        "most_recent_post_date": most_recent_post_date, 
        "oldest_post_date": oldest_post_date, 
        "is_active" : is_member and (date.get_delta_from_datetime(most_recent_post_date) <= max_days_of_inactivity),
        'is_member' : is_member,
        'is_deleted': (not is_member) and await member.is_user_deleted(username),
        'teams': teams,
        "team_activity" : await ops_user.get_team_activity(db,username,teams)
        }

async def update_activity(db,team = None,skip_deleted = True,max_days_of_inactivity = 28):
    if(team is None):
        team = (await db.teams.find_one({'top_level' : True}))['name']
    
    team_members = await member.get_team_membership_simple(team)
    
    if(team_members is None):
        return 
    
    update_batch = []
    deleted_users = await ops_user.get_deleted_users(db) if skip_deleted else []
    
    async for user,team_activity in ops_user.users_team_activity(db,team):
        teams = await member.get_user_membership(user)
        is_member = user in team_members.keys()
        if( user in deleted_users):
            continue
        
        
        update_batch.append(pymongo.UpdateOne({'username': user}, {"$set": {"team_activity": team_activity,"teams": teams ,'is_member' : is_member,  'is_deleted': (not is_member) and await member.is_user_deleted(user) }}))
    
    
    result = await db.users.bulk_write(update_batch)
    
    return result


async def initialize(db,team = None):
    if(team is None):
        team = (await db.teams.find_one({'top_level' : True}))['name']
    
    team_members = await member.get_team_membership(team)
    
    if(team_members is None):
        return
    
    async for user,team_activity in ops_user.users_team_activity(db,team):
        teams = await member.get_user_membership(user)
        is_member = user in team
        
        if(user in team_members):
            role = team_members[user]['role'] 
            devices = team_members[user]['devices'] 
            joindate = team_members[user]['joindate']
            
        else:
            role = devices = joindate = None
        
        
       
        
        await db.users.insert_one({'username': user,"role":  role,"devices": devices, "joindate": joindate,"team_activity": team_activity,"teams": teams ,'is_member' : is_member,  'is_deleted': (not is_member) and await member.is_user_deleted(user) })
    



