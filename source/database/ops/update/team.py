import asyncio
from collections import defaultdict
from datetime import datetime,timedelta
from time import sleep
import motor.motor_asyncio
import kb_commands.member as member
import kb_commands.keybase as keybase
import kb_commands.post as post
from database.ops import user as ops_user
from database import operations
from utils import date,formatting
import json
import pymongo
import os

try:
    from kb_commands import keybase
    
except:
    from ....kb_commands import keybase



async def update(db):
    results = await (db.teams.find({},projection = {"name": True})).to_list(length = None)
    results = [item['name'] for item in results]

    
    for team in results:
        channels = await keybase.get_team_channels(team)
        await db.teams.update_one({"name": team},{"$set": {'channels': channels}})
        
        for channel in channels:
            await member.join_channel(team,channel)
            
            
async def initialize(db,team):
    result = await keybase.get_allsubteams(team)
    subteams = result[1:]

    channels = await keybase.get_team_channels(team)
    result = await db.teams.insert_one({"_id" : team,"name" : team,"subteams" : subteams,"channels" : channels,"top_level" : True})
    
    for subteam in subteams:
        channels = await keybase.get_team_channels(subteam)
        result = await db.teams.insert_one({"_id" : subteam,"name" : subteam,"subteams" : None,"channels" : channels,"top_level" : False, "main_team": team})
