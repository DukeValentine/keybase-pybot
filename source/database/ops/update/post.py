from collections import defaultdict
from collections import Counter
from datetime import datetime
import motor.motor_asyncio
import pymongo
import asyncio
import re

import logging
from . import user

try:
    from kb_commands import post
    
except:
    from ....kb_commands import post


async def insert_post(db,keybase_post,semaphore):
    async with semaphore:
        try:
            await db.posts.insert_one(keybase_post)
        except pymongo.errors.DuplicateKeyError as error:
            pass
    
    return


async def update_posts(db, max_days_of_inactivity = 28,unread_only = True, page_size = 1000, chat_source = None):
    
    if(chat_source is not None):
        posts = await  post.get_parsed_posts_in_multiple_channels(chat_source, None, db = db, unread_only = unread_only,only_first_page = False,page_size = page_size)
        
        if(posts != None and len(posts) > 0):
            semaphore = asyncio.BoundedSemaphore(100)
            
            tasks = asyncio.gather(*[insert_post(db,keybase_post,semaphore) for keybase_post in posts])
            try:
                await tasks
            except pymongo.errors.DuplicateKeyError as error:
                pass
            
        return
    
    
    
    results = db.teams.find({},projection = {"channels": True})
    async for team in results:
        team_name = team['_id']
        channels = team['channels']
        print(team_name)
        print(channels)
        posts = await  post.get_parsed_posts_in_multiple_channels(team_name, channels, db = db, unread_only = unread_only,only_first_page = False,page_size = page_size)
        if(posts != None and len(posts) > 0):
            semaphore = asyncio.BoundedSemaphore(100)
            
            tasks = asyncio.gather(*[insert_post(db,keybase_post,semaphore) for keybase_post in posts])
            try:
                await tasks
            except pymongo.errors.DuplicateKeyError as error:
                pass
        
    await user.update_activity(db,max_days_of_inactivity = max_days_of_inactivity)
    
    return


