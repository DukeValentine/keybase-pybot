from collections import defaultdict
from collections import Counter
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
import motor.motor_asyncio
import pymongo
import asyncio
import re
import logging
from enum import Enum
import pymongo
from utils import date,formatting
from kb_commands import member


async def is_user_authorized(db, command_name, user_role):
    if(user_role is None):
        return
    
    command = await db.botauth.command.find_one({"name": command_name.upper()})
    
    if(command is None):
        return
    
    return (user_role >=  command.get("role", member.ROLES['owner'].value))
