from collections import defaultdict
from collections import Counter
from datetime import datetime,timedelta
import motor.motor_asyncio
import pymongo
import asyncio
import re
from database.ops import log as db_log
import logging
from enum import Enum
import pymongo
from utils import date,formatting

async def get_credentials(db):
    return await db.botauth.sites.find_one({"name": "pixiv"})


async def set_tokens(db,access_token,refresh_token):
    await db.botauth.sites.update_one({"name": "pixiv"},{"$set": {"access_token": access_token, "refresh_token": refresh_token} })
