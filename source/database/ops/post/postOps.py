from collections import defaultdict
from collections import Counter
from datetime import datetime
import motor.motor_asyncio
import pymongo
import asyncio
import re

import logging

try:
    from kb_commands import post,keybase
except:
    from ....kb_commands import post,keybase

def test():
    print("posts database ops")
    
    
async def post_exists(db, post):
    result = db.posts.find_one({'id': post['id'],'conversation_id': post['conversation_id']})
    return (result is not None)


async def get_post_info(db,msg_id,team,channel):
    return await db.posts.find_one({'id': msg_id,'channel': channel,'team': team})


async def initialize_posts_team(db,team):
    channels = await keybase.get_team_channels(team)
    posts = await  post.get_parsed_posts_in_multiple_channels(team, channels, db = db)
    if(posts != None or len(posts) > 0):
        await db.posts.insert_many(posts)
