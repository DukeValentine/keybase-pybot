import asyncio
from datetime import datetime,timedelta
from time import sleep
import motor.motor_asyncio
import json
import pymongo
import os
import subprocess




async def get_main_team(db):
    team = (await db.teams.find_one({'top_level' : True})).get('name',None)
    return team


async def is_team_open(db,main_team,team):
    
    result = await db.teams.find_one({"name": team,'top_level' :  False})
    if(result is None):
        return
    
    return result.get("open",False)
    



    
