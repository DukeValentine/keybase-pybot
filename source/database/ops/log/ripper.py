from collections import defaultdict
from collections import Counter
from datetime import datetime
import motor.motor_asyncio
import pymongo
import asyncio
import re

from . import events

try:
    from utils.customTypes import nested_dict,NoNoneDict
except:
    from ....utils.customTypes import nested_dict,NoNoneDict

def test():
    print("log.ripper imported")


async def insert_pixiv_post(db,post_info,downloaded,context = "ripper"): #downloaded flag indicates wheter the post was actually downloaded. If not, the program will know which ones to attempt again
    if(post_info['id'] == 0):
        return
    
    post_info['downloaded'] = downloaded
    try:
        result = await db.log.ripper.pixiv.insert_one(post_info)
    except pymongo.errors.DuplicateKeyError:
        if(context == "ripper"):
            await db.log.ripper.pixiv.update_one({"id": post_info['id'], "tag": "preview"},{"$set": {"tag": post_info['tag']}})
        
    result = await events.download(db,post_info['id'],post_info['filenames'],post_info['path'],datetime.utcnow(),source = "pixiv",success=downloaded)
    return result

async def mark_pixiv_post_as_uploaded(db,post_id,team,channel):
    result =  await db.log.ripper.pixiv.update_one({"id":post_id},{"$set": {f"uploaded_to_chat.{team}.{channel}": True}})
    return result


async def check_if_pixiv_post_downloaded(db,post_id):
    post = await db.log.ripper.pixiv.find_one({"id":post_id})
    return (post is not None and post['downloaded'] and post['tag'] != "preview")

async def get_pixiv_post(db,post_id):
    return db.log.ripper.pixiv.find_one({"id": post_id})


async def get_downloaded_pixiv_posts(db,tag,team,channel,uploaded_to_chat = False):
    return db.log.ripper.pixiv.find({'tag':tag, f'uploaded_to_chat.{team}.{channel}': {"$exists": uploaded_to_chat}})


async def insert_nhentai_doujin(db,doujin):
    doujin['id'] = str(doujin['id'])
    doujin["_id"] = doujin['id']
    doujin['duplicates'] = []
    
    
    try:
        await db.log.ripper.nhentai.insert_one(doujin)
    except pymongo.errors.DuplicateKeyError:
        pass

async def get_single_doujin(db, galleryid):
    return await db.log.ripper.nhentai.find_one({"_id": galleryid})

async def get_doujins_not_in_db(db,galleryids):
    galleryids = [str(galleryid) for galleryid in galleryids]
    
    result = db.log.ripper.nhentai.find({"_id": {"$in": galleryids}}, projection = {"_id": True})
    result = [item["_id"] async for item in result]
    
    not_in_db = [galleryid for galleryid in galleryids if galleryid not in result]
    return not_in_db

async def get_many_doujins(db, tags, exclude_tags = None, previously_fetched = False, limit =10):
    if(type(tags) == str):
        tags = [tags]
    
    find_query = NoNoneDict()
    find_query['most_recent'] = True
    find_query['fetched'] =  previously_fetched
    find_query['tags.name'] = NoNoneDict()
    find_query['tags.name']["$in"] = tags
    find_query['tags.name']["$nin"] = exclude_tags
    print(find_query)
    
    result = db.log.ripper.nhentai.find(find_query,projection = {"_id": True})
    result = await result.to_list(length = limit)
    galleryids = [item["_id"] for item in  result]
    
    await db.log.ripper.nhentai.update_many({"_id": {"$in": galleryids}}, {"$set": {"fetched": True}})
    
    
    return galleryids





async def insert_danbooru_post(db,post_info,downloaded,target = "danbooru",context = "ripper"): #downloaded flag indicates wheter the post was actually downloaded. If not, the program will know which ones to attempt again
    post_info['downloaded'] = downloaded
    
    try:
        result = await db.log.ripper[target].insert_one(post_info)
    except pymongo.errors.DuplicateKeyError:
        if(context != "ripper"):
            await db.log.ripper[target].update_one({"id": post_info['id']},{"$set": post_info})
    
    if 'filename' in post_info:
        filename = post_info['filename']
    else:
        filename = post_info['image']
    
    result = await events.download(db,post_info['id'],filename,post_info['path'],datetime.utcnow(),source = target,success=downloaded, context=context)
    
    return result


async def mark_danbooru_post_as_uploaded(db,post_id,md5,team,channel,target = "danbooru"):
    result =  await db.log.ripper[target].update_one({"id":post_id,"md5":md5},{"$set": {f"uploaded_to_chat.{team}.{channel}": True}})
    return result


async def append_favgroup_to_post(db,post_id,favgroup,target = "danbooru"):
    await db.log.ripper[target].update_one({"id":post_id}, {"$addToSet": {"favgroups": favgroup}})
    
    return 


async def check_if_danbooru_post_downloaded(db,post_id,md5,target = "danbooru"):
    post = await db.log.ripper[target].find_one({"id":post_id,"md5":md5})
    return (post is not None and post['downloaded'] and post['tag'] != "preview")

async def get_downloaded_danbooru_posts(db,favgroup,team,channel,uploaded_to_chat = False,target = "danbooru"):
    return db.log.ripper[target].find({"tag": favgroup,f'uploaded_to_chat.{team}.{channel}': {"$exists": uploaded_to_chat}})


