from collections import defaultdict
from collections import Counter
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
import motor.motor_asyncio
import pymongo
import asyncio
import re
import logging
from enum import Enum
import pymongo
from utils import date,formatting
from . import events




async def append_to_periodical(db,name,item,date = datetime.utcnow()):
    
    
    periodical = await db.log.periodical.find_one({"name": name })
    
    if(periodical is not None):
        await db.log.periodical[name].create_index("item", unique = True)
            
        
        await db.log.periodical.update_one({"name": name}, {"$addToSet": {"items": item }})
        
        try:
            await db.log.periodical[name].insert_one({"item": item, "last_date": date,"history": [{"date": date, "change": "added"}], "valid": True})
            
        except pymongo.errors.DuplicateKeyError:
            if(periodical.get("allow_duplicates",False) is True):
                reference_date = date - relativedelta(days = periodical['duplicate_tolerance'].get('days',0), months = periodical['duplicate_tolerance'].get('months',0))
                await db.log.periodical[name].update_one({"item": item, "date": {"$lte": reference_date}}, { "$set": {"valid": True, "last_date": date}, "$push": {"history": {"date": date, "change": "added"}}}) 
                return f"Appended duplicate {item} to {name}"
            
            else:
                result = await db.log.periodical[name].update_one({"item": item,"valid": False}, { "$set": {"valid": True, "last_date": date}, "$push": {"history": {"date": date, "change": "added"}}}) 
                if(result.matched_count == 0):
                    return f"nochange for {item} in {name}"
                else:
                    return f"Added previously  removed {item} to {name}"
        else:
            return f"Appended {item} to {name}"


async def remove_from_periodical(db,name,item,date = datetime.utcnow()):
    date = datetime.utcnow()

    
    result = await db.log.periodical.find_one({"name": name })
    
    if(result is not None):
        result = await db.log.periodical.update_one({"name": name}, {"$pull": {"items": item}})
        return await db.log.periodical[name].update_one({"item": item,"valid": True},{"$set": {"valid": False,"last_date": date, "$push": {"history": {"date": date, "change": "removed"}}}}) 
    
    else:
        print(f"No target found with {name}")
        return f"No target found with {name}"


async def undo_last_change(db,name):
    result = await db.log.periodical.find_one({"name": name })
    
    if(result is not None):
        last_item = db.log.periodical.find({"items": {"$exist": True}}, {"items": {"$slice": pymongo.DESCENDING}})
        if(last_item is None):
            return
        
        return await remove_from_periodical(db,name, last_item["items"][0])





async def get_periodical_items(db,name):
    periodical = await db.log.periodical.find_one({"name": name})
    
    if(periodical is not None):
        items = db.log.periodical[name].find({"valid": True},sort = [("last_date", pymongo.ASCENDING)])
        if(items is None):
            return None,None
        else:
            date = datetime.utcnow()
            reference_date = date - relativedelta(days = periodical['duplicate_tolerance'].get('days',0), months = periodical['duplicate_tolerance'].get('months',0))
            
            if periodical.get("allow_duplicates",False) is True:
                allitems = []
                duplicate_candidates = []
                
                async for item in items:
                    allitems.append(item)
                    if(item['last_date'] <= reference_date):
                        duplicate_candidates.append(item)
                        
                return allitems,duplicate_candidates
                
            
            else:
                return items.to_list(length=None),None



        
        
async def check_reminders(db):
    result = db.log.periodical.reminders.find({})    
    async for reminder in result:
        last_check = reminder.get("last_check", datetime.utcfromtimestamp(0))
        current_date = datetime.utcnow()
        if( (current_date - last_check).days >= reminder.get("interval_days", 7) ):
            await db.log.periodical.reminders.update_one({"_id": reminder['_id']},{"$set" : {"last_check": current_date},"$push": {"checks": current_date}})
            return {"message": reminder.get("message",None), "team": reminder.get("team",None), "channel": reminder.get("channel",None)}
        
    
    



periodicals = {
    "ADD": append_to_periodical,
    "REMOVE": remove_from_periodical
    }

async def change_periodical(db,name,values,command_name,date,users = None):
    if(command_name in periodicals):
        results = [await periodicals[command_name](db,name,value,date) for value in values]
        return results
    
        


