from collections import defaultdict
from collections import Counter
from datetime import datetime,timedelta
import motor.motor_asyncio
import pymongo
import asyncio
import re
from database.ops import log as db_log
import logging
from enum import Enum
import pymongo
import itertools
from utils import date,formatting

try:
    from utils import fingerprint as fingerprint_utils
except:
    from ....utils import fingerprint as fingerprint_utils


async def fingerprint_already_inserted(db,fingerprint):
    result = await db.log.lookup.find_one({"_id": fingerprint})
    return result is not None

async def find_similar_fingerprint(db,fingerprint,threshold = 90):
    if(fingerprint is None):
        return
    
    cursor = db.log.lookup.find({})
    async for item in cursor:
        if(fingerprint_utils.similarity_fingerprints(item['fingerprint'],fingerprint) >= threshold):
            return item
    
    return
    


async def insert_lookup_source(db,fingerprint,source):
    if isinstance(source,str): source =  [source]
    
    if(source is None):return

    try:
        await db.log.lookup.insert_one({"_id": fingerprint, "fingerprint": fingerprint, "source": source})
        return True
        
    except pymongo.errors.DuplicateKeyError:
        return False
    
async def update_doujin_fingerprints(db, doujin_fingerprints):
    for fingerprint,galleryids in doujin_fingerprints.items():
        print(galleryids)
        duplicates = [] if (len(galleryids) == 1) else galleryids[1:]
        
        await db.log.ripper.nhentai.update_one({"_id": galleryids[0]}, {"$set": {"fingerprint": fingerprint, "most_recent": True, "duplicates": duplicates}})
        try:
            await db.log.ripper.nhentai.update_many({"_id": {"$in": galleryids[1:]}}, {"$set": {"fingerprint": fingerprint, "most_recent": False, "duplicates": [], "duplicateof": galleryids[0]}})
        except IndexError:
            pass
        

async def update_similar_doujins(db,threshold = 80):
    cursor = db.log.ripper.nhentai.find({"most_recent": True},projection = {'fingerprint': True}, sort = [("_id",pymongo.DESCENDING)])
    doujins = await cursor.to_list(length = None)
    duplicates = defaultdict(list)
    
    for doujin_A, doujin_B in itertools.combinations(doujins,2):
        if(fingerprint_utils.similarity_fingerprints(doujin_A['fingerprint'],doujin_B['fingerprint']) >= threshold):
            if(doujin_A['_id'] in duplicates):
                duplicates[doujin_A['_id']].append(doujin_B['_id'])
            elif(doujin_B['_id'] in duplicates):
                duplicates[doujin_B['_id']].append(doujin_A['_id'])
            else:
                duplicates[doujin_A['_id']] = [doujin_A['_id'],doujin_B['_id']]
        
    for key,values in duplicates.items():
        duplicates[key] = sorted(values,reverse=True)
        
    for galleryids in duplicates.values():
        print(await db.log.ripper.nhentai.update_one({"_id": galleryids[0]},{"$addToSet": {"duplicates": galleryids[1:]}}))
        print(await db.log.ripper.nhentai.update_many({"_id": {"$in": galleryids[1:]}}, {"$set": {"most_recent": False, "duplicateof": galleryids[0]}}))
        
        
    
    
    
    
    
