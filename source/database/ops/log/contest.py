from collections import defaultdict
from collections import Counter
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
import motor.motor_asyncio
import pymongo
import asyncio
import re
import logging
from enum import Enum
import pymongo
from utils import date,formatting
from . import events
from database.ops.constants import *


async def add_contest(db,contest_name,date = datetime.utcnow(),users = None):
    result = await db.log.periodical.update_one({"name": "contest"}, {"$addToSet": {"items": contest_name }})
    
    print(result.modified_count)
    print()
    
   
    if(await db.log.periodical.contest.count_documents({"active": True}) == 0 ):
        await db.log.periodical.contest.insert_one({"contest_name": contest_name, "start_date": date, "active": True})
        return f"Added new contest {contest_name}"
   
   
    return "There's already an active contest, close it before adding a new one"


async def remove_winner(db,contest_name,date = datetime.utcnow(),users = None):
    return

async def add_winner(db,message,date = datetime.utcnow(),users = None):
    result = await db.log.periodical.contest.update_one({"active": True}, {"$set": {"active": False, "end_date": date, "winners": users, "notes": message} })
    return f"Added {users} as the winner(s) of the contest" if result.modified_count > 0 else f"No winners were added because there's no active contest"
    
    
async def append_winner(db,message,date = datetime.utcnow(),users = None):
    await db.log.periodical.contest.find_one_and_update({}, {"$push": {"winners": {"$each": users} }}, sort = {"_id": pymongo.DESCENDING})
    return f"Added {users} as the winner(s) of the contest"



async def reopen_contest(db,contest_name,date = datetime.utcnow(),users = None):
    await db.log.periodical.contest.find_one_and_update({}, {"set": {"active": True }}, sort = {"_id": pymongo.DESCENDING})
    return
    

async def remove_contest(db,contest_name,date = datetime.utcnow(),users = None):
    result = await db.log.periodical.update_one({"name": contest_name}, {"$pull": {"items": contest_name }})
    
    if(result.modified_count > 0 and db.log.periodical.contest.countDocuments({"active": True}) == 0 ):
        await db.log.periodical.contest.delete_one({"contest_name": contest_name})
        return f"Removed {contest_name}"
    
    
    return f"Failed to remove {contest_name}"


async def undo_latest_contest(db,contest_name,date = datetime.utcnow(),users = None):
    result = db.log.periodical.update({"name": "contest"},{"$pop": {"items": 1} })
    
    if(result.modified_count > 0):
        await db.log.periodical.contest.find_one_and_delete({}, sort = {"_id": pymongo.DESCENDING})
        return "Removed latest contest"
        
    return "No contests were removed"

async def get_history(db):
    result = db.log.periodical.contest.find({})
    
    if(result is not None):
        return await result.to_list(length=None)
    
    else:
        return []


change_contest = {
    "ADD": {
        "": add_contest,
        "winner": add_winner,
        "winner.append": append_winner,
        "again": reopen_contest
        },
    
    "REMOVE":{
        "": remove_contest,
        "winner": remove_winner,
        "undo": undo_latest_contest
    }
    
    
    }





async def change(db,change_type,contest_name,date,users,subtarget):
    
    try:
        change_function = change_contest[change_type][subtarget]
        result = await change_function(db,contest_name,date,users)
    except IndexError:
        pass
    
    
    
    return result



