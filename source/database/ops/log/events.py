from collections import defaultdict
from collections import Counter
from datetime import datetime
import logging
import motor.motor_asyncio
import pymongo
import asyncio
import re

try:
    from utils import logger
except:
    from .... import logger





async def general(db, type, name,date,level,event_message,context = None):
    event_type = type.upper()
    event_name = name.upper()
    await db.log.events.insert_one({"type": event_type,"level":level,"name":event_name,"date":date, "message": event_message,"context": context})
    return

async def upload(db,post_id,filename, team, channel,date,source = "unknown", success = True, context = None):
    await db.log.events.insert_one({"type": "UPLOAD","level": logging.INFO,"filename": filename,"post_id": post_id , "team":team, "channel": channel,"date": date, "source": source, "success": success, "context": context})
    return 

async def download(db,post_id,filename,directory,date,source = "unknown", success = True, context = None):
    await db.log.events.insert_one({"type": "DOWNLOAD","level": logging.INFO,"post_id": post_id ,"filename": filename, "directory":directory,"date": date, "source": source, "success": success, "context": context})
    return




async def info(db, type, name,date,event_message,context = None):
    await db.log.events.insert_one({"type": type,"level":logging.INFO,"name":name,"date":date, "message": event_message,"context": context})
    return 

async def error(db, type, name,date,event_message,context = None):
    return 

async def critical(db, type, name,date,event_message,context = None):
    return 

async def debug(db, type, name,date,event_message,context = None):
    return 

async def verbose(db, type, name,date,event_message,context = None):
    return
