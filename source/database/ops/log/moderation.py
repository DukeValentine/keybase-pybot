from collections import defaultdict
from collections import Counter
from datetime import datetime,timedelta
import motor.motor_asyncio
import pymongo
import asyncio
import re
import logging
from enum import Enum
import pymongo
from utils import date,formatting
from . import events

try:
    from bot import moderation
except:
    from ....bot import moderation







async def log_applicant_status(db,username,team, status, change,date):
    await insert_application_log(db,username,team, status,change,date)
    return


async def insert_application_log(db,username,team, status,change,date):
    await events.general(db,"MODERATION","APPLICATION",date,logging.info, event_message = f"User {username} application to {team} is {status} | Change {change}",context= {"username": username, "team": team, "status": status, "change": change})
    await db.log.team.application.insert_one({"username": username, "team":team, "status": status, "change": change, "date":date})
    return


async def insert_moderation_log(db,username,team,severity,context,date):
    if(severity not in moderation.severity_to_name):
        print("severity level not found")
        return
    
    await db.log.moderation.user.insert_one({"username": username,"team": team,"severity": severity,"context" : context,"date":date})
    return
    






