from collections import defaultdict
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
import motor.motor_asyncio
import pymongo
import asyncio
import re
import logging
from enum import Enum
import pymongo 
from utils import date,formatting

from .common import *

try:
    from database.ops import user
except:
    from ....database.ops import user



async def sum_results(db,team,results):
    count = 0
    
    async for result in results:
        count += result['count']
    
    return {'team': team, 'count': count}



    

    

async def item_aggregation(db,team,item = "postcount",start_date = None,end_date = datetime.utcnow(), by_subteam = False,by_user = False,only_attachments = False,content_types = []):
    team_grouping = subteam_grouping.get(by_subteam,"$main_team")
    
    results =  db.posts.aggregate([team_match(team),date_range_match(start_date,end_date), content_type_match(only_attachments,item), grouping(by_subteam,by_user,item),sorting(item)])
    
    return results



async def all_items_aggregation(db,team,start_date = None,end_date = datetime.utcnow(), by_subteam = False,by_user = False) -> nested_dict :
    results_filtered = nested_dict()
    
    for item in items:
        results = await item_aggregation(db,team,item,start_date,end_date,by_subteam,by_user)
        async for entry in results:
            result = NoNoneDict()
            result[item] = entry['_id'].get(item,None)
            result['username'] =  entry['_id']["username"] if by_user else None
            result['role'] =  await user.get_user_role(db,entry['_id']["username"]) if by_user else None
            result['total'] = entry[item]
            
            try:
                results_filtered[entry['_id']['team']][item].append(result)
            except (KeyError,AttributeError):
                results_filtered[entry['_id']['team']][item] = [result]
        
    return results_filtered



    






if __name__ == "__main__":
    team = "team_name"
    start_date = datetime.utcfromtimestamp(1500000000)
    end_date = datetime.utcnow()
    by_subteam = False
    print([team_match(team),date_range_match(start_date,end_date), content_type_match(attachment_only=True), grouping(by_subteam,"postcount"),sorting("count")])





    
    
    
        
    
