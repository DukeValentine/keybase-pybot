from collections import defaultdict
from collections import Counter
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
import motor.motor_asyncio
import pymongo
import asyncio
import re
import logging
from enum import Enum
import pymongo
from utils import date,formatting
from database.ops import user
from .common import *

nested_dict = lambda: defaultdict(nested_dict)

async def sum_results(db,team,results):
    count = 0
    
    async for result in results:
        count += result['count']
    
    return {'team': team, 'count': count}



    

    

async def item_aggregation(db,username,item = "postcount",start_date = None,end_date = datetime.utcnow(), by_subteam = False,only_attachments = False,content_types = []):
    
    results =  db.posts.aggregate([user_match(username),date_range_match(start_date,end_date), content_type_match(attachment_only = only_attachments), grouping(by_subteam,item),sorting(item)])
    
    return results



async def all_items_aggregation(db,username,start_date = None,end_date = datetime.utcnow(), by_subteam = False):
    results_filtered = nested_dict()
    
    for item in items:
        results = await item_aggregation(db,username,item,start_date,end_date,by_subteam)
        async for entry in results:
            try:
                 results_filtered[item][entry['_id']['team']].append({item: entry['_id'].get(item,None), "count":entry[item]})
            except (KeyError,AttributeError):
                results_filtered[item][entry['_id']['team']] = [{item: entry['_id'].get(item,None), "count":entry[item]}]
        
    return results_filtered
