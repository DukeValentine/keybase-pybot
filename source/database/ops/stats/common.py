import pymongo
try:
    from utils.customTypes import nested_dict,NoNoneDict
except:
    from ....utils.customTypes import nested_dict,NoNoneDict
    
subteam_grouping = {
    False : "$main_team",
    True : "$team"
    }


field_grouping = {
    "size": None,
    "type": "$content.type",
    "mimeType": "$content.mimeType"   ,
    "postcount": None
    
    }

by_user = {
    False: None,
    True: "$sender"
}

only_attachments = {
    "mimeType":True,
    "size": True,
    "type": False,
    "postcount": False
    
    }



counting = {
    "size": {"$sum" : "$content.size"},
    "type": {"$sum" : 1},
    "mimeType": {"$sum" : 1}   ,
    "postcount": {"$sum" : 1}
    
    }

items = ["postcount","size", "type","mimeType"]


def team_match(team):
    return {"$match": {"team": {"$regex": team}}}

def user_match(username):
    return {"$match": {"sender": username}}

def date_range_match(start_date,end_date):
    if(start_date is None):
        return {"$match": {}}
    
    return {"$match" : {"sent_at": {"$gte": start_date,"$lte": end_date} }}

def content_type_match(attachment_only,item,*types):
    if(attachment_only or only_attachments.get(item,False)):
        return {"$match": {'content.type' : 'attachment'}}
    elif len(types) == 0:
        return {"$match": {"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text' },{'content.type': 'unfurl'}]}}
    else:
        content_type = []
        for type in types:
            content_type.append({'content.type': type})
            
        return {"$match": {"$or" : content_type}}
        
        
    
def grouping(by_subteam, by_user,field = "postcount"):
    group = nested_dict()
    group["$group"]["_id"]["team"] = subteam_grouping[by_subteam]
    
    field_value = field_grouping.get(field,None)
    if(field_value is not None):
        group["$group"]["_id"][field] = field_value
        
    group["$group"][field] = counting.get(field, {"$sum" : 1})
    
    if by_user:
        group["$group"]["_id"]['username'] = "$sender"
    
    return group


def sorting(field = "postcount",order = pymongo.DESCENDING):
    return {"$sort" : { field: pymongo.DESCENDING}}
