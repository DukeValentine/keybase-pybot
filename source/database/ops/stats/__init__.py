from . import team,user,common
from datetime import datetime



_stats_type = {
    "team": team,
    "user": user
    }


async def no_type_found(type):
    print(f"No stats type found for {type}")
    



async def aggregate(stat_instance : 'StatsCommand', db: 'PymongoDB'):
    stats_target = _stats_type.get(stat_instance.stat_type,None)
    
    if(stats_target is not None):
        if(stat_instance.subtarget is None):
            return await stats_target.all_items_aggregation(db,stat_instance.target,stat_instance.start_date,stat_instance.end_date,stat_instance.by_subteam,stat_instance.by_user)
        else:
            return await stats_target.item_aggregation(db,stat_instance.target,stat_instance.subtarget,stat_instance.start_date,stat_instance.end_date,stat_instance.by_subteam,stat_instance.by_user)
        
    print(f"No stats type found for {stat_type}")
    return {"error": f"No stats type found for {stat_type}"}
