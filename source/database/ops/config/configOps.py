from collections import defaultdict
from collections import Counter
from datetime import datetime,timedelta
import motor.motor_asyncio
import pymongo
import json
import asyncio
import re
from database.ops import log as db_log
import logging
from enum import Enum
import pymongo
from utils import date,formatting

try:
    from kb_commands import keybase
except:
    from ....kb_commands import keybase


async def reset_database(db):
    await db.botauth.delete_many({})
    await db.botauth.command.delete_many({})
    await db.botauth.sites.delete_many({})
    await db.config.delete_many({})
    await db.log.events.delete_many({})
    await db.log.periodical.delete_many({})
    await db.log.periodical.contest.delete_many({})
    await db.log.periodical.monthly_ip.delete_many({})
    await db.log.periodical.weekly_loli.delete_many({})
    await db.log.ripper.atf.delete_many({})
    await db.log.ripper.danbooru.delete_many({})
    await db.log.ripper.gelbooru.delete_many({})
    await db.log.ripper.pixiv.delete_many({})
    await db.users.delete_many({})
    await db.teams.delete_many({})
    await db.posts.delete_many({})
    await db.config.ripping.delete_many({})
    await db.config.db.delete_many({})
    await db.config.delete_many({})
    await db.pybot_stats.delete_many({})
    await db.pybot_stats.runtime.delete_many({})
    
    await db.users.drop_indexes()
    await db.posts.drop_indexes()
    await db.teams.drop_indexes()
    await db.log.periodical.drop_indexes()
    await db.log.botauth.sites.drop_indexes()
    await db.log.botauth.command.drop_indexes()
    
    

async def create_indexes(db):
    await db.teams.create_index("name", unique = True)
    await db.users.create_index("username", unique = True)
    await db.users.applications.create_index([("username", pymongo.ASCENDING),
                                  ("team", pymongo.ASCENDING)], unique = True)
    await db.config.create_index("type", unique = True)
    await db.posts.create_index([("id", pymongo.ASCENDING),
                                  ("conversation_id", pymongo.ASCENDING)],unique = True)
    
    await db.posts.create_index("sent_at")
    await db.posts.create_index("sender")
    await db.posts.create_index("team")
    await db.posts.create_index("channel")
    await db.posts.create_index("content.type")
    
    await db.log.periodical.create_index("name", unique = True)
    await db.log.ripper.danbooru.create_index("id", unique = True)
    await db.log.ripper.danbooru.create_index("md5", unique = True)
    await db.log.ripper.atf.create_index("id", unique = True)
    await db.log.ripper.atf.create_index("md5", unique = True)
    await db.log.ripper.pixiv.create_index("id", unique = True)
    
    await db.botauth.sites.create_index("name", unique = True)
    await db.botauth.command.create_index("name", unique = True)
    
    
async def load_config_file(db,config_path):
    with open(config_path) as config_file:
        try:
            config_content = config_file.read()
            json_config = json.loads(config_content)
            for key, values in json_config.items():
                for value in values:
                    insert_value = value.copy()
                    try:
                        await db[key].insert_one(value)
                    except pymongo.errors.DuplicateKeyError as error:
                        print(error.details['keyValue'])
                        print(key)
                        print(insert_value)
                        result = await db[key].replace_one(error.details['keyValue'],  insert_value)
                        print(result.modified_count)
        except json.JSONDecodeError as e:
            print("error")
            print(e)
            exit(1)
    

    
    
async def get_database_config(db):
    config = await db.config.find_one({})
    return config

async def get_ripping_config(db):
    config = await db.ripper.config.find_one({})
    return config

async def get_ripping_destination(db):
    return db.ripper.dest.find({})

