from collections import defaultdict
from collections import Counter
from datetime import datetime,timedelta
import motor.motor_asyncio
import pymongo
import asyncio
import re
from database.ops import log as db_log
import logging
from enum import Enum
import pymongo
from utils import date,formatting


#APPLICANTS
async def insert_applicant(db,username,team,date):
    try:
        await db.users.applications.insert_one({"username":username, "team": team, "status": "PENDING", "contacted": False, "replied": False, "open": True, "application_start": date, "application_end": None})
    except pymongo.errors.DuplicateKeyError:
        return "duplicate"
    else:
        return "okay"
        
    
async def update_applicant_status(db,username,new_status):
    await db.users.applications.update_one({"username":username, "open": True},{"$set": {"status": new_status.upper()} })
    return 

async def mark_applicant_as_contacted(db,username,team):
    await db.users.applications.update_one({"username":username, "open": True},{"$set": {"contacted": True}})
    return 

async def mark_applicant_as_replied(db,username,team):
    await db.users.applications.update_one({"username":username, "open": True},{"$set": {"replied": True}})
    return

async def close_application(db,username,team, date):
    await db.users.applications.update_one({"username":username, "open": True},{"$set": {"open": False,"application_end": date}})
    return

async def reopen_application(db,username,team):
    await db.users.applications.update_one({"username":username, "open": False},{"$set": {"open": True, "status": "PENDING"}})
    return    
    
    
    
    
    


#POST STATS
async def get_total_postcount(db,username):
    return await db.posts.count_documents({'sender' : username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'},{'content.type': 'unfurl'}] })

async def get_team_postcount(db,username,team):
    return await db.posts.count_documents({'sender' : username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'},{'content.type': 'unfurl'}] ,"team": team})

async def get_latest_postdate(db,username,team):
    most_recent_post_date = await db.posts.find_one({ "sender":username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'},{'content.type': 'unfurl'}] , "team": team},   sort=[('sent_at',pymongo.DESCENDING)], projection={'sent_at':True,'_id': False})
    if(most_recent_post_date is None):
        return datetime.utcfromtimestamp(-1)

    return most_recent_post_date.get('sent_at',datetime.utcfromtimestamp(-1))

async def get_oldest_postdate(db,username,team):
    oldest_post_date = await db.posts.find_one({ "sender":username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'},{'content.type': 'unfurl'}] ,"team": team},   sort=[('sent_at',pymongo.ASCENDING)], projection={'sent_at':True,'_id': False})
    if(oldest_post_date is None):
        return datetime.utcfromtimestamp(-1)
    
    return oldest_post_date.get('sent_at',datetime.utcfromtimestamp(-1))


async def get_latest_post(db,username,team):
    most_recent_post_date = await db.posts.find_one({ "sender":username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'},{'content.type': 'unfurl'}] , "team": team},   sort=[('sent_at',pymongo.DESCENDING)], projection={'sent_at':True,'_id': False})
    if(most_recent_post_date is None):
        return datetime.utcfromtimestamp(-1)

    return most_recent_post_date

async def get_oldest_post(db,username,team):
    oldest_post_date = await db.posts.find_one({ "sender":username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text'},{'content.type': 'unfurl'}] ,"team": team},   sort=[('sent_at',pymongo.ASCENDING)], projection={'sent_at':True,'_id': False})
    if(oldest_post_date is None):
        return datetime.utcfromtimestamp(-1)
    
    return oldest_post_date


async def get_postcount_by_subteam(db,username,team):
    result = db.posts.aggregate([{"$match": {"sender": username}},{"$match": {"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text' }]}}, {"$group" : {"_id": {"sender": "$sender","team" : "$team"}, "count" : {"$sum" : 1}}},{"$sort" : { "count": pymongo.DESCENDING}}])
    
    post_list = []
    
    async for item in result:
        item_info = result["_id"]
        item_info['count'] = result['count']
        post_list.append(item_info)
        
    
    return post_list

async def get_postcount_by_channel(db,username,team):
    result = db.posts.aggregate([{"$match": {"sender": username}},{"$match": {"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text' }]}}, {"$group" : {"_id": {"sender": "$sender","team" : "$team","channel": "$channel"}, "count" : {"$sum" : 1}}},{"$sort" : { "count": pymongo.DESCENDING}}])
    
    post_list = []
    
    async for item in result:
        item_info = result["_id"]
        item_info['count'] = result['count']
        post_list.append(item_info)
        
    
    return post_list

async def get_postcount_by_interval(db,username,start_date,stop_date):
    post_count = await db.posts.countDocuments({"username": username,"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text' }], "sent_at" : {"$gte" : start_date, "$lte": stop_date} })
    return post_count


async def get_postcount_by_type(db,username,team):
    results = await db.posts.aggregate([{"$match": {"sender": username}},{"$match": {"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text' }]}}, {"$group" : {"_id": "$content.type", "count" : {"$sum" : 1}}},{"$sort" : { "count": pymongo.DESCENDING}}])
    
    post_list = []
    
    async for item in results:
        item_info = dict()
        item_info['type'] = item["_id"]
        item_info['count'] = item['count']
        post_list.append(item_info)
        
    
    return post_list




#ROLES,STATUS
async def add_role(db,username,role, date):
    await db.user.update({"username":username},{"$push": { "custom_roles" : {"role_name": role, "date":date}}})
    
    return

async def initialize_roles(db):
    user_roles = {'unknown': 1, 'reader': 2, 'writer': 3, 'admin': 4, 'owner': 5}
    await db.botauth.keybaseroles.insert_one({"roles": user_roles})
    
    
async def initialize_status(db):
    user_status = {'inactive': 1, 'active': 2, 'left_the_team': 3, 'deleted': 4, 'banned': 5}
    await db.botauth.keybaseroles.insert_one({"status": user_status})
    
    
async def get_roles(db):
    roles = await db.users.keybaseroles.find_one({})
    if(roles is None):
        await initialize_roles(db)
        roles = await db.botauth.keybaseroles.find_one({})
    
    
    return roles
    
async def get_role_value(db,role):
    roles = await get_roles(db)
    role_value = roles.get(role,1)
    return role_value

async def get_status_value(db,status):
    status = await db.botauth.keybaseroles.find_one({"status": {"$exists": True}})
    if(status is None):
        await initialize_status(db)
        status = await db.botauth.keybaseroles.find_one({"status": {"$exists": True}})
    
    status = status['status']
    status_value = status.get(status,None)
    return status_value

async def get_user_role(db,username):
    user = await db.users.find_one({"username": username})
    if(user is None):
        return None
    
    else: 
        return user.get('role', await get_role_value(db,"unknown"))


    
#ACTIVITY
async def users_team_activity(db,team):
    results = db.posts.aggregate([{"$match": {"team": {"$regex": team}}},{"$match": {"$or" : [{'content.type' : 'attachment'},{'content.type' :  'text' },{'content.type': 'unfurl'}]}}, {"$group" : {"_id":{"sender":"$sender", "date": "$sent_at","team": "$team"}}},{"$sort" : { "_id.date": -1, "_id.sender": 1}},{"$group": {"_id": {"sender": "$_id.sender"},"latest": {"$first": "$_id.date"}, "oldest": {"$last": "$_id.date"},"post_count": {"$sum":1}  }}])
    
    nested_dict = lambda: defaultdict(nested_dict)
    
    async for result in results:
        team_activity = nested_dict()
        team_activity[team]['latest'] = result['latest']
        team_activity[team]['oldest'] = result['oldest']
        team_activity[team]['post_count'] = result['post_count']
        
        yield result['_id']['sender'], team_activity
        
        
async def mark_user_removed(db,username):
    await db.users.update_one({"username": username},{"$set": {"is_member": False}})

    


async def get_team_activity(db,username,teams):
    if(teams is None):
        return None
    
    nested_dict = lambda: defaultdict(nested_dict)
    team_activity = nested_dict()
    
    for team in teams:
        main_team = team.split(".")[0]
        latest = await get_latest_postdate(db,username,team)
        oldest = await get_oldest_postdate(db,username,team)
        post_count = await get_team_postcount(db,username,team)
        
        try:
            if(team_activity[main_team].get("latest") < latest):
                team_activity[main_team]['latest'] = latest
            if((team_activity[main_team].get("oldest") > oldest and post_count > 0) or  (team_activity[main_team]['post_count'] == 0 and post_count > 0 )):
                team_activity[main_team]['oldest'] = oldest
                
            team_activity[main_team]['post_count'] =  team_activity[main_team].get("post_count",0) + post_count
            
            
            
        except (KeyError,AttributeError,TypeError):
            team_activity[main_team]['latest'] = latest
            team_activity[main_team]['oldest'] = oldest
            team_activity[main_team]['post_count'] = post_count
            pass
        
    return team_activity



async def register_inactivity(db,username,team,inactivity_period,date, severity = "warning"):
    inactivity_interval = dict()
    inactivity_interval['days'] = inactivity_period.days
    inactivity_interval['hours'] = inactivity_period.seconds/3600
    
    context = dict()
    context['reason'] = "inactivity"
    context['inactivity'] = inactivity_interval
    
    await db_log.events.general(db, "MODERATION", f"USER{severity.upper()}",date,logging.INFO,f"User {username} received {severity} due to Inactivity at team {team}",context = context)
    
    await db.user.update({"username":username}, { "$push": { severity :  { "context" : context, "date": date}  }})
    
    return


async def get_deleted_users(db):
    result = db.users.find({"is_deleted": True},projection = {'username': True})
    users = dict()
    async for user in result:
        users[user['username']] = True
    return users

async def get_inactive_users(db,max_role,team,max_days_of_inactivity):
    users = []
    reference_date = datetime.utcnow()
    limit_date = reference_date - timedelta(days =max_days_of_inactivity)
    max_role_value = await get_role_value(db,max_role)
    print(limit_date)
    print(max_role_value)
    
    
    results = db.users.find({f'team_activity.{team}.latest' : {'$lte' : limit_date}, 
                             'joindate': {'$lte' : limit_date}, 
                             'is_member': True ,
                             'role' : {'$lte': max_role_value}}, 
                            projection = {'username' : True,'_id' : False,'most_recent_post_date' : True,"team_activity": True}, sort = [('username',pymongo.ASCENDING)])
    
    
    
        
    
    
    
    return await results.to_list(length=None),reference_date
