'''
        
MEMBER
        
username STRING
post_count int
role int
status int
most_recent_post_date    Date 
oldest_post_date   Date 
join_date   Date 
warning_count int
rejoin_count int
special_roles array
devicesID array
senderUID array

        
        
'''


import asyncio
import shlex
from datetime import datetime,timedelta
from time import sleep
import motor.motor_asyncio
import kb_commands.member as member
import kb_commands.keybase as keybase
import kb_commands.post as post
from . import operations,ops
import utils.date as date
import json
import pymongo
import os
import subprocess
from . import db_cli


def connect_database(url = "localhost", port = 27017,test_run = False):
    
    #try:
        #exit_code = subprocess.Popen(shlex.split("ps -edaf | grep mongo | grep -v grep"), stdout=subprocess.PIPE, stderr=subprocess.PIPE).wait()
        
        #if(exit_code != 0):
            #print("Mongodb service isn't running\n")
            
            #subprocess.check_call(shlex.split("sudo supervisorctl start mongodb"))
            #sleep(5)
            
    #except FileNotFoundError:
        #pass
        
   
    client = motor.motor_asyncio.AsyncIOMotorClient(url, port,connectTimeoutMS = 1500,serverSelectionTimeoutMS = 5000)
    pybot_db = client.keybase if test_run is False else client.test

    return pybot_db






async def reset_users():
    return

async def reset_posts():
    return

async def reset_teams():
    return

async def reset_config(db):
    await db.config.ripping.delete_many({})
    await db.config.db.delete_many({})
    await db.config.delete_many({})
    return




async def load_config_from_file(db,config_path):
    with open(config_path) as config_file:
        try:
            config_content = config_file.read()
            json_config = json.loads(config_content)
            ripping_config = json_config.get('ripping',None)
            db_config = json_config.get('db',None)
            
        except json.JSONDecodeError as e:
            print("error")
            print(e)
            exit(1)
        
        else:
            if(ripping_config):
                try:
                    await db.config.insert_one({"type" : "ripping","ripping" : ripping_config})
                    print("Inserted ripping config")
                except pymongo.errors.DuplicateKeyError as error:
                    await db.config.update_one({"type" : "ripping"},{"$set" : {"ripping" : ripping_config}})
                    print("Updated ripping config")
                
                
            if(db_config):
                try:
                    await db.config.insert_one({"type" : "db","db" : db_config})
                    print("Inserted database config")
                    
                except pymongo.errors.DuplicateKeyError as error:
                    await db.config.update_one({"type" : "db"},{"$set" : {"db" : db_config}})
                    print("Updated database config")
        return





async def get_inactive_users(db,max_role,team,max_days_of_inactivity):
    
    reference_date = datetime.utcnow()
    limit_date = reference_date - timedelta(days =max_days_of_inactivity)
    max_role_value = await ops.user.get_role_value(db,max_role)
    print(limit_date)
    print(max_role_value)
    
    
    results = db.users.find({'most_recent_post_date' : {'$lte' : limit_date}, 
                             'is_member': True ,
                             'role' : {'$lte': max_role_value}}, 
                            projection = {'username' : True,'_id' : False,'most_recent_post_date' : True}, sort = [('username',pymongo.ASCENDING)])
    return await results.to_list(length=None),reference_date

async def get_single_field_user(db,username, field):
    result = await db.users.find_one({'username' : username}, projection ={'_id' : False, field : True})
    if(result is None):
        return None
    
    return result.get(field,None)







async def update_users(db,max_days_of_inactivity = 28):
    users = await ( (await operations.get_users(db)).to_list(length=None) )
    update_batch = [pymongo.UpdateOne({'username' : user['username']},{"$set" : await get_member_activity_data(db,user['username'],max_days_of_inactivity)}) for user in users]
    await db.users.bulk_write(update_batch)
    
    #tasks = asyncio.gather(*[ db.users.update_one({'username' : user['username']},{"$set" : await get_member_activity_data(db,user['username'],max_days_of_inactivity)})] for )
    #async for user in await operations.get_users(db):
        #await db.users.update_one({'username' : user['username']},{"$set" : await get_member_activity_data(db,user['username'],max_days_of_inactivity)})
    return


async def get_member_activity_data(db,username,max_days_of_inactivity,is_member = True, team = None):
    if(team is None):
        team = (await db.teams.find_one({'top_level' : True}))['name']
    
    most_recent_post_date = await operations.get_most_recent_postdate_user(db,username)
    oldest_post_date = await operations.get_oldest_postdate_user(db,username)
    teams = await member.get_user_membership(username)
    post_count = await operations.get_user_total_postcount(db,username)
    
    is_member = (teams is not None) and team in teams
    
    return {
        "username" : username,
        "post_count" : post_count, 
        "most_recent_post_date": most_recent_post_date, 
        "oldest_post_date": oldest_post_date, 
        "is_active" : is_member and (date.get_delta_from_datetime(most_recent_post_date) <= max_days_of_inactivity),
        'is_member' : is_member,
        'is_deleted': (not is_member) and await member.is_user_deleted(username),
        'teams': teams
        }





async def update_teams(db):
    results = await (db.teams.find({},projection = {"name": True})).to_list(length = None)
    results = [item['name'] for item in results]

    
    for team in results:
        channels = await keybase.get_team_channels(team)
        await db.teams.update_one({"name": team},{"$set": {'channels': channels}})
        
        for channel in channels:
            await member.join_channel(team,channel)
        


async def update_posts(db, max_days_of_inactivity = 28,unread_only = True):
    results = db.teams.find({},projection = {"channels": True})
    async for team in results:
        team_name = team['_id']
        channels = team['channels']
        print(team_name)
        print(channels)
        posts = await  post.get_parsed_posts_in_multiple_channels(team_name, channels, db = db, unread_only = unread_only)
        if(posts != None and len(posts) > 0):
            tasks = asyncio.gather(*[db.posts.insert_one(keybase_post) for keybase_post in posts])
            try:
                await tasks
            except pymongo.errors.DuplicateKeyError as error:
                pass
        
    await update_users(db,max_days_of_inactivity)
    
    return

async def update_applicants(db,team):
    return



async def initialize_database(db,team, max_days_of_inactivity = 28):
    await db.users.delete_many({})
    await db.teams.delete_many({})
    await db.posts.delete_many({})
    await db.config.ripping.delete_many({})
    await db.config.db.delete_many({})
    await db.config.delete_many({})
    await db.pybot_stats.delete_many({})
    await db.pybot_stats.runtime.delete_many({})
    await db.users.drop_indexes()
    
    result = await db.teams.create_index("name", unique = True)
    result = await db.users.create_index("username", unique = True)
    await db.config.create_index("type", unique = True)
    await db.posts.create_index([("id", pymongo.ASCENDING),
                                  ("conversation_id", pymongo.ASCENDING)],unique = True)
    
    await db.posts.create_index("sent_at")
    await db.posts.create_index("sender")
    await db.posts.create_index("team")
    initial_run = datetime.utcnow()
    await db.pybot_stats.insert_one({'runtime' : {'first_run': initial_run,'last_run' : initial_run}})
    #await db.pybot_stats.runtime.insert_one({'last_run': initial_run})
    
    
    #result = await db.users.create_index("uid", unique = True)
    #result = await 
    #print(result)
    #print(len(result.keys()))
    
    result = await keybase.get_allsubteams(team)
    subteams = result[1:]

    channels = await keybase.get_team_channels(team)
    result = await db.teams.insert_one({"_id" : team,"name" : team,"subteams" : subteams,"channels" : channels,"top_level" : True})
    
    members = await member.get_team_membership(team)
    await db.users.insert_many(members.values())
    posts = await  post.get_parsed_posts_in_multiple_channels(team, channels, db = db)
    if(posts != None or len(posts) > 0):
        await db.posts.insert_many(posts)
        #print(f"total {len(posts)} in {team}")
    
    
    
    
    
    for subteam in subteams:
        channels = await keybase.get_team_channels(subteam)
        result = await db.teams.insert_one({"_id" : subteam,"name" : subteam,"subteams" : None,"channels" : channels,"top_level" : False})
        
        posts = await  post.get_parsed_posts_in_multiple_channels(subteam, channels, db = db)
        if(posts != None and len(posts) > 0):
            print(len(posts))
            await db.posts.insert_many(posts)
        else:
            print(f"failed to fetch posts for {subteam} ")
                
            #print(f"total {len(posts)} in {subteam}")
        
        #print(result)
    
    update_batch = [ pymongo.UpdateOne({'username' : username},{"$set" : await get_member_activity_data(db,username,max_days_of_inactivity)}) for username in members.keys()]
    
    await db.users.bulk_write(update_batch)
    
    member_set = set(members.keys())
    complete_user_set = set(await db.posts.distinct('sender'))
    former_members = complete_user_set.difference(member_set)
        

    former_members_data = [await get_member_activity_data(db,username,max_days_of_inactivity,is_member=False) for username in former_members]
    
    #for data in former_members_data:
        #print(data)
        #await db.users.insert_one(data)
    
    await db.users.insert_many(former_members_data)
    return

async def print_info(db,team,max_days_of_inactivity = 28):
    async for result in (db.pybot_stats.find({}, projection = {'last_run': True, "_id":False})):
        if(result):
            print(f"last: {result}//{result['last_run'].ctime()}//{datetime.timestamp(result['last_run'])}")
    

    
    async for document in db.users.find({}):
        print(document)
        print()
    print(await get_inactive_users(db,'reader',max_days_of_inactivity))
    return



def main(team,rebuild):
    
    
    loop = asyncio.get_event_loop()
    pybot_db = connect_database()
    
    if(rebuild):
        loop.run_until_complete(initialize_database(pybot_db,team,28))
    else:
        loop.run_until_complete(print_info(pybot_db,team))
    

if __name__ == "__main__":
    parser = db_cli.create_parser()
    main(parser.team,parser.rebuild)

    

    
