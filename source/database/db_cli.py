import argparse
import os

def create_parser():
    parser = argparse.ArgumentParser(description = "Keybase-pybot Database Management\n", prog = "db_manager")
    
    team = parser.add_argument_group()
    team.add_argument("--team", action = "store", type = str, help = "Team name", required=True)
    team.add_argument("--subteam", type = str, default = None, help = "Subteams names", nargs = '+')
    team.add_argument("--channels", type = str, default = None, nargs = '+', action = 'append')
    
    time = parser.add_mutually_exclusive_group()
    time.add_argument("--weeks", type = int, default = 4, help = "Number of weeks that will be tracked for activity. Can be used in addition with the days parameter")
    time.add_argument("--days", type = int, default = 0, help = "Number of days that will be tracked for activity. Can be used in addition with the weeks parameter")
    
    action = parser.add_argument_group()
    action.add_argument("--rebuild",default = False, action = "store_true")
    action.add_argument("--moderator",nargs='+', default = None)
    action.add_argument("--add_excluded",nargs='+', default = None)
    action.add_argument("--remove_excluded",nargs='+', default = None)
    action.add_argument("--erase_excluded",default = False, action = "store_true")
    return parser.parse_args()
