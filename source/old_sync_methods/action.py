from enum import Enum
from messages import get_message
from logger import logger
import member
import  keybase

ACTION_LEVEL = Enum("action_type","download list warn ban")











def get_action(level,team_name,interval,parser = None):
    
    all_actions = {
        'list' : ListMembers(level,team_name,interval,parser),
        'warn' : WarnMembers(level,team_name,interval,parser),
        'ban'  : BanMembers(level,team_name,interval,parser),
        'download' : DownloadAttachments(level,team_name,interval,parser)
        
        }
    
    return all_actions[level]



class Action:
    
    
    def __init__(self,level,team_name,interval):
        self.level = ACTION_LEVEL[level]
        self.message = get_message(team_name,interval,self.level.name)
        self.team_name = team_name
        self.team_members = None
        
            
        self.days_of_inactivity = interval
        
    @classmethod
    def initialize_team_members(cls,team_name,interval):
        cls.team_members = member.get_complete_team_membership(team_name,interval)
        
    def execute(self):
        raise NotImplementedError
    

class DownloadAttachments(Action):
    def __init__(self,level,team_name,interval,parser):
        super().__init__(level,team_name,interval)
        self.download_base_directory = parser.download_base_directory
        self.overwrite = parser.overwrite
        self.skip_failed = parser.skip_failed
        self.scan_all = parser.scan_all
        self.max_attempts = parser.max_attempts
        self.subteams = parser.subteam
        self.channels = parser.channels
        
    def execute(self):
        logger.info("Downloading attachments")
        
        if(self.scan_all == True):
            keybase.download_all_attachments(self.team_name,self.download_base_directory,self.max_attempts,self.overwrite)
        
        else:
            return
        
        
    
    
class ListMembers(Action):
    def __init__(self,level,team_name,interval,parser):
        logger.debug("ListMembers")
        
        self.excluded = parser.excluded
        super().__init__(level,team_name,interval)
        self.excluded = parser.excluded
        self.moderator = parser.moderator
        
        logger.verbose(f"{self.excluded}")
        
        
    def execute(self):
        self.team_members =  member.get_complete_team_membership(self.team_name,self.days_of_inactivity)
        inactive_members = member.get_inactive_members( self.team_members)
        inactive_members = member.remove_excluded_from_inactivity_check(inactive_members,self.excluded)
        logger.info(f"Found {len(inactive_members)} removable inactive members in the team")
        member.print_members_info(member.sort_members_by_only_username_and_status(inactive_members),self.days_of_inactivity)
        
        
        inactive_member_count = member.Member.get_inactive_member_count()
        logger.info(f"Total of {inactive_member_count} inactive members")
        
        
class BanMembers(Action):
    def __init__(self,level,team_name,interval,parser):
        logger.debug("BanMembers")
        super().__init__(level,team_name,interval)
        self.excluded = parser.excluded
        self.moderator = parser.moderator
        
    
    def execute(self):
        self.team_members =  member.get_complete_team_membership(self.team_name,self.days_of_inactivity)
        logger.warning("Banning")
        
        inactive_members = member.get_inactive_members( self.team_members)
        inactive_members = member.remove_excluded_from_inactivity_check(inactive_members,self.excluded)
        inactive_members = member.get_removable_members(inactive_members,self.days_of_inactivity)
        
        keybase.remove_members(self.team_name,inactive_members,self.moderator,self.message)
        


        
        
        
class WarnMembers(Action):
    def __init__(self,level,team_name,interval,parser):
        logger.debug("WarnMembers")
        super().__init__(level,team_name,interval)
        self.excluded = parser.excluded
        self.moderator = parser.moderator
        
    
    def execute(self):
        logger.warning("Warning")
        self.team_members =  member.get_complete_team_membership(self.team_name,self.days_of_inactivity)
        inactive_members = member.get_inactive_members( self.team_members)
        inactive_members = member.remove_excluded_from_inactivity_check(inactive_members,self.excluded)
        member.warn_inactive_members(inactive_members,self.days_of_inactivity,self.moderator, self.message)
