CODE_OK = 0
TOP_MAX_HITS_SEARCH = 10000
CONTEXT = 0

ATTACHMENT_POST = 2

LIST_MEMBERS = "list"
WARN_MEMBERS = "warn"
BAN_MEMBERS = "ban"


COMMON_GENERIC_FILENAMES = ["paste.png","paste.jpg"]







def bytes_to_si(value,target_unit):
    units = {
        'K' : 1024,
        'M' : 1024*1024,
        'G' : 1024*1024*1024
    }
    
    return value/units[target_unit]
    
    
