from enum import Enum
import operator
import subprocess
from datetime import datetime
import post
import date
import keybase
import messages
import re
from logger import logger
import constants


STATUS = Enum('status','inactive active left_the_team deleted' )

ROLES = Enum('role','unknown reader writer admin owner')



class Member:
    active_member_count = 0
    inactive_member_count = 0
    former_member_count = 0
    total_member_count = 0
    
    member_count = dict()
    
    team_name = None
    
    
    
    
    def __init__(self,username,role=None):
        self.username = username
        self.post_count = 0
        self.role = ROLES.unknown
        self.status = STATUS.inactive
        self.most_recent_post_date = None
        self.oldest_post_date = None
        self.join_date = None
        self.added_by = None
        
        
        if role is not None:
            if role in ROLES.__members__:
                self.role = ROLES[role]
                
            self.increment_member_count()
        
    
    @classmethod
    def set_team_name(cls,team_name):
        cls.team_name = team_name 

    
    @classmethod
    def increment_active_member_count(cls):
        cls.active_member_count += 1
        
    @classmethod
    def increment_member_count(cls):
        cls.total_member_count += 1
        
    @classmethod
    def get_inactive_member_count(cls):
        return(cls.total_member_count - cls.active_member_count)
    
    
    
        
        
    
    def increment_post_count(self,post_date):
        if (self.status != STATUS.deleted or self.status != STATUS.left_the_team):
            self.post_count += 1
            self.update_most_recent_post(post_date)
            self.update_oldest_post_date(post_date)
            
    
    def is_removable_for_inactivity(self,days_of_inactivity):
        return (date.get_delta_from_datetime(self.join_date) > days_of_inactivity and date.get_delta_from_datetime(self.most_recent_post_date) > days_of_inactivity and self.role.value <= ROLES.reader.value and self.is_in_team())
 
    
    
    def update_most_recent_post(self, post_date):
        post_date = date.format_date(post_date)
        
        if(self.most_recent_post_date is None or self.most_recent_post_date < post_date):
            self.most_recent_post_date = post_date
            
            
    def update_oldest_post_date(self, post_date):
        post_date =  date.format_date(post_date)
        
        if(self.oldest_post_date is None or self.oldest_post_date > post_date):
            self.oldest_post_date = post_date
            
    
    def update_join_date(self,join_date):
        join_date = date.format_date(join_date)
        
        
        if self.join_date is None or self.join_date < join_date:
            self.join_date = join_date
            

        
    def set_active(self, post_date, days_of_inactivity):
        

        if(self.status == STATUS.inactive):
            if (date.get_delta_from_datetime(post_date) <=  days_of_inactivity):
                
                self.status = STATUS.active
                self.increment_active_member_count()
        
    def is_in_team(self):
        return (self.status == STATUS.active or self.status == STATUS.inactive)
    
    def is_excluded_from_inactivity_check(self,excluded):
        if self.username == "stantheman":
            print("excluding")
            print(excluded)
        
        for username in excluded:
                   
            
            if self.username == username:
                print("match")
                return True
        
        return False
        
    
    def check_if_left_the_team(self):
        output = subprocess.run(["keybase","id",self.username],stdout=subprocess.PIPE,stderr = subprocess.PIPE)
        
        if(output.returncode == constants.CODE_OK):
            self.status = STATUS.left_the_team
        
        else:
            self.status = STATUS.deleted



def remove_excluded_from_inactivity_check(members,excluded):
    return [member for member in members if not member.is_excluded_from_inactivity_check(excluded)]

def get_removable_members(members,days_of_inactivity):
    return [member for member in members if member.is_removable_for_inactivity(days_of_inactivity)]
    

def parse_posts_activity_info(posts,members,days_of_inactivity):
    logger.debug(f"Total posts {post.UserPost.channel_post_count}")
    #logger.debug(members.keys())
    
    for user_post in posts:
        members = user_post.fill_team_member_info(members,days_of_inactivity)
        print("next post")
    
    
    return members
                

def sort_members_by_only_username(team_members):
    return sorted(team_members.values(),key=operator.attrgetter('username'))


def sort_members_by_only_status(team_members):
    return sorted(team_members.values(),key=operator.attrgetter('status.value'))


def sort_members_by_only_username_and_status(team_members):
    sorted_members = []
    
    try:
        sorted_members = sorted(team_members.values(),key=operator.attrgetter('username','status.value'))
        
    except AttributeError as error:
        sorted_members = sorted (team_members,key=operator.attrgetter('username','status.value'))
    
    finally:
        return sorted_members
        
    
    #return sorted(team_members.values(),key=operator.attrgetter('username','status.value'))



def get_inactive_members(team_members):
    inactive_members = [member for member in team_members.values() if member.status == STATUS.inactive and member.role == ROLES.reader]

    
    return inactive_members


def get_deleted_members(team_members):
    deleted_members = [member for member in team_members.values() if member.status == STATUS.deleted]

    
    return deleted_members


def get_complete_team_membership(team_name,number_of_days_of_inactivity):
    
    members = initialize_team_members(team_name)
    
    members = get_members_activity_info(members,number_of_days_of_inactivity)
    
    
    return members

def initialize_team_members(team_name):
    logger.debug(f"Getting complete team membership")
    Member.set_team_name(team_name)
    
    members = keybase.get_team_membership(Member.team_name)
    members = keybase.get_members_join_date(members,Member.team_name)
    
    logger.debug(f"Team {team_name} initialized")
    return members

def get_members_activity_info(members,number_of_days_of_inactivity):
    logger.debug("Parsing activity info")
    
    posts = keybase.get_all_posts_in_team_subteams(Member.team_name)
    members = parse_posts_activity_info(posts,members,number_of_days_of_inactivity)
    
    logger.debug(f"Team {Member.team_name} : gathered all activity info")
    return members


def print_members_info(team_members,days_of_inactivity):
    logger.info("Members info")
    
    if(isinstance(team_members,dict)):
        team_members = team_members.values()
    
    for member in team_members:
        
        logger.info(f"{member.username} , status : {member.status.name}")
        logger.verbose(f"Last post: {date.date_ctime(member.most_recent_post_date)}")
        logger.verbose(f"Oldest post: {date.date_ctime(member.oldest_post_date)}")
        logger.verbose(f"Join date: {date.date_ctime(member.join_date)}")
        
        if (member.is_removable_for_inactivity(days_of_inactivity)):
            logger.warning(f"{member.username} has been inactive for longer than is allowed")
                



def warn_inactive_members(team_members,days_of_inactivity,moderator,warning_message):
    logger.info("Sending warnings")
    
    
    
    if(isinstance(team_members,dict)):
        team_members = team_members.values()
        
    warned_members = []
    
    for member in team_members:
        if (member.is_removable_for_inactivity(days_of_inactivity)):
            warned_members.append(member)
            
    for member in warned_members:
        logger.info(f"{member.username} , status : {member.status.name}")
        logger.debug(f"Last post: {date.date_ctime(member.most_recent_post_date)}")
        logger.debug(f"Oldest post: {date.date_ctime(member.oldest_post_date)}")
        logger.debug(f"Join date: {date.date_ctime(member.join_date)}")
        print()
    
    
    keybase.send_members_message(warned_members,moderator,warning_message)
        
    
    
    
