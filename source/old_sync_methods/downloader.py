from time import sleep
from concurrent.futures.thread import ThreadPoolExecutor
from concurrent.futures import as_completed as completed_threads
from logger import logger


def download_pool_manager(threads,api_request,skip_failed,failure_message,max_attempts,args_list):
    downloaded_count = 0
    total_files = len(args_list)
    
    loop = asyncio.get_event_loop()
    
    with ThreadPoolExecutor(max_workers=threads) as executor:
        results = {executor.submit(api_request,skip_failed,failure_message,max_attempts,args) : args for args in args_list}
        
        completed = completed_threads(results)
    
        
        for item in  completed:
            downloaded_count +=1
            logger.info(f"Downloaded {downloaded_count} of {total_files}")


    return
