import argparse
import logging
import os
from logger import VERBOSE_LEVEL




def create_parser():
    parser = argparse.ArgumentParser(description = "Inactivity checker\n", prog = "Inactivity checker")

    
    
    
    action = parser.add_argument_group()
    action.add_argument("--action", choices = ["warn","ban","list","download"], default = "list", nargs = "?")
    action.add_argument("--moderator",nargs='+')
    action.add_argument("--excluded",nargs='+', required=True)
    
    
    download_args = parser.add_argument_group()
    download_args.add_argument("--download_dir","--dest", dest = "download_base_directory",default = os.path.join(os.getcwd(),"downloads"))
    download_args.add_argument("--overwrite", default = False, action = "store_true")
    download_args.add_argument("--skip_failed", default = False, action = "store_true")
    download_args.add_argument("--scan_all", default = False, action = "store_true")
    download_args.add_argument("--max_attempts", default = 6, type = int)
        
   
    
    
    log_args = parser.add_mutually_exclusive_group()
    log_args.add_argument("--debug", action = "store_const",dest = "log_level" ,const = logging.DEBUG, default = logging.INFO)
    log_args.add_argument("--verbose",action = "store_const",dest = "log_level" ,const = VERBOSE_LEVEL)
    
    
    action.add_argument("--warn",dest = "action", action = "store_const",help = "Only send a warning message to inactive members", const = "warn")
    action.add_argument("--ban",dest = "action",action = "store_const",help = "Remove inactive members", const = "ban")
    action.add_argument("--list", action = "store_true")
    
    team = parser.add_argument_group()
    team.add_argument("--team", action = "store", type = str, help = "Team name", required=True)
    team.add_argument("--subteam", type = str, default = None, help = "Subteams names", nargs = '+')
    team.add_argument("--channels", type = str, default = None, nargs = '+', action = 'append')
    
    
    time = parser.add_mutually_exclusive_group()
    time.add_argument("--weeks", type = int, default = 4, help = "Number of weeks that will be tracked for activity. Can be used in addition with the days parameter")
    time.add_argument("--days", type = int, default = 0, help = "Number of days that will be tracked for activity. Can be used in addition with the weeks parameter")
    
    

    return parser.parse_args()
