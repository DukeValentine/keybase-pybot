import bot_commands
import re
from logger import ParsingException
from logger import TargetUserException


class KeybaseBot():

    def __init__(self,bot_name,creator_name):
        self.bot_name = bot_name
        self.creator_name = creator_name
        
        self.last_command_executed = None
        self.last_checked_for_new_commands = None
        self.all_commands = self.initialize_commands()
        
        self.received_command_count = 0
        self.total_executed_command_count = 0
        self.sucessfully_executed_command_count = 0
        
        self.command_history = None
        
        
        
        
    
    def initialize_commands(self):
        bot_commands.ListAliasesCommand()
        bot_commands.MostRecentPostCommand()
        bot_commands.OldestPostCommand()
        bot_commands.UserPostCountCommand()
        bot_commands.ShowUserStatsCommand()
        
        return bot_commands.Command.all_commands
    
    
    def parse_command(self,command_input,requester):
        """
        Given a string will parse the given command to the target users,which can be from 1 to many, including the caller
        Returns a Command if the string matches one, otherwise returns None
        """
        bot_name_index = 0
        input_command_index = 1
        target_users_index = 2
        
        
        if(requester == self.name):
            return None
        
        
        input_parts = command_input.split("/")
        
        if len(input_parts) != 3:
            raise ParsingException("Input string not properly formatted")
        
        
        username_regex = re.compile(r"@(\w*)")
        
        try:
            input_bot_name = username_regex.search(input_parts[bot_name_index]).group(1)
            input_command = input_parts[input_command_index].strip('/').strip('/')
            target_users = username_regex.findall(input_parts[target_users_index])
            
            
            if(input_bot_name != self.bot_name):
                raise ValueError("Name of this bot differs from given by the command")
            
            if(target_users is None and requester is None):
                raise TargetUserException("No target user found")

        except AttributeError as e:
            print(e.args)
            return None
        
        
        else:
            parsed_command = self.all_commands.get(bot_commands.format_alias_key(input_command),None)
            
            if(parsed_command.is_reflexive == False and target_users is None):
                raise TargetUserException("Targeting the requester in non reflexive commands is not allowed")

            
            parsed_command.requester = requester
            parsed_command.target_users = target_users
            
            
            
            return parsed_command
    
    


    def fetch_new_commands(self,chat_names,team_name=None):
    
        """
        Will look up for new commands as mentions of the bot name in the given chats.
        If the chat refers to a team channel, the team_name must be supplied.
        
        Returns a list of string of possible commands. No parsing is done at this stage
        
        """
    
    
        return 0


    def monitor_chats_for_commands(self,chat_names, team_names = None):
        """
        Will monitor the given chats for new commands
        """
    
    
    
    
        return 0
    
    
    def run(self):
        
        return
