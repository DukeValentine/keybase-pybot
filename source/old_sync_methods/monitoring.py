from collections import Counter
import linecache
import os
import tracemalloc
import constants
from logger import logger

def start_monitoring():
    tracemalloc.start()


def display_top(key_type='lineno', limit=5):
    if(tracemalloc.is_tracing() == False):
        tracemalloc.start()

    snapshot = tracemalloc.take_snapshot()
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)
    print()
    logger.debug(f"Top {limit} lines")
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        
        logger.debug(f'{index} : {filename}:{frame.lineno} : {constants.bytes_to_si(stat.size,"K"):.1f}KiB')
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            logger.debug({line})

    other = top_stats[limit:]
    
    if other:
        size = sum(stat.size for stat in other)
        logger.debug(f'{len(other)} others: {constants.bytes_to_si(size,"K"):.1f}KiB')
    total = sum(stat.size for stat in top_stats)
    logger.debug(f'Total allocated size: {constants.bytes_to_si(total,"K"):.1f} KiB')
