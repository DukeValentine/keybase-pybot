from datetime import datetime
import json
import subprocess
import constants
import downloader
import post
import member as team_member
from logger import logger
import post
import date
import re
import os
from time import sleep


TOP_MAX_HITS_SEARCH = 10000
CONTEXT = 0
CODE_OK = 0





def get_all_posts_in_channel_since_date():
    return 0

def send_message_private_chat(message,usernames):
    if(message is not None):
        subprocess.run(["keybase","chat","send",usernames, message])
        

def send_members_message(team_members,moderator_username,message):
        if(message is None):
            return
    
        logger.warning(f"{len(team_members)} members will be messaged")
        #logger.warning(team_members)
        logger.info(message)
    
        for member in team_members:
            logger.info(f"{member.username} is inactive")
            last_post = date.format_date(member.most_recent_post_date)
            logger.info(f"{ last_post}")
            
            if(member.role == team_member.ROLES.reader):
                moderators_usernames = ",".join(moderator_username)
                usernames = f"{member.username},{moderators_usernames}"
                send_message_private_chat(message, usernames)
            

def remove_member(team_name, username):
    teams = get_user_membership(username)
    
    logger.warning(f"Removing {username} from {teams}")
    
    #get user memberships
    #for team in teams
    
    for team in teams:
        subprocess.run(["keybase","team","remove-member",team,"--user",username,"--force"])
    
    

def remove_members(team_name, team_members,moderator_username,message):
    logger.warning(f"{len(team_members)} members will be removed")
    [logger.warning(f"{member.username} will be removed\nLast post: {date.date_ctime(member.most_recent_post_date)}") for member in team_members]
    
    #logger.warning(team_members)
    confirmation = input('Proceed? y/N:')
    
    
    if(confirmation.upper() == 'Y'):
        logger.warning("Received confirmation, removing users")
        [remove_member(team_name, member.username) for member in team_members if member.role == team_member.ROLES.reader] 
        
        send_members_message(team_members,moderator_username,message)

   
   
def read_channel_chat_for_unread_posts(team,channel):
    return ""


def get_user_membership(username, max_attempts = 6):
    api_json_input = ' {"method": "list-user-memberships", "params": {"options": {"username": "_USERNAME_"}}}'
    api_json_input = api_json_input.replace("_USERNAME_",username)
    
    result = run_keybase_api_request(False,"Failed to get user membership",max_attempts,["keybase","team","api","-m",api_json_input])
    json_general = json.loads(result.stdout)
    
    teams = []
    
    return [team['fq_name'] for team in json_general['result']['teams']]

def join_all_channels(team_name,max_attempts = 6):
    subteams = get_subteams(team_name,max_attempts)
    
    for team in subteams:
        channels = get_channel_list(team,max_attempts)
        
        for channel in channels:
            api_json_input = '{"method": "join", "params": {"options": {"channel": {"name": "_TEAM_NAME_", "members_type": "team", "topic_name": "_TOPIC_NAME_"}}}}'
            api_json_input = api_json_input.replace("_TEAM_NAME_",team)
            api_json_input = api_json_input.replace("_TOPIC_NAME_",channel)
            result = run_keybase_api_request(True,f"Failed to join {team}#{channel}",max_attempts,["keybase","chat","api","-m",api_json_input])
        
    
    

def get_team_membership(team,max_attempts = 6,specified_users  = None):
    logger.info(f"Getting members from {team}")
    api_json_input = ' {"method": "list-team-memberships", "params": {"options": {"team": "_TEAM_NAME_"}}}'
    api_json_input = api_json_input.replace("_TEAM_NAME_",team)
    
    result = run_keybase_api_request(False,"Failed to get team membership",max_attempts,["keybase","team","api","-m",api_json_input])
    
    json_general = json.loads(result.stdout)
    
    logger.debug(f"Return code {result.returncode}")
    
    members = dict()
    
    for role,users in json_general['result']['members'].items():
        
        if(specified_users is not None):
            users_not_found = []
            #if()
            
                #logger.error(f"You specified user {username}, but they are not in the team")
        
        
        for user in users:
            username = user['username']
            
            
            
            #logger.debug(f"{username} : {role.strip('s')}")
            members[username] = team_member.Member(username,role.strip('s'))
    
    return members



def get_channel_list(team,max_attempts = 6):
    api_json_input = '{"method": "listconvsonname", "params": {"options": {"topic_type": "CHAT", "members_type": "team", "name": "_TEAM_NAME_"}}}'
    api_json_input = api_json_input.replace("_TEAM_NAME_",team)
    
    result = run_keybase_api_request(False,f"Failed to get channel list from {team}",max_attempts,["keybase","chat","api","-m",api_json_input])
    
    json_general = json.loads(result.stdout)
    
    conversations = json_general['result']['conversations']
    
    if(conversations is None):
        return []
    
    channels = [conversation['channel']['topic_name'] for conversation in conversations]
        
    return channels
    
    


def get_subteams(team,max_attempts = 6):
    logger.info(f"Getting subteams from {team}")
    
    for attempt in range(1,max_attempts):
        result = subprocess.run(["keybase","team","show-tree",team],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                        
        
        if result.returncode == CODE_OK:
            result = result.stdout.decode('utf-8').split("\n")
            break

        else:
            if(attempt > max_attempts):
                logger.error("Failed to get channel list")
                logger.debug(result.stderr.decode('utf-8'))
                exit(1)
            
            else:
                logger.debug(f"Failed to get memberlist in attempt number {attempt}\nRetrying")
    
    return [field for field in "".join(result).strip().split() if len(field)>1 and team in field ] 


def get_all_posts_in_team_subteams(team_name, date = None,max_attempts=6,specified_users = None):
    all_posts = []
    
    all_subteams = get_subteams(team_name)
    
    for team in all_subteams:
        channels = get_channel_list(team)
        logger.info(f"Team name: {team}")
        logger.info(f"Channels : {channels}")
        
        
        logger.info("Starting search")
        
        for channel in  channels:
            logger.info(f"Searching {channel}")
            channel_posts = get_all_posts_in_channel(team,channel)
            logger.info(f"Search complete. {len(channel_posts)} results")    
                
            if(len(channel_posts) > 0):
                all_posts += channel_posts
    
    logger.debug("Finished getting posts")
    return all_posts



def get_raw_posts_in_channel(team,channel,max_attempts = 6,specified_users = None):
    api_json_input = '{"method": "searchregexp", "params": {"options": {"max_hits": 20000,"channel": {"name": "_TEAM_NAME_", "members_type": "team", "topic_name": "_TOPIC_NAME_"}, "query": ".*", "is_regex": true}}}'
    
    
    api_json_input = api_json_input.replace("_TEAM_NAME_",team)
    api_json_input = api_json_input.replace("_TOPIC_NAME_",channel)
    
    result = run_keybase_api_request(False,f"Failed to search {team}#{channel}",max_attempts,["keybase","chat","api","-m",api_json_input])
    json_general = json.loads(result.stdout)
    
    try:
        hits = json_general['result']['hits']
    
    except KeyError as e:
        logger.error(json_general)
        exit(1)
        
    else:
        return hits
    
    
def download_all_attachments(team_name,base_directory = os.path.join(os.getcwd(),"downloads"),max_attempts = 6,overwrite = False):
    all_attachments = []
    
    all_subteams = get_subteams(team_name)
    
    logger.info(f"Downloading attachments from {team_name} and its subteams")
    
    for team in all_subteams:
        channels = get_channel_list(team)
        
        for channel in channels:
            logger.verbose(f"Downloading attachments from {team}#{channel}")
            all_attachments += download_attachments_in_channel(team,channel,base_directory,max_attempts,overwrite)
            
    return all_attachments



            


def download_attachments_in_channel(team,channel,base_directory = os.path.join(os.getcwd(),"downloads"),max_attempts = 6,overwrite = False,threads = 3):
    hits = get_raw_posts_in_channel(team,channel,max_attempts)
    
    attachments = []
    
    download_args_list = []
    
        
    if(hits is not None):
        for hit in hits:
            message_info = hit['hitMessage']['valid']
            
            if(message_info['messageBody']['messageType'] == constants.ATTACHMENT_POST):
                message_id = message_info['messageID']
                
                message_object = message_info['messageBody']['attachment']['object']
                filename = message_object['filename']
                size = message_object['size']
                download_dir = os.path.join(base_directory,team,channel)
                fullpath = os.path.join(download_dir,filename)
                
                attachment = post.UserAttachment(message_id,message_info['senderUsername'],filename,team,channel)
                attachment.parse_metadata(size,message_object['metadata'])
                attachments.append(attachment)
                
                
                
                if(filename in constants.COMMON_GENERIC_FILENAMES):
                    filename += str(message_id)
                
                
                
                if(os.path.isdir(download_dir) == False):
                    os.makedirs(download_dir)
                    
                if(os.path.exists(fullpath) == False and overwrite == False):
                    download_json_input = '{"method": "download", "params": {"options": {"channel": {"name": "_TEAM_NAME_", "members_type": "team", "topic_name": "_TOPIC_NAME_"}, "message_id": _MESSAGE_ID_, "output": "_FULLPATH_"}}}'
        
                    download_json_input = download_json_input.replace("_TEAM_NAME_",team)
                    download_json_input = download_json_input.replace("_TOPIC_NAME_",channel)
                    download_json_input = download_json_input.replace("_MESSAGE_ID_",str(message_id))
                    download_json_input = download_json_input.replace("_FULLPATH_",fullpath)
                    
                    
                    attachment.set_as_downloaded()
                    logger.debug(download_json_input)
                    download_args_list.append(["keybase","chat","api","-m",download_json_input])
                    
                    
                    #run_keybase_api_request(True,"failed to download attachment",max_attempts,["keybase","chat","api","-m",download_json_input])
                    
            
        logger.info(f"Total of {post.UserAttachment.total_attachment_count} attachments found. {post.UserAttachment.downloaded_attachment_count} will be downloaded")
        downloader.download_pool_manager(threads,run_keybase_api_request,True,"failed to download attachment",max_attempts,download_args_list)    
    
    
    
    return attachments


def get_all_posts_in_channel(team,channel,max_attempts = 6,specified_users = None):
    posts = []
    
    hits = get_raw_posts_in_channel(team,channel,max_attempts)
    
   
    if(hits is not None):
        for hit in hits:
            message_info = hit['hitMessage']['valid']
            
            post_date = message_info['ctime'] #ctime is in ms, dividing by 1000 gives time in second for timestamp usage
            sender = message_info['senderUsername']
            post_content = message_info['bodySummary']
            
            if(specified_users is None or sender in specified_users):
                posts.append(post.UserPost(sender,post_date,post_content,team,channel))
            
            
             
    return posts
    
    

def get_members_join_date(members,team,max_attempts = 6,specified_users = None):
    logger.info(f"Getting {team} members join dates")
    
    api_json_input = '{"method":"read","params":{"options": {"channel": {"name":"_team-name_","members_type":"team","topic_name":"general"}}}}'
    api_json_input = api_json_input.replace("_team-name_",team)
    
    
    result = run_keybase_api_request(False,"Failed to get members' join date",max_attempts,["keybase","chat","api","-m",api_json_input])
    
    json_general = json.loads(result.stdout)
    
    for message in json_general['result']['messages']:
        
        try:
            system_msg_type = message['msg']['content']['system']['systemType']
           
        
        
        except KeyError as e:
            pass

        else:
             if(system_msg_type == 0):
                username = message['msg']['content']['system']['addedtoteam']['addee']
                join_date = datetime.utcfromtimestamp(message['msg']['sent_at'])
                
                if(specified_users is not None and username not in specified_users):
                    continue
                
                logger.debug(f"{username} : {join_date.ctime()}")
                
                
                member_joined = members.get(username,None)
                if(member_joined is not None):
                    member_joined.update_join_date(join_date)
                                                
    return members
    

def run_keybase_api_request(skip_failed,failure_message,max_attempts,args):
    for attempt in range(1,max_attempts+1):
        logger.debug(f"Attempt n° : {attempt} for {args}")
        result = subprocess.run(args,stdout = subprocess.PIPE,stderr=subprocess.PIPE)
        sleep(0.5)
        
        if (result.returncode == CODE_OK and json.loads(result.stdout).get('error',None) is None):
            break
        
        else:
            if(attempt >= max_attempts):
                logger.error(failure_message)
                logger.debug(result.stdout.decode('utf-8'))
                logger.debug(result.stderr.decode('utf-8'))
                if(skip_failed == False):
                    print("exit")
                    exit(1)
            
    return result
