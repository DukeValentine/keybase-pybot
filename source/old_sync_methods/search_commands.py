import asyncio
import os
import json
import shlex
import bs4
import re
import time
from collections import defaultdict

saucenao_search_url = "https://saucenao.com/search.php"
percentage_regex = re.compile("([0-9]*(.[0-9]*?))%")

async def iqdb_search(filepath,minimum_percentage = 85,best_match_only = True):
    result = await asyncio.create_subprocess_exec("curl", "-F",f"file=@{filepath}","https://iqdb.org",stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    #await result.wait()
    text_result = await result.stdout.read()
    text_result = bs4.BeautifulSoup(text_result,'html.parser')
    
    match_links = []
    
   
    
    for match in text_result.find_all('table'):
        for similarity_match in match.find_all('td'):
            percentage = percentage_regex.match(similarity_match.text)
            if(percentage is not None and float(percentage.group(1)) > minimum_percentage):
                links = [f"https:{link.get('href')}"  for link in match.find_all('a')]
                match_links.append(links)
                #print(links)
    
    if(len(match_links) > 0):
        if(best_match_only):
            #print(match_links)
            return match_links[0][0]
        else:
            return  match_links
    return None


async def filter_valid_links(links):
    return [link_filtered for link_filtered in links if (not "https://saucenao.com/info.php?lookup_type" in link_filtered)]



async def saucenao_search(filepath,minimum_percentage = 85,best_match_only = True):
    print(filepath)

    result = await asyncio.create_subprocess_exec("curl", "-F",f"file=@{filepath}",saucenao_search_url,stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    
    await result.wait()
    text_result = await result.stdout.read()
    
    if(result.returncode != 0):
        err = await result.stderr.read()
        print(err)
        return None
    
    
    match_links = []
    
    text_result = bs4.BeautifulSoup(text_result,'html.parser')
    match_check = text_result.find("div",{'class': 'result'})
    
    for match in text_result.find_all ("td",{'class': 'resulttablecontent'}):
        similarity_match = match.find('div',class_ = "resultsimilarityinfo")
        percentage =  percentage_regex.match(similarity_match.text)
        
        if(percentage is not None and float(percentage.group(1)) > minimum_percentage):
                #print(percentage.group(1))
                links = [f"{link.get('href')}"  for link in match.find_all('a')]
                links = await filter_valid_links(links)
                
                if(len(links) > 0): 
                    match_links.append(links)
                print(links)
    
    
    

    
    if( match_check is not None and match_check.get('id',None) is not None):
        return None
    
    
    if(len(match_links)> 0):
        if(best_match_only):
            print(match_links)
            return match_links[0][0]
        else:
            return match_links
        
    return None
        
        
async def process_saucenao_result(result):
    
    
    
    return

async def send_message_keybase(channel,team,message,reply_id = None):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()

    
    api_json_input['method'] = "send"
    api_json_input['params']['options']['channel']['name'] = team
    api_json_input['params']['options']['channel']['members_type'] = "team"
    api_json_input['params']['options']['channel']['topic_name'] = channel
    
    if(reply_id):
        api_json_input['params']['options']['reply_to'] = reply_id
    
    api_json_input['params']['options']['message']['body'] = message
    
    await asyncio.create_subprocess_exec("keybase", "chat","api","-m", json.dumps(api_json_input),stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)

async def download_keybase_post(id,channel,team,directory):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "download"
    api_json_input['params']['options']['channel']['name'] = team
    api_json_input['params']['options']['channel']['members_type'] = "team"
    api_json_input['params']['options']['channel']['topic_name'] = channel
    api_json_input['params']['options']['message_id'] = id
    api_json_input['params']['options']['output'] = os.path.join(directory,"looksource")
    
    print(json.dumps(api_json_input))
    
    result = await asyncio.create_subprocess_exec("keybase", "chat","api","-m", json.dumps(api_json_input),stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    await result.wait()
    
    if(result.returncode != 0): 
        return None

    
    result = await result.stdout.read()
    
    if("attachment downloaded" in result.decode("utf-8")):
        return result.decode("utf-8")

    
    else:
        return None


async def func():
    
    
    
    result = await asyncio.create_subprocess_exec("keybase", "chat","api-listen","--filter-channel",'{"name":"lo_nly", "members_type": "team"}',stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    while True:
         line = await result.stdout.readline()
         decoded_line = line.decode('utf-8').strip()
         
         try:
            yield json.loads( decoded_line)
        
         except json.decoder.JSONDecodeError:
            print(f"error parsing {decoded_line}")
            yield dict() 
    
    
async def search(poster_id, source_id,channel,team,path):
    if(source_id is not None):
        return_code = await download_keybase_post(source_id,channel,team,path)
        if(return_code):
            print("looking")
            filename = f"looksource{len(asyncio.all_tasks())}"
            
            link = await saucenao_search(os.path.join(path,filename))
            if(link is not None):
                if(type(link) is not str):
                    link = ",".join(link)
                
                await send_message_keybase(channel,team,link,reply_id = poster_id)
            
            else:
                await send_message_keybase(channel,team,"No source found",reply_id = poster_id)


async def main():
    #result = await saucenao_search("/home/nelarus-pc/Pictures/keybase_pics/77577513_p2_.jpg")
    #print(result.get('href'))
    #await download_keybase_post(193,"0_testing","hentai_club","/home/nelarus-pc/Downloads")
    
    abc = func()
    async for resp in abc:
        if(resp.get('msg',None) is not None):
            
            resp = resp['msg']
            
            if(resp['content'].get('type',None) == "text" and resp['content']['text']['body'] == "!source"):
                poster_id = resp['id']
                source_id = resp['content']['text'].get('replyTo',None)
                team = resp['channel']['name']
                channel = resp['channel']['topic_name']
                directory = os.getenv("HOME")
                await search(poster_id,source_id,channel,team,os.path.join(directory,"files"))
                
                #if(source_id is not None):
                    #return_code = await download_keybase_post(source_id,channel,team,os.path.join(directory,"files"))
                    #if(return_code):
                        #print("looking")
                        #link = await saucenao_search(os.path.join(directory,"files","looksource"))
                        #if(link is not None):
                            #if(type(link) is not str):
                                #link = ",".join(link)
                            
                            #await send_message_keybase(channel,team,link,reply_id = poster_id)
                        
                        #else:
                           #await send_message_keybase(channel,team,"No source found",reply_id = poster_id)
                        
                        
                
                    #result = await saucenao_search("/home/nelarus-pc/Pictures/keybase_pics/73840094_p0_master1200.jpg")
                    #print(result)
                
                
                #print("source command")
        
            if(resp['content'].get('type',None) == "reaction" and (resp['content']['reaction']['b'] == ":mag:" or resp['content']['reaction']['b'] == ":mag_right:")):
                source_id = resp['content']['reaction']['m']
                poster_id = source_id
                team = resp['channel']['name']
                channel = resp['channel']['topic_name']
                directory = os.getenv("HOME")
                await search(poster_id,source_id,channel,team,os.path.join(directory,"files"))
    
  
if __name__ == "__main__":
    print("abc")
    asyncio.run(main())
    print("abc")
