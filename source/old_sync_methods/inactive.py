import os

#from member import Member

import member
import operator

import keybase
import action
import cli
import date
import messages
import monitoring
import logging
from logger import logger
from logger import logger_config


monitoring.start_monitoring()

parser = cli.create_parser()
logger_config(parser.log_level)

team_name = parser.team
number_of_weeks_of_inactivity = parser.weeks
number_of_days_of_inactivity = date.get_days_of_inactivity(weeks=parser.weeks, days = parser.days)

MESSAGE = messages.get_message(team_name,number_of_days_of_inactivity,parser.action)

print(f"message = {MESSAGE}")

action = action.get_action(parser.action,team_name,number_of_days_of_inactivity,parser)
print(action.level.name)
action.execute()

monitoring.display_top()
                            




            

