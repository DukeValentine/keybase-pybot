def get_message(team_name,interval,action):
    messages = {
            'warn' : format_inactive_warning_message(team_name,interval),
            'ban'  : format_ban_message(team_name,interval),
            'list' : None,
            'download' : None
    
    }
    
    return messages[action]
    
    




def format_inactive_warning_message(team_name,interval):
    
    return(
            f"Beep Boop,I am a bot\nInactivity warning. You have been inactive in  {team_name} for {interval} days and will soon be removed per the rules if you don't participate in any of the channels. \nThis is an automated message, if you believe it was send in error do not worry : please reply here informing so and I will immediately look into your situation.\nIf you happen to be removed and want to rejoin, please apply to the team as you did for the first time. Note that extra scrutiny might be applied to the interview process"
    )



def format_ban_message(team_name,interval):
    
    
    return(
         f"Beep Boop,I am a bot\nYou have been removed from the group after being inactive in  {team_name} for {interval} days"
    )
