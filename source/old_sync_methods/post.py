import os
import constants
import date
import member
from logger import logger


IMAGE_ATTACHMENT = 1
VIDEO_ATTACHMENT = 2

ATTACHMENT_TYPE = {
    IMAGE_ATTACHMENT : 'image',
    VIDEO_ATTACHMENT : 'video'
    }

class UserPost:
    total_post_count = 0
    channel_post_count = dict()
    
    def __init__(self,sender,post_date,post_content,team,channel):
        self.sender = sender
        self.device_name = None
        self.device_type = None
        self.user_mentions = []
        self.channel_mentions = []
        self.reactions = dict()
        
        self.post_date = post_date
        self.post_content = post_content
        self.team = team
        self.channel = channel
        #self.increment_channel_post_count(team,channel)
        
        
    @classmethod
    def increment_post_count(cls):
        cls.total_post_count+=1
        
    @classmethod
    def increment_channel_post_count(cls,team,channel):
        key = f"{team}#{channel}"
        cls.channel_post_count[key] = cls.channel_post_count.get(key,0) + 1
        
    
    def fill_team_member_info(self,members,days_of_inactivity):
        
        team_member = members.get(self.sender,None)
        
        if(team_member is not None):
            
            team_member.set_active(self.post_date, days_of_inactivity = days_of_inactivity)
            team_member.increment_post_count(self.post_date)
            
        else:
            
            members[self.sender] =  member.Member(self.sender)
            members[self.sender].check_if_left_the_team()
            #members[self.sender].set_active(self.post_date, days_of_inactivity = days_of_inactivity)
            
        return members
    
    
class UserAttachment():
    total_attachment_count = 0
    downloaded_attachment_count = 0
    channel_attachment_count = dict()
    
    @classmethod
    def increment_downloaded_attachment_count(cls):
        cls.downloaded_attachment_count +=1
        
    @classmethod
    def increment_channel_attachment_count(cls,team,channel):
        key = f"{team}#{channel}"
        cls.channel_attachment_count[key] = cls.channel_attachment_count.get(key,0) + 1
        cls.total_attachment_count += 1
    
    
    def __init__(self,id,uploader,filename,team,channel):
        self.id = id
        self.uploader = uploader
        self.filename = filename
        self.metadata = dict()
        self.team = team
        self.channel = channel
        self.downloaded = False
        
        self.increment_channel_attachment_count(team,channel)
        
    def parse_metadata(self,size,misc_metadata):
        self.metadata['size'] = size
        type = misc_metadata['assetType']
        
        try:
            for key,value in misc_metadata[ATTACHMENT_TYPE[type]].items():
                self.metadata[key] = value
                
        except KeyError as e:
            pass
    
    def set_as_downloaded(self):
        self.downloaded = True
        self.increment_downloaded_attachment_count()
        
        
    def size_in_si(self,unit):
        return constants.bytes_to_si(self.size,unit)
        
        
    
