import os
import json
from collections import defaultdict
import subprocess
import shlex
import asyncio



home_dir = os.getenv("HOME")

def run_gallerydl_command(dest_dir,url):
    if(url is None): 
        return
    
    db_file = os.path.join(home_dir,".config","gallery-dl","downloaded_daily")
    download_dir = os.path.join(home_dir,"files","ripme",dest_dir)
    
    
    result = subprocess.run(["gallery-dl","--ugoira-conv","-o","extractor.danbooru.directory=\"\"","-o","extractor.pixiv.directory=\"\"","--download-archive",db_file,"-d",download_dir,url],stdout = subprocess.PIPE,stderr=subprocess.PIPE)
    
    if(result.returncode != 0):
        print(result.stderr.decode('utf-8'))
        print(result.stdout.decode('utf-8'))
        
    else:
        print(f"Updated {download_dir} from {url}")
    return 



def run_upload_command(api_json_input,directory,filename,team,channel):
    if(team is None or channel is None):
        return
    
    api_json_input['params']['options']['channel']['name'] = team
    api_json_input['params']['options']['channel']['topic_name'] = channel
    
    api_json_input['params']['options']['filename'] = os.path.join(directory,filename)
    #print (json.dumps(api_json_input))
    result = subprocess.run(["keybase","chat","api","-m",json.dumps(api_json_input)],stdout = subprocess.PIPE,stderr=subprocess.PIPE)
    
    if(result.returncode != 0):
        print(result.stderr.decode('utf-8'))
        print(result.stdout.decode('utf-8'))
        
    else:
        print(f"Uploaded {filename} successfully to {team}#{channel}")
    
    

def initialize_upload_info():
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "attach"
    api_json_input['params']['options']['channel']['name'] = ""
    api_json_input['params']['options']['channel']['members_type'] = "team"
    api_json_input['params']['options']['channel']['topic_name'] = ""
    api_json_input['params']['options']['filename'] = ""
    
    return api_json_input




def rip_to_keybase():
    api_json_input = initialize_upload_info()
    
    
    
    
    with open(os.path.join(home_dir,".config","keybase-pybot","rip_config.json")) as json_file:
        data = json.load(json_file)

        for category in data.values():
            run_gallerydl_command(category['directory'],category['url'])
            run_gallerydl_command(category['directory'],category.get('url2',None))
            
            directory = os.path.join(home_dir,"files","ripme", category['directory'])
            args = ["ls",directory]
    
            result = subprocess.run(args,stdout = subprocess.PIPE,stderr=subprocess.PIPE)
            
            if(result.returncode == 0):
                [run_upload_command(api_json_input,directory,file, category.get('team',None),category.get('channel',None)) for file in result.stdout.decode('utf-8').split("\n") if file]
                [run_upload_command(api_json_input,directory,file, category.get('team2',None),category.get('channel2',None)) for file in result.stdout.decode('utf-8').split("\n") if file]
                    
                
            else:
                print(result.stderr.decode('utf-8'))
                print(result.stdout.decode('utf-8'))
           
            
            args = f"rm {os.path.join(directory,'*')}"
            result = subprocess.run(args,stdout = subprocess.PIPE,stderr=subprocess.PIPE, shell = True)
            #print(result.args)
            #print(json.dumps(api_json_input))
            #print(result)
    
    
    return




def main():
    rip_to_keybase()
    return


main()
    
