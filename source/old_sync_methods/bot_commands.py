import re
from collections import defaultdict
from member import ROLES





def format_alias_key(alias):
    
    
    return alias.upper().replace(' ','')



class Command:
    all_commands = dict()
    all_aliases = defaultdict(list)
    
    
    
    
    
    def __init__(self,name,aliases,minimum_user_arguments = 0, minimum_role = ROLES.admin, reflexive = True):
        self.name = name
        self.minimum_user_arguments = minimum_user_arguments
        self.target_users = None
        self.requester = None
        self.minimum_role = minimum_role
        self.is_reflexive = reflexive
        
        for alias in aliases:
            self.add_command(alias,self)
            self.add_alias(alias,self.name)
        
    
    @classmethod
    def add_command(cls,alias,command):
        cls.all_commands[format_alias_key(alias)] = command
        
    
        
    @classmethod
    def add_alias(cls,alias,command_name):
        cls.all_aliases[command_name].append(alias)
        
    
    def is_user_authorized_to_execute(self,user_role):
        return user_role.value >= self.minimum_role 
    
        
    def is_command_valid(self,command_input):
        command_input = command_input.upper()
        
        
    def execute(self):
        raise NotImplementedError
    
    def list_this_command_aliases(self):
        raise NotImplementedError
    
    
    @classmethod
    def list_all_aliases(cls):
        all_commands_aliases = []
        
        for command_name, aliases in cls.all_aliases.items():
            aliases_formatted = " , ".join(aliases)
            
            all_commands_aliases.append(f"{command_name} : {aliases_formatted}")
        
        return  "\n".join(all_commands_aliases)



class UserPostCountCommand(Command):
    ALIASES = ['count my posts','count user posts','count post by user','count posts by user','count my post','count user post','get post count by', 'get post count by user','get user post count']
    
    
    def __init__(self):
        super().__init__("Get user post count" , self.ALIASES,minimum_user_arguments = 0, minimum_role = ROLES.reader)
        
        
class MostRecentPostCommand(Command):
    ALIASES = ['get my most recent post','get my recent post','get recent post by me','get recent post by','get recent post by user'
               ,'get most recent post by me','get most recent post ','get user most recent post','get most recent post by']
    
    
    
    def __init__(self):
        super().__init__("Get user most recent post" , self.ALIASES,minimum_user_arguments = 0, minimum_role = ROLES.reader)
        
        
        
class OldestPostCommand(Command):
    ALIASES = ['get my oldest post','get oldest post by me','get oldest post by','get oldest post by user','get oldest post ','get user oldest post','get oldest post by']
    
    def __init__(self):
        super().__init__("Get user oldest post" , self.ALIASES,minimum_user_arguments = 0, minimum_role = ROLES.reader)
        
        
class ShowUserStatsCommand(Command):
    ALIASES = ['show my stats','show stats of user','show user stats','show my info','show info of user','user stats','display my stats','display status of user','display user info','display my info']
    
    
    def __init__(self):
        super().__init__("Show user stats" , self.ALIASES,minimum_user_arguments = 0, minimum_role = ROLES.reader)
        

class ListAliasesCommand(Command):
    ALIASES = ['help','list commands','list-commands','show commands','show-commands','command list','command-list','get all commands','all commands']
    
    
    def __init__(self):
        super().__init__("Help" , self.ALIASES,minimum_user_arguments = 0, minimum_role = ROLES.reader)
   
   
    def execute(self):
       print(super().list_all_aliases())
       

class BanCommand(Command):
    ALIASES = ['ban','remove']
    
    def __init__(self):
        super().__init__("Ban User",self.ALIASES, minimum_user_arguments = 1 , minimum_role = ROLES.admin, reflexive = False)
        
        

        
    
    
    
