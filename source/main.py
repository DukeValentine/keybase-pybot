import asyncio

from cli import pybot_cli
from database import pybot_db
from database import operations
from bot import action


parser = pybot_cli.create_parser()
pybot_cli.validate_parser(parser)


pybot_db = pybot_db.connect_database(parser.url,parser.port,parser.test)

action = action.get_action(pybot_db,parser.action,parser.team, parser.days + 7*parser.weeks , parser=parser)

loop = asyncio.get_event_loop()

try:
    loop.run_until_complete( action.execute())
except KeyboardInterrupt as e:
    print("Received KeyboardInterrupt, shutting down...")
    loop.stop()
    exit(0)

