import asyncio
from concurrent.futures.process import ProcessPoolExecutor
from concurrent.futures.thread import ThreadPoolExecutor
from .keybase import run_api_request,run_keybase_api_request,KeybaseAPIError
from collections import defaultdict
import database.operations as db_operations
import json
import os
from tqdm import tqdm

home_dir = os.getenv("HOME")

async def test():
    print("test")
    
def sub_loop(team,channel,directory,message_id,filename,max_attempts):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(download_attachment(team,channel,directory,message_id,filename,max_attempts))
    

    
async def download_pool_manager(db,team,directory,max_attempts = 6):
    attachments,attachment_count= await db_operations.get_attachments_cursor(db,team)
    semaphore = asyncio.BoundedSemaphore(os.cpu_count())
    
    pbar = tqdm(total = attachment_count)
    
    tasks = asyncio.gather(*[semaphore_download_attachment(pbar,semaphore,attachment['team'],attachment['channel'],directory,attachment['id'],attachment['content']['filename'],max_attempts) async for attachment in attachments])
    result = await tasks
    
    pbar.close()

async def download_all_attachments_subteams(db,team,directory,max_attempts = 6):
    attachments,attachment_count= await db_operations.get_attachments_cursor(db,team)
    current_count = 0
    
    async for attachment in attachments:
        await download_attachment(attachment['team'],attachment['channel'],directory,attachment['id'],attachment['content']['filename'],max_attempts)
        current_count+=1
        print(f"{current_count} of {attachment_count}")
    return

async def download_all_attachments_subteam(db,team,directory,max_attempts = 6):
    attachments,attachment_count = await db_operations.get_attachments_cursor(db,team, get_subteams = False)
    
    print(f"count {attachment_count}")
    print()
    current_count = 0
    async for attachment in attachments:
        await download_attachment(attachment['team'],attachment['channel'],directory,attachment['id'],attachment['content']['filename'],max_attempts)
        current_count+=1
        print(f"{current_count} of attachment_count")
        
    return

async def download_all_attachments_channel(db,team,channel,directory,max_attempts = 6):
    attachments,attachment_count = await db_operations.get_attachments_cursor(db,team, get_subteams = False, channel = channel)
    async for attachment in attachments:
        print( await download_attachment(attachment['team'],attachment['channel'],directory,attachment['id'],attachment['content']['filename'],max_attempts) )
    return


async def semaphore_download_attachment(pbar,semaphore,team,channel,directory,message_id,filename,max_attempts = 6, team_rip = True):
    async with semaphore:
        result = await download_attachment(team,channel,directory,message_id,filename,max_attempts, team_rip )
        pbar.update()
        return result

async def download_attachment(team,channel,directory,message_id,filename,max_attempts = 6, team_rip = True):
    
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "download"
    api_json_input['params']['options']['channel']['name'] = team
    
    if(channel is not None):
        api_json_input['params']['options']['channel']['members_type'] = "team"
        api_json_input['params']['options']['channel']['topic_name'] = channel
        
    api_json_input['params']['options']['message_id'] = message_id
    
    
    
    if(team_rip):
        if("paste" in filename):
            filename = f"copy{message_id}_{filename}"
            
        api_json_input['params']['options']['output'] = os.path.join(directory,team,channel,filename).replace("~",home_dir)
        path = os.path.join(directory,team,channel)
        
    else:
        path = directory
        api_json_input['params']['options']['output'] = os.path.join(directory,filename).replace("~",home_dir)
    
    
    try:
        path = path.replace("~",home_dir)
        os.makedirs(path)
    except FileExistsError  as error:
        pass
    
    try:
        return await run_keybase_api_request(api_json_input,"keybase","chat","api","-m")
    except KeybaseAPIError as error:
        return error.get_context()
    
