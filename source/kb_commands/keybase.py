from datetime import datetime
import asyncio
from collections import defaultdict
import json
import os
import database.operations as operations
import re


class KeybaseAPIError(Exception):
    def __init__(self, message,api_input,api_args):
        self.message = message
        self.api_input = api_input
        self.api_args = api_args
    def __str__(self):
        return repr(self.message)
    
    def get_context(self):
        return {"error": self.message, "input": self.api_input,"args": self.api_args}




async def warn_users(users,moderator):
    return

async def remove_users(db,users,moderator,team_name):
    if(moderator is None):
        print("No moderator given")
        return
    
    print(f"To be removed: {' , '.join(users)}")
    
    print("Confirm removals? y/N")
    response = input()
    if(response.upper() == "Y"):
        print()
        tasks = asyncio.gather(*[remove_user(user,moderator, await operations.get_user_memberships(db,user,team_name)) for user in users])
        await tasks
        
    else:
        print("Users won't be removed")
    
    return users

async def remove_user(user,moderator,teams): 
    
    
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "remove-member"
    api_json_input['params']['options']['username'] = user
    
    
    for team in teams:
        api_json_input['params']['options']['team'] = team
        print(json.dumps(api_json_input))
        
        try:
            response = await run_keybase_api_request(api_json_input,"keybase","team","api","-m")
        except KeybaseAPIError as error:
            print(error.get_context())
    
    
    
    
    return


async def get_allsubteams(team,max_attempts = 6):
    result = await asyncio.create_subprocess_exec("keybase","team","show-tree",team,stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    await result.wait()
    
    if(result.returncode != 0): 
        return None

    subteams = await result.stdout.read()
    
    subteam_regex = "(" + re.escape(team) + "?\.?\w+)"
    
    return re.findall(subteam_regex,subteams.decode("utf-8"))

async def get_team_channels(team,max_attempts = 6):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "listconvsonname"
    api_json_input['params']['options']['topic_type'] = "CHAT"
    api_json_input['params']['options']['members_type'] = "team"
    api_json_input['params']['options']['name'] = team
    
    result = await run_api_request(api_json_input,"keybase","chat","api","-m")
    json_general = json.loads(result)
    
    conversations = json_general['result']['conversations']
    
    if(conversations is None):
        return []
    
    channels = [conversation['channel']['topic_name'] for conversation in conversations]
        
    return channels




async def run_keybase_api_request(api_json_input, *args):
    result = await asyncio.create_subprocess_exec(*args, json.dumps(api_json_input),stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    
    stdout, stderr = await result.communicate()
    
    jres = json.loads(stdout)
    if("error" in jres or result.returncode != 0):
        raise KeybaseAPIError(stdout,api_json_input,args)
        
    
    return stdout


async def run_api_request(api_json_input, *args):
    #print(api_json_input)
    result = await asyncio.create_subprocess_exec(*args, json.dumps(api_json_input),stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    stdout, stderr = await result.communicate()
    
    if(result.returncode != 0): 
        return None

    return stdout



