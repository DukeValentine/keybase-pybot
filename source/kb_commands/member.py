import asyncio
from enum import Enum
import json
from datetime import datetime
from .keybase import run_api_request,run_keybase_api_request, KeybaseAPIError
from collections import defaultdict
from database.ops import user as dbOPsUser

from typing import TypeVar,Dict
class SimpleMembership(Dict):
    str: str

class Membership(Dict):
    username : str
    role: int
    join_date : datetime
    
class TeamMembership(Membership):
    str : Membership
    



STATUS = Enum('status','inactive active left_the_team deleted banned' )

ROLES = Enum('role','unknown reader writer admin owner')


def get_role_value(role):
    try:
        return ROLES[role].value
    
    except KeyError as e:
        return -1
    
async def is_user_deleted(username,max_attempts = 6):
    result = await asyncio.create_subprocess_exec("keybase","id",username,stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    await result.wait()
    
    return result.returncode != 0

async def warn_inactive_members():
    return


async def join_channel(team,channel):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "join"
    api_json_input['params']['options']['channel']['name'] = team
    api_json_input['params']['options']['channel']['members_type'] = "team"
    api_json_input['params']['options']['channel']['topic_name'] = channel
    try:
        await run_keybase_api_request(api_json_input,"keybase","chat","api","-m")
    except KeybaseAPIError as error:
        print(error)
        return
    
async def add_to_team(team,username,role = "reader"):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "add-members"
    api_json_input['params']['options']['team'] = team
    api_json_input['params']['options']["usernames"] = [{"username":username,"role": role}]
    return await run_keybase_api_request(api_json_input,"keybase", "team","api","-m")


    
async def get_user_devices(username,max_attempts = 6):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "getdeviceinfo"
    api_json_input['params']['options']['username'] = username
    
    result_devices = await run_api_request(api_json_input,"keybase","chat","api","-m")
    json_general = json.loads(result_devices.decode("utf-8"))
    devices = json_general['result'].get('devices',None)
    return devices

async def get_user_membership(username : str,max_attempts  : int = 6 ):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "list-user-memberships"
    api_json_input['params']['options']['username'] = username
    result = await run_api_request(api_json_input,"keybase","team","api","-m")
    json_result = json.loads(result.decode("utf-8"))
    try:
        return [team['fq_name'] for team in json_result['result']['teams']]
    
    except KeyError as e:
        return []
    except TypeError as e:
        return None
    
    
async def get_team_membership_simple(team : str) -> SimpleMembership:
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "list-team-memberships"
    api_json_input['params']['options']['team'] = team
    
    
    try:
        result = await run_keybase_api_request(api_json_input,"keybase","team","api","-m")
        join_dates = await get_members_join_date(team)
    except KeybaseAPIError as error:
        print(error.get_context())
        return error
    
    members = SimpleMembership()
    json_general = json.loads(result.decode("utf-8"))
    for role,users in json_general['result']['members'].items():
        for user in users:
            members[ user['username']] =  user['username']
            
    return members
            


async def get_team_membership(team : str,max_attempts: int = 6) -> TeamMembership  :
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "list-team-memberships"
    api_json_input['params']['options']['team'] = team
    
    
    try:
        result = await run_keybase_api_request(api_json_input,"keybase","team","api","-m")
        join_dates = await get_members_join_date(team)
    except KeybaseAPIError as error:
        print(error.get_context())
        return error
        
   
    
    json_general = json.loads(result.decode("utf-8"))
    members = TeamMembership()
    
   
    for role,users in json_general['result']['members'].items():
        for user in users:
            username = user['username']
            members[username] = {'username': username,'role': get_role_value(role.strip('s')), 'devices' : await get_user_devices(username),'joindate' : join_dates.get(username,datetime.utcfromtimestamp(0))}
            
    
    return  members

async def get_members_join_date(team,max_attempts = 6):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "read"
    api_json_input['params']['options']['channel']['name'] = team
    api_json_input['params']['options']['channel']['members_type'] = "team"
    api_json_input['params']['options']['channel']['topic_name'] = "general"
    api_json_input['params']['options']['pagination']['next'] = ""
    api_json_input['params']['options']['pagination']['num'] = 500
    last_page = False
    
    members_join_date = dict()
    
    while( last_page is False):
        result = await run_api_request(api_json_input,"keybase","chat","api","-m")
        json_general = json.loads(result)
        
        for message in json_general['result']['messages']:
            try:
                system_msg_type = message['msg']['content']['system']['systemType']
            
            except KeyError as e:
                pass

            else:
                if(system_msg_type == 0):
                    username = message['msg']['content']['system']['addedtoteam']['addee']
                    join_date = datetime.utcfromtimestamp(message['msg']['sent_at'])
                    members_join_date[username] = join_date
        
        
        
        last_page = json_general['result']['pagination'].get("last",False)
        api_json_input['params']['options']['pagination']['next'] = json_general['result']['pagination']['next']
        
    return members_join_date    

