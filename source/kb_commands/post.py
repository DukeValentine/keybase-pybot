import asyncio
from collections import defaultdict
from datetime import datetime
import json
import database.operations as operations
import database.ops as db_operations
from concurrent.futures.process import ProcessPoolExecutor
from .keybase import run_api_request,run_keybase_api_request
import utils.logger as logger
import os


async def send_reply(message,team,channel,replyid):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "send"
    if(channel is not None):
        api_json_input['params']['options']['channel']['members_type'] = "team"
        api_json_input['params']['options']['channel']['topic_name'] = channel
    
    

    api_json_input['params']['options']['channel']['name'] = team
    api_json_input['params']['options']['message']['body'] = str(message)
    api_json_input['params']['options']['reply_to'] = replyid
    print(json.dumps(api_json_input))
    
    result = await run_api_request(api_json_input,"keybase","chat","api","-m")
    print(result)
    
async def send_message(message,team,channel):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "send"
    if(channel is not None):
        api_json_input['params']['options']['channel']['members_type'] = "team"
        api_json_input['params']['options']['channel']['topic_name'] = channel
    
    

    api_json_input['params']['options']['channel']['name'] = team
    api_json_input['params']['options']['message']['body'] = str(message)
    
    result = await run_api_request(api_json_input,"keybase","chat","api","-m")
    print(result)
    


async def send_chat_message(message,*usernames):
    print(f"{message}\n{usernames}")
    
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "send"
    members = usernames[0]
    if(len(usernames)>1):
        for username in usernames[1:]:
            members += f",{username}"
    api_json_input['params']['options']['message']['body'] = message
    api_json_input['params']['options']['channel']['name'] = members
    print(json.dumps(api_json_input))
    result = await run_api_request(api_json_input,"keybase","chat","api","-m")
    return


async def send_chat_attachment(directory,filename,team,channel,title = None):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "attach"
    api_json_input['params']['options']['channel']['name'] = team
    
    if(channel is not None):
        api_json_input['params']['options']['channel']['members_type'] = "team"
        api_json_input['params']['options']['channel']['topic_name'] = channel.replace("#","")
    
    if(title):
         api_json_input['params']['options']['title'] = title
    
    api_json_input['params']['options']['filename'] = os.path.join(directory,filename).replace("~",os.getenv("HOME")) if directory is not None else filename
    return await run_keybase_api_request(api_json_input,"keybase","chat","api","-m")    

async def delete_message(team,channel,message_id):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "delete"
    api_json_input['params']['options']['channel']['name'] = team
    
    if(channel is not None):
        api_json_input['params']['options']['channel']['members_type'] = "team"
        api_json_input['params']['options']['channel']['topic_name'] = channel
    
    api_json_input['params']['options']['message_id'] = message_id
    return await run_keybase_api_request(api_json_input,"keybase","chat","api","-m")
    


def sub_loop(directory,filename,team,channel):
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete()

async def attachment_upload_pool(path,files,team,channel):
    await logger.info(f"Starting upload for {team}#{channel}")
    
    tasks = asyncio.gather(*[send_chat_attachment(path,filename,team,channel) for filename in files])
    await tasks
    await logger.info(f"Finished upload for {team}#{channel}")


async def parse_multiple_posts(posts):
    return [await parse_post(post) for post in posts]
    
    

async def parse_post(post):
    nested_dict = lambda: defaultdict(nested_dict)
    parsed_post = nested_dict()
    
    if(post is None):
        return 
    
    
    
    
    parsed_post['id'] = post['id']
    parsed_post["conversation_id"] = post["conversation_id"]
    parsed_post['sender'] = post['sender']['username']
    parsed_post['sender_uid'] = post['sender']['uid']
    parsed_post['device_id'] = post['sender']['device_id']
    
    device = post['sender'].get('device_name',None)
    
    if(device):
        parsed_post['device_name'] = post['sender']['device_name']
    parsed_post['channel'] = post['channel'].get('topic_name',None)
    parsed_post['team'] = post['channel']['name']
    parsed_post['main_team'] = post['channel']['name'].split(".")[0]
    
    parsed_post['sent_at'] = datetime.utcfromtimestamp( post['sent_at'])
    parsed_post['sent_at_ms'] = datetime.utcfromtimestamp( post['sent_at_ms']/1000) 
    parsed_post['content']['type'] = post['content']['type']
    
    if(post['content']['type'] == "attachment"):
        parsed_post['content']['filename'] = post['content']['attachment']['object']['filename']
        parsed_post['content']['size'] = post['content']['attachment']['object']['size']
        parsed_post['content']['mimeType'] = post['content']['attachment']['object']['mimeType']
        parsed_post['content']['metadata'] = post['content']['attachment']['object']['title']
        parsed_post['content']['title'] = post['content']['attachment']['object']['filename']
        
    elif(post['content']['type'] == "text"):
        parsed_post['content']['text'] = post['content']['text']
    
    elif(post['content']['type'] == "reaction"):
        parsed_post['content']['reaction'] = post['content']['reaction']
        
    else:
        try:
            content_type = post['content']['type']
            parsed_post['content'][content_type] = post['content'][content_type] 
        except KeyError as error:
            pass
        
    return parsed_post


async def get_raw_posts_in_channel(team, channel, max_attempts = 6,db = None,unread_only = False, only_first_page = False, page_size = 1000):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "read"
    api_json_input['params']['options']['unread_only'] = unread_only
    api_json_input['params']['options']['channel']['name'] = team
    
    if(channel is not None):
        api_json_input['params']['options']['channel']['members_type'] = "team"
        api_json_input['params']['options']['channel']['topic_name'] = channel
        
    api_json_input['params']['options']['pagination']['next'] = ""
    api_json_input['params']['options']['pagination']['num'] = page_size
    last_page = False
    page_already_read = False
    raw_posts = []
    
    
   
        
    
    while( last_page is False and page_already_read is False):
        
        for attempt in range(1,max_attempts):
            result = await run_keybase_api_request(api_json_input,"keybase","chat","api","-m")
            json_general = json.loads(result)
            
            try:
                hits = json_general['result']['messages']
                last_page = (json_general['result']['pagination'].get("last",False) or only_first_page)
                raw_posts += [post['msg'] for post in hits if 'msg' in post]
                if(len(raw_posts) == 0): raise ValueError(f"No posts found for {team}#{channel}")
            
            except ValueError as error:
                if(json_general['result'].get('pagination',None) is not None):
                    print(f"okay {channel} ")
                    last_page = True
                    break
                
                else:
                    raise error
                
                
            except KeyError as error:
                print(f"{team}#{channel}")
                print("json input")
                print(json.dumps(api_json_input))
                print("response")
                print(json_general)
                
                if(json_general.get('error',None) is None and json_general['result'].get('offline',True) and attempt >= max_attempts):
                    last_page = True
                    break
                
               
                await asyncio.sleep(1)
                if(attempt >= max_attempts):
                    return []
                pass
            else:
                last_post_page = raw_posts[-1]
                page_already_read = await db_operations.post.post_exists(db,last_post_page)
                print(f"okay {channel} ")
                api_json_input['params']['options']['pagination']['next'] = json_general['result']['pagination'].get('next',api_json_input['params']['options']['pagination']['next'])
                break
            
    
    return raw_posts

async def get_raw_posts_in_multiple_channels(team, channels, max_attempts = 6,db = None,unread_only = False, only_first_page = False, page_size = 1000):
    posts = []
    
    for channel in channels:
        current_posts = await get_raw_posts_in_channel(team,channel,max_attempts,db,unread_only, only_first_page, page_size)
        if(current_posts and len(current_posts)>0):
            posts+= current_posts
        else:
            print(f"empty for {team}#{channel}")
        
    return posts
        
    
async def get_parsed_posts_in_channel(team, channels, max_attempts = 10,db = None,unread_only = False, only_first_page = False, page_size = 1000):   
    return await parse_multiple_posts(await get_raw_posts_in_channel(team,channels,max_attempts,db,unread_only, only_first_page, page_size ))

async def get_parsed_posts_in_multiple_channels(team, channels, max_attempts = 6,db = None,unread_only = False, only_first_page = False, page_size = 1000):
    return await parse_multiple_posts(await get_raw_posts_in_multiple_channels(team,channels,max_attempts,db,unread_only, only_first_page, page_size))
    
