from datetime import datetime
import asyncio
from collections import defaultdict
import json
import os
from .keybase import run_keybase_api_request
import re

async def warn_users(users,moderator):
    return

async def remove_users(db,users,moderator,team_name):
    if(moderator is None):
        print("No moderator given")
        return
    
    print(f"To be removed: {' , '.join(users)}")
    
    print("Confirm removals? y/N")
    response = input()
    if(response.upper() == "Y"):
        print()
        tasks = asyncio.gather(*[remove_user(user,moderator, await operations.get_user_memberships(db,user,team_name)) for user in users])
        return await tasks
        
    else:
        print("Users won't be removed")
        return []
    


async def remove_user(username,moderator,teams): 
    
    
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "remove-member"
    api_json_input['params']['options']['username'] =username
    
    
    for team in teams:
        api_json_input['params']['options']['team'] = team
        print(json.dumps(api_json_input))
        
        try:
            response = await run_keybase_api_request(api_json_input,"keybase","team","api","-m")
        except KeybaseAPIError as error:
            print(error.get_context())
            return {'status': -1,"username":username, "response": error}
            
    
    
    
    return {'status': 0, "username":username,"response": teams}


async def get_allsubteams(team,max_attempts = 6):
    result = await asyncio.create_subprocess_exec("keybase","team","show-tree",team,stdout=asyncio.subprocess.PIPE,stderr =asyncio.subprocess.PIPE)
    await result.wait()
    
    if(result.returncode != 0): 
        return None

    subteams = await result.stdout.read()
    
    subteam_regex = "(" + re.escape(team) + "?\.?\w+)"
    
    return re.findall(subteam_regex,subteams.decode("utf-8"))

async def get_team_channels(team,max_attempts = 6):
    nested_dict = lambda: defaultdict(nested_dict)
    api_json_input = nested_dict()
    api_json_input['method'] = "listconvsonname"
    api_json_input['params']['options']['topic_type'] = "CHAT"
    api_json_input['params']['options']['members_type'] = "team"
    api_json_input['params']['options']['name'] = team
    
    result = await run_api_request(api_json_input,"keybase","chat","api","-m")
    json_general = json.loads(result)
    
    conversations = json_general['result']['conversations']
    
    if(conversations is None):
        return []
    
    channels = [conversation['channel']['topic_name'] for conversation in conversations]
        
    return channels
