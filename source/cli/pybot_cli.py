import argparse
import os
from utils.logger import VERBOSE_LEVEL
import logging

ACTIONS = ["warn","ban","list","download","config","rebuild","ripping","run","update.posts","update.user","update.team", "update.lookup"]

keybase_parser = parser = argparse.ArgumentParser(description = "Keybase pybot\n", prog = "Keybase pybot")

def create_parser():
    parser = keybase_parser

    
    action = parser.add_argument_group()
    action.add_argument("--action", choices = ACTIONS, default = "list", nargs = "?", required = True)
    action.add_argument("--moderator",nargs='+', default = None)
    action.add_argument("--excluded",nargs='+', default = [])
    action.add_argument("--max_inactive_role", default = None)
    action.add_argument("--url", default = "localhost")
    action.add_argument("--port", default = 27017)
    action.add_argument("--testdb", dest = "test",action = "store_true", default = False)
    action.add_argument("--dryrun",action = "store_true", default = False)
    
    botrun = parser.add_argument_group()
    botrun.add_argument("--listen_all",action = "store_true",default = False)
    botrun.add_argument("--listen_channels", action = 'append', default = None)
    botrun.add_argument("--listen_users", action = 'append', default = [])
    
    
    config = parser.add_argument_group()
    config.add_argument("--configpath", default = None)
    config.add_argument("--reset_config", default = False)
    
    update = parser.add_argument_group()
    update.add_argument("--page_size", type = int, default = 1000)
    update.add_argument("--unread", action = "store_true",default = False)
    update.add_argument("--csvfile")
    update.add_argument("--username", action = "append", dest = "usernames", default = None)
    update.add_argument("--usernames",nargs='+', default = None)
    update.add_argument("--update_action",choices = ["set","unset","rename","min","max","inc","push"], default = None)
    update.add_argument("--field",action = "append",default = None)
    update.add_argument("--value",action = "append",default = None)
    update.add_argument("--manual",action = "store_true", default = False)
    
    upload_args = parser.add_argument_group()
    upload_args.add_argument("--noupload", action = "store_true",default = False)
    
    
    download_args = parser.add_argument_group()
    download_args.add_argument("--download_dir","--dest", dest = "download_base_directory",default = os.path.join(os.getcwd(),"downloads"))
    download_args.add_argument("--overwrite", default = False, action = "store_true")
    download_args.add_argument("--skip_failed", default = False, action = "store_true")
    download_args.add_argument("--scan_all", default = False, action = "store_true")
    download_args.add_argument("--max_attempts", default = 6, type = int)
        
   
    
    
    log_args = parser.add_mutually_exclusive_group()
    #log_args.add_argument("--silent", action = "store_const",dest = "log_level" ,const = 33)
    log_args.add_argument("--debug", action = "store_const",dest = "log_level" ,const = logging.DEBUG, default = logging.INFO)
    log_args.add_argument("--verbose",action = "store_const",dest = "log_level" ,const = VERBOSE_LEVEL)
    log_args.add_argument("--silent", action = "store_const",dest = "log_level" ,const = logging.WARNING)
    
    
    action.add_argument("--warn",dest = "action", action = "store_const",help = "Only send a warning message to inactive members", const = "warn")
    action.add_argument("--ban",dest = "action",action = "store_const",help = "Remove inactive members", const = "ban")
    action.add_argument("--list", action = "store_true")
    
    team = parser.add_argument_group()
    team.add_argument("--team", action = "store", type = str, help = "Team name")
    team.add_argument("--subteam", type = str, default = None, help = "Subteams names", nargs = '+')
    team.add_argument("--channels", type = str, default = None, nargs = '+', action = 'append')
    
    
    time = parser.add_mutually_exclusive_group()
    time.add_argument("--weeks", type = int, default = 4, help = "Number of weeks that will be tracked for activity. Can be used in addition with the days parameter")
    time.add_argument("--days", type = int, default = 0, help = "Number of days that will be tracked for activity. Can be used in addition with the weeks parameter")
    
    

    return parser.parse_args()


def validate_parser(parser):
    print(parser.log_level)
    
    if(parser.action == "rebuild"):
        if(parser.team is None):
            print("ERROR: For rebuild action team must be supplied")
            exit(1)
            
    elif(parser.action == "config"):
        if(parser.configpath is None):
            raise argparse.ArgumentError(argument = None, message = "ERROR: For config action the configpath must be supplied")
            
    elif(parser.action == "update.user"):
        if (parser.manual is True and (parser.update_action is None or parser.field is None or parser.usernames is None or parser.value is None)):
            keybase_parser.error(message = "Manual user update requires the action, field to be updated and the new value")
            
    elif(parser.action == "update.lookup"):
         if(parser.csvfile is None):
              keybase_parser.error(message = "lookup updates need a csv file to read from")
        
    



