class ABC():
    def __init__(self):
        self.abc = None
        
    def print_class(self):
        print(self.__class__)

class DEF(ABC):
    def __init__(self):
        super(DEF,self).__init__()
        
        
obj1 = ABC()
obj1.print_class()
obj2 = DEF()
obj2.print_class()
